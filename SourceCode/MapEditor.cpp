#include"all.h"
#include"MapEditor.h"
#include<iostream>
#include "user.h"
#include <compare>

void MapEditor::init()
{
	state = 0;
	timer = 0;
	cursolPos = {};
	EDTpoint = {};
	cursolArrayPosX = 0;
	cursolArrayPosY = 0;
	doUseMause = false;
	conState = 0;
	mapEditorFlg = false;
	tipType = 1;

}

void MapEditor::update(char terrain[CHIP_NUM_Y][CHIP_NUM_X])
{
	////////////////////////////////////////////////////////////////////////////////
	////デバッグ用の機能なのでImgui以外ではmapEditorFlgの値は変えないように！！！！！////
	///////////////////////////////////////////////////////////////////////////////

	ImGuiCode();

	if (mapEditorFlg)
	{
		switch (state)
		{
			//パラメータ設定
		case 0:
			//SetCursorPos((int)cursolPos.x, (int)cursolPos.y);


			break;
			//更新
		case 1:
			GetCursorPos(&EDTpoint);
			ScreenToClient(window::getHwnd(), &EDTpoint);
			drawMaptip(terrain);
			break;

			//そのままセーブ
		case 2:
			saveData(terrain);
			state = 0;
			break;

			//名前を付けてセーブ
		case 3:

			break;
		}
	}

	//drawMaptip()が動いている間はImguiを触っているときも裏でチップが描かれてしまうのでマウス中ボタンでstateを変える
	if (TRG(0) & MOUSE_CENTER_CLICK)
	{
		state = 0;
	}



	debug::setString("EDTpointX:%ld", EDTpoint.x);
	debug::setString("EDTpointY:%ld", EDTpoint.y);
	debug::setString("cursolArrayPosX:%d", cursolArrayPosX);
	debug::setString("cursolArrayPosY:%d", cursolArrayPosY);
	debug::setString("doUseMause:%d", doUseMause);
}



void MapEditor::ImGuiCode()
{
#ifdef USE_IMGUI
	if (showMapEditWindow)
	{
		ImGui::PushStyleColor(ImGuiCol_TitleBgActive, ImVec4(0.0f, 0.7f, 0.2f, 1.0f));//色を緑色に
		ImGui::PushStyleColor(ImGuiCol_TitleBg, ImVec4(0.0f, 0.3f, 0.1f, 1.0f));
		ImGui::Begin("Map Editor");

		static const char* mapcolor[]{ "Stage1", "Stage2" };
		static int selectedMapcolor = 0;
		static bool selected[2];


		//エディターモード起動
		if (ImGui::Button("EditorMode"))
		{
			mapEditorFlg = true;
		}
		
		
		if (mapEditorFlg)
		{


			ImGui::Begin("SelectAset");
			//マップチップの”画像”の種類を選ぶ		マップチップの画像が増えたら適時最大数を変更する
			ImGui::Combo("MapColor", &selectedMapcolor, mapcolor, IM_ARRAYSIZE(mapcolor));
			ImGui::SameLine(); HelpMarker("selentStage texture");
			SceneGame::instance()->bgManager()->texNo = selectedMapcolor;
			//マップチップの属性（床や坂など）を選ぶ　※-1は何もなしブロック
			ImGui::SliderInt("TipType", &tipType, -1, 20);
			ImGui::End();


			ImGui::Begin("Preview");

			float imageTexPosX = static_cast<float> (tipType % 4 * 32);		//切り抜き位置X
			float imageTexPosY = static_cast<float>(tipType / 4 * 32);		//切り抜き位置Y
			if (tipType % 4 == 0) imageTexPosX = 0;
			ImGui::draw_preview(selectedMapcolor, { 2, 2 }, { imageTexPosX,  imageTexPosY }, { 32, 32 }, { 1, 1, 1, 1 }, false);

			ImGui::End();

			//チップを描けるようにする
			if (ImGui::ButtonEx("drawTip", ImVec2(50, 20)))
			{
				state = 1;
			}


			static int a;
			static int b;
			static int c;

			


			//セーブ
			if (ImGui::Button("Save"))	state = 2;
			
			ImGui::SameLine();

			if (ImGui::Button("SaveAs..."))	SaveAsFlg = true;
			
		if (SaveAsFlg)
		{
			ImGui::Begin("SaveAs...");
			ImGui::InputText("named", fileN, sizeof(fileN));
			if (ImGui::Button("OK"))
			{
				//strcpy_s(fileN,char_array_menuType);
				//Filename = fileN;
				state = 2;
				SaveAsFlg = false;
			}
			ImGui::SameLine();
			if (ImGui::Button("Cancel"))
			{
				SaveAsFlg = false;
			}
			ImGui::End();
		}
		
			//debug::setString("doUseMause:%s", char_array_menuType);
			//エディターモード終了
			if (ImGui::Button("End"))
			{
				mapEditorFlg = false;
				state = 0;
			}
		}
		ImGui::End();

		ImGui::PopStyleColor(); //PushStyleColorの数だけ記述
		ImGui::PopStyleColor();
	}
#endif
}




void MapEditor::drawMaptip(char terrain[CHIP_NUM_Y][CHIP_NUM_X])
{
	float castEDTpointX = (float)EDTpoint.x + 1.0f;
	float castEDTpointY = (float)EDTpoint.y + 1.0f;
	//カーソルの位置を配列情報に直す
	cursolArrayPosX = (int)castEDTpointX / 32;
	cursolArrayPosY = (int)castEDTpointY / 32;


	//描く
	if (STATE(0) & MOUSE_LEFT_CLICK)
	{
		terrain[cursolArrayPosY][cursolArrayPosX] = tipType;
	}

	//消す
	if (STATE(0) & MOUSE_RIGHT_CLICK)
	{
		terrain[cursolArrayPosY][cursolArrayPosX] = -1;
	}
}

void MapEditor::saveData(char terrain[CHIP_NUM_Y][CHIP_NUM_X])
{
	
	ofstream ofs(fileN, ios::out | ios::trunc);
	if (ofs)
	{
		for (int i = 0; i < CHIP_NUM_Y; i++)
		{
			for (int j = 0; j < CHIP_NUM_X; j++)
			{
				maptip_array[i][j] = terrain[i][j];
				ofs << maptip_array[i][j];
				ofs << ",";
			}
			ofs << endl;
		}
		ofs.close();
	}
	else
	{

	}
}


//Ctrl＋Zみたいな取り消すやつもやってみたい
