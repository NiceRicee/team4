//******************************************************************************
//
//
//		TITLE_CPP
//
//
//******************************************************************************

#include "all.h"
#include "audio.h"
#include "scene_pause.h"

VECTOR2 im_pos;
VECTOR2 im_pos2;
VECTOR2 im_pos3;

void SceneTitle::init()
{
	music::play(BGM_TITLE, true);

	state = 0;
	timer = 0;

	divergence = 0;
	arrow_pos = {};
	arrow_pos_2 = {};

	is_next_string = false;
	// ポーズ画面関連
	isPause = false;
}

void SceneTitle::deinit()
{
	music::stop(BGM_TITLE);
}
//***************************
//    更新処理
//***************************
void SceneTitle::update()
{
	const wchar_t* respawn_file_name = { L"./Data/Files/respawn_data.dat" };
	std::ifstream ifs(respawn_file_name);

	// ポーズ中も確認コマンド表示中もアニメーションさせる
	++timer;

	// ポーズ画面
	if (isPause) {
		ScenePause::getInstance()->update_title_scene();
		// メニューを閉じた時の1フレーム処理
		if (ScenePause::getInstance()->getStageBackFlg()) {
			isPause = false;
		}
		return;  // ポーズ中は操作を受け付けない
	}


	switch (state)
	{
	case 0:
		//////// 初期設定 ////////

		++state;
		/*fallthrough*/

	case 1:
		/////// パラメーターの設定 ////////
		GameLib::setBlendMode(Blender::BS_ALPHA);
		// メニューデータの読み込み
		ScenePause::getInstance()->loadData();

		// パラメータ設定
		arrow_pos = { 510.0f, 575.0f };
		arrow_pos_2 = { 920.0f, arrow_pos.y };
		divergence = 0;

		++state;
		/*fallthrough*/

	case 2:
		//////// 通常時 ////////
		// 矢印の動き
		{
			static int arrow_timer     = 0;
			static bool symmetry       = false;
			const float SYMMETRY_VALUE = 0.7f;
			const int SYMMETRY_FRAME   = 40;
			// 左右の動き
			switch (symmetry)
			{
			case false: arrow_pos.x += SYMMETRY_VALUE;  arrow_pos_2.x -= SYMMETRY_VALUE; break;
			case true:  arrow_pos.x -= SYMMETRY_VALUE;  arrow_pos_2.x += SYMMETRY_VALUE; break;
			}
			// 左右判定
			if (arrow_timer / SYMMETRY_FRAME % 2 == 0) symmetry = false;
			else symmetry = true;
			// タイマー管理
			if (arrow_timer >= SYMMETRY_FRAME * 2) arrow_timer = 0;
			++arrow_timer;
		}
		if (!FadeRectangle3::getInstance()->fadeFlg)
		{
	        // 一番最初の press enter
			if (!is_next_string) {
				// 矢印の初期化
				if (TRG(0) & PAD_TRG1) { is_next_string = true; }
			}
			else {
				//--------<選択別処理>--------//
				// ファイル存在時
				if (ifs.is_open()) {
					switch (divergence)
					{
					case 0:// 初期設定
						arrow_pos = { 523.0f, 542.0f };
						arrow_pos_2 = { 888.0f, arrow_pos.y };

						++divergence;
						/*fallthrough*/

					case 1:// チェックポイントから
						if (TRG(0) & PAD_DOWN)   divergence = 2;
						if (TRG(0) & PAD_TRG1) {
							FadeRectangle3::getInstance()->fadeFlg = true; FadeRectangle3::getInstance()->fadeState = 0;
							continueFlg = true;
						}
						break;

					case 2: // 初期設定
						arrow_pos = { 523.0f, 590.0f };
						arrow_pos_2 = { 888.0f, arrow_pos.y };

						++divergence;
						/*fallthrough*/

					case 3:// 初めから
						if (TRG(0) & PAD_UP)     divergence = 0;
						if (TRG(0) & PAD_DOWN)   divergence = 4;
						if (TRG(0) & PAD_TRG1) {
							FadeRectangle3::getInstance()->fadeFlg = true;
							FadeRectangle3::getInstance()->fadeState = 0;
							continueFlg = false;
						}
						break;

					case 4: // 初期設定
						arrow_pos = { 523.0f, 644.0f };
						arrow_pos_2 = { 888.0f, arrow_pos.y };

						++divergence;
						/*fallthrough*/

					case 5:// オプション
						if (TRG(0) & PAD_UP)     divergence = 2;
						if (TRG(0) & PAD_TRG1) {
							isPause = true;
							ScenePause::getInstance()->resetState();
						}
						break;
					}
				}
				else {
					switch (divergence)
					{
					case 0:// 初期設定
						arrow_pos = { 510.0f, 593.0f };
						arrow_pos_2 = { 907.0f, arrow_pos.y };

						++divergence;
						/*fallthrough*/

					case 1:// 初めから
						if (TRG(0) & PAD_DOWN)   divergence = 2;
						if (TRG(0) & PAD_TRG1) {
							FadeRectangle3::getInstance()->fadeFlg = true;
							FadeRectangle3::getInstance()->fadeState = 0;
							continueFlg = false;
						}
						break;

					case 2:// 初期設定
						arrow_pos = { 510.0f, 650.0f };
						arrow_pos_2 = { 907.0f, arrow_pos.y };

						++divergence;
						/*fallthrough*/

					case 3:// オプション
						if (TRG(0) & PAD_UP)     divergence = 0;
						if (TRG(0) & PAD_TRG1) {
							isPause = true;
							ScenePause::getInstance()->resetState();
						}
						break;
					}
				}
				if (playSE && TRG(0) & (PAD_UP | PAD_DOWN))   sound::play(XWB_SOUNDS, XWB_SOUNDS_CHOICE);
				if (playSE && TRG(0) & PAD_TRG1)   sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);
			}
		}
		FadeRectangle3::getInstance()->firstHalfMove(SCENE_STAGE1);
		// 後半の動き
		FadeRectangle3::getInstance()->secondHalfMove(SCENE_NONE);


		break;
	}

#ifdef USE_IMGUI
	ImGui::Begin("title");

	ImGui::SliderFloat2("logo", (float*)&im_pos, -50.0f, 1280.0f);
	ImGui::SliderFloat2("moji", (float*)&im_pos2, -50.0f, 1280.0f);
	ImGui::SliderFloat2("arrow", (float*)&arrow_pos, 0.0f, 1280.0f);

	ImGui::End();
#endif
}

//***************************
//    描画処理
//***************************
void SceneTitle::draw()
{
	GameLib::clear(0, 1.0f, 0);

	title_back_draw();
	fall_light_draw();
	// 矢印(点)
	{
		texture::begin(TEX_TITLE_SELECT);
		texture::draw(TEX_TITLE_SELECT, { arrow_pos },
			{ 1.0f, 1.0f }, { 0.0f, 0.0f }, { 32.0f, 32.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::draw(TEX_TITLE_SELECT, { arrow_pos_2 },
			{ -1.0f, 1.0f }, { 0.0f, 0.0f }, { 32.0f, 32.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(TEX_TITLE_SELECT);
	}
	// タイトルの文字
	if(is_next_string)
	{
		const wchar_t* respawn_file_name = { L"./Data/Files/respawn_data.dat" };
		std::ifstream ifs(respawn_file_name);
		if (ifs.is_open())
		{
			texture::begin(TEX_TITLE_STRING_CON);
			texture::draw(TEX_TITLE_STRING_CON, { 0.0f, 0.0f },
				{ 1.0f, 1.0f }, { 0.0f, 0.0f }, { 1450.0f, 720.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(TEX_TITLE_STRING_CON);
		}
		else
		{
			texture::begin(TEX_TITLE_STRING_NEW);
			texture::draw(TEX_TITLE_STRING_NEW, { 0.0f, 0.0f },
				{ 1.0f, 1.0f }, { 0.0f, 0.0f }, { 1450.0f, 720.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(TEX_TITLE_STRING_NEW);
		}
	}
	else // 確認コマンド
	{
		texture::begin(TEX_CONFIRMATION_COMMAND);
		texture::draw(TEX_CONFIRMATION_COMMAND, { 0.0f, 0.0f },
			{ 1.0f, 1.0f }, { 0.0f, 0.0f }, { 1450.0f, 720.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(TEX_CONFIRMATION_COMMAND);
	}
	// ポーズ画面
	if (isPause)  ScenePause::getInstance()->draw_title_scene();
	// フェードインフェードアウト
	if (FadeRectangle3::getInstance()->fadeFlg)
	{
		texture::begin(RECTANGLE);
		for (int i = 0; i < 11; ++i)
		{
			for (int o = 0; o < 23; ++o)
			{
				FadeRectangle3* fade = FadeRectangle3::getInstance();
				texture::draw(RECTANGLE, { 32.0f + 64.0f * o , 33.0f + 66.0f * i }, { 1.0f, 1.0f },
					{ 0, 0 }, { 64.0f, 66.0f }, { 32.0f, 33.0f }, ToRadian(0), fade->color[o + i * 20]);
			}
		}
		texture::end(RECTANGLE);
	}
}

void SceneTitle::title_back_draw()
{
	const int FRAME = 3;
	const int TOTAL = 84;
	const int now_frame = timer / FRAME % TOTAL;

	texture::begin(now_frame + TITLE_0);
	texture::draw(now_frame + TITLE_0, { 0.0f, 0.0f }, { 1.0f, 1.0f }, { 0.0f, 0.0f },
		{ 1450.0f, 720.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
	texture::end(now_frame + TITLE_0);

	if (now_frame == TOTAL - 1) { timer = 55 * FRAME; }
}

void SceneTitle::fall_light_draw()
{
	const int FRAME = 3;
	const int TOTAL = 25;
	static int fall_light_timer = 0;
	const int now_frame = fall_light_timer / FRAME % TOTAL;

	texture::begin(now_frame + FALL_LIGHT_1);
	texture::draw(now_frame + FALL_LIGHT_1, { 0.0f, 0.0f }, { 1.0f, 1.0f }, { 0.0f, 0.0f },
		{ 1450.0f, 720.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
	texture::end(now_frame + FALL_LIGHT_1);

	++fall_light_timer;
	if (now_frame == TOTAL - 1) { fall_light_timer = 10 * FRAME; }
}
