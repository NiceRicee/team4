#pragma once

#include "component.h"

class BaseAnimeObject
{
public:
    std::vector<VECTOR2> anime_pos;
protected:
    //--------<関数>--------

    //--------<変数>--------
    std::vector<VECTOR2> anime_scale;
    std::vector<VECTOR2> anime_texpos;
    std::vector<VECTOR2> anime_texsize;
    std::vector<VECTOR2> anime_pivot;
    std::vector<float>   anime_angle;
    std::vector<VECTOR4> anime_color;

    VECTOR2 center;
    int timer;
    int anime_switch;
    int anime_case;     // アニメ管理用変数(switch分で使う)
private:
    // 画像ありアニメーション関連
    int     anime_state;
    int     anime_c;        // アニメが現在何コマ目か
    int     anime_timer;    // アニメ用タイマー
protected:
    //--------<コンストラクタ>--------
    BaseAnimeObject(VECTOR2 c);
private:
    //--------<関数>--------
    // アニメ初期設定
    void animeInit(int animeNo);
    // アニメ遷移
    bool animeFlow(int frame, int total, bool loop);
protected:
    // アニメーション
    // 返り値がtrue:アニメの終点に達した(この機能が使えるのはアニメーションがループしないときだけ)
    bool animation(const animeData& animeData);
    // アニメの設定
    void animeSet(int state);
public:
    virtual bool anime_movement() = 0;
    virtual void anime_draw() = 0;
    int anime_pos_size() { return (int)anime_pos.size(); }
};
