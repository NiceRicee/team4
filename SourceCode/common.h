#ifndef COMMON_H
#define COMMON_H

//シーンのラベル
enum
{
	SCENE_NONE,
	SCENE_TITLE,
	SCENE_STAGE1,
	SCENE_CLEAR,
};

//ステージのラベル
enum
{
	STAGE_1,
	STAGE_2,
};

//画面の大きさ
#define SCREEN_W   1450
#define SCREEN_H    720
// 焚火の数
#define BONFIRE_NUM   24

extern int nextScene;

#endif // COMMON_H
