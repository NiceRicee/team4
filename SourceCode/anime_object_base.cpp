#include "all.h"
#include "anime_object_base.h"

BaseAnimeObject::BaseAnimeObject(VECTOR2 c)
    : center(c)
    , timer(0)
    , anime_switch(0)
    , anime_case(0)
    , anime_state(0)
    , anime_c(0)
    , anime_timer(0) {}

//--------------------------------------------------------------
//アニメ初期設定
//--------------------------------------------------------------
void BaseAnimeObject::animeInit(int animeNo)
{
    anime_c = anime_timer = 0;
    for (int i = 0; i < (int)anime_texpos.size(); ++i) {
        anime_texpos[i] = { 0, animeNo * anime_texsize[i].y };
    }
}

//--------------------------------------------------------------
//  アニメ遷移
//--------------------------------------------------------------
bool BaseAnimeObject::animeFlow(int frame, int total, bool loop)
{
    anime_c = anime_timer / frame;
    if (loop)
    {
        if (anime_c >= total)
        {
            anime_c = 0;
            anime_timer = 0;
        }
    }
    else
    {
        if (anime_c >= total)
        {
            anime_c = total - 1;
            return true;    //アニメの終端に達した
        }
    }
    for (int i = 0; i < (int)anime_texpos.size(); ++i) {
        anime_texpos[i].x = anime_c * anime_texsize[i].x;
    }
    ++anime_timer;

    return false;           //アニメの終端ではない
}

//*******************************
//    アニメーション
//*******************************
bool BaseAnimeObject::animation(const animeData& animeData)
{
    switch (anime_state)
    {
    case 0:
        animeInit(animeData.animeNo);

        ++anime_state;
        /*fallthrough*/

    case 1:
        // アニメーション
        return (animeFlow(animeData.frame, animeData.total, animeData.isLoop));

    }
    return false;
}

//*******************************
//    アニメーションの設定
//*******************************
void BaseAnimeObject::animeSet(int state)
{
    anime_case = state;
    anime_state = 0;
}