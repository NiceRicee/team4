#pragma once

#include "Template.h"


class FadeRectangle3 : public Singleton<FadeRectangle3>
{
private:
    //--------<定数>--------
    static const int INDEX = 253;

    //--------<変数>--------
    int values[INDEX];
    int size = sizeof(values) / sizeof(int);

    //--------<関数>--------
    void shuffle(int array[], int size);  // 決められた数列の中から重複しない乱数

public:
    FadeRectangle3()
    {
        for (int i = 0; i < INDEX; ++i)
        {
            values[i] = i;
            color[i] = { 0.0f, 0.0f, 0.0f, 0.0f };
        }
    }

    //--------<変数>--------
    VECTOR4 color[INDEX]{};

    bool fadeFlg{ false };
    int fadeState{};
    int timer{};

    //--------<関数>--------
    void firstHalfMove(int nextScene);
    void secondHalfMove(int nextScene);
};

