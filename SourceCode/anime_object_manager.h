#pragma once

#include "anime_object_base.h"

class AnimeObjectManager
{
public:
    //--------<コンストラクタ/関数等>--------
    AnimeObjectManager(BaseAnimeObject* ani);

    BaseAnimeObject& get_anime_object_manager() const { return *anime_object_manager; }

private:
    unique_ptr<BaseAnimeObject> anime_object_manager;
};
