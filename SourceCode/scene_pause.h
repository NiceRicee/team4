#pragma once

//--------< extern >--------//
extern bool playEffect;
extern bool playBGM;
extern bool playSE;


class ScenePause : public Singleton<ScenePause>
{
private:
    //--------<定数>--------
    static const int BOX_NUM = 30;
    static const int SLIDER_NUM = 2;
    //--------<変数>--------
    bool stageBackFlg;

    // メニュー
    int menuType;
    int boxTypeX[4][4] = {
        1,1,1,1,
        1,1,1,1,
        1,1,1,1,
        1,1,1,1,
    };
    int old_boxTypeX[4][4] = {
        1,1,1,1,
        1,1,1,1,
        1,1,1,1,
        1,1,1,1,
    };
    int boxTypeY;
    int sub;

    std::vector<VECTOR2> Im_pos;
    std::vector<VECTOR2> Im_size;
    std::vector<VECTOR4> Im_color;

    VECTOR2 pos[BOX_NUM]{};
    VECTOR2 size[BOX_NUM]{};
    VECTOR2 pivot[BOX_NUM]{};
    VECTOR4 color[BOX_NUM]{};

    float sliderSizeX[2]{ 180.0f, 180.0f }; // [0] : BGM  [1] : SE
    float old_sliderSizeX[2]{ 180.0f, 180.0f }; // [0] : BGM  [1] : SE

    // ファイル操作系
    const wchar_t* Filename        = { L"./Data/Files/MenuSaveData.dat" };
    const wchar_t* Filename_Slider = { L"./Data/Files/SliderData.dat" };

    ifstream input_File;
    ofstream output_File;

    //--------カーソル関連--------//
    // カーソル
    VECTOR2 cursolPos;
    // マウス
    POINT point;
    VECTOR2 pointerPos;
    VECTOR2 oldPointerPos;
    bool doUseMause;
    int conState;
    // スティック
    VECTOR2 leftStick;          // コントローラー左スティック用変数
    VECTOR2 rightStick;         // コントローラー右スティック用変数

    // デバイス認識関連
    enum device
    {
        KEYBOARD,
        CONTROLLER,
    };

    int nowDevice;

    int state;
    int timer;
public:
    void resetState() { state = 0; }
    bool getStageBackFlg() { return stageBackFlg; }

    // ファイル操作関連
    void AdaptOptions();  // 変更された項目の適応
    void loadData();
private:
    void saveData();
    // 共通のコード
    void imGui_context();
    void parameters_init();
    void device_relation();
    void menuType_selection_process(int min, int max, int conditional);
    void boxTypeY_selection_process(int min, int max);
    void boxTypeX_selection_process(int min, int max);
    void slider_process(int min, int max);
    void draw_common();

public:
    //--------< コンストラクタ/関数等 >--------
    ScenePause();
    void update_game_scene();
    void draw_game_scene();
    void update_title_scene();
    void draw_title_scene();
    ~ScenePause();
};