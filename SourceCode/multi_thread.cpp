#include "all.h"
#include "multi_thread.h"

MultiThread::MultiThread()
{
	clear();
}

MultiThread::~MultiThread(){}

//-------------------------------------------------------
//  変数のクリア
//-------------------------------------------------------
void MultiThread::clear()
{
	state = 0;
	timer = 0;
	angle = 0;
	string_color_a = 1.0f;
}
//-------------------------------------------------------
//  テクスチャのロード(タイトル)
//-------------------------------------------------------
void MultiThread::thread_texture_load_title(bool& is_load)
{
	texture::load(loadTexture_title);
	texture::load(loadTexture_fade);
	texture::load(loadTexture_pause);

	is_load = true;
}
//-------------------------------------------------------
//  テクスチャのロード(ゲーム画面)
//-------------------------------------------------------
void MultiThread::thread_texture_load_game(bool& is_load)
{
	texture::load(loadTexture);
	//texture::load(loadTexture_fade);
	//texture::load(loadTexture_pause);
	texture::load(loadTexture_mask);

	is_load = true;
}
//-------------------------------------------------------
//  テクスチャのロードしてる間出す画面(タイトル)
//-------------------------------------------------------
void MultiThread::thread_update_now_loading(bool& is_load, VECTOR3 clear_color)
{
	while (!is_load)
	{
		switch (state)
		{
		case 0: /*初期設定*/
			GameLib::setBlendMode(Blender::BS_ALPHA);

			++state;
			/*fallthrough*/

		case 1:/*通常時*/
			//--------<更新>--------//
			update_common();

			//--------<描画>--------//
			draw_common(clear_color);

			break;
		}
		++timer;
	}
}
//-------------------------------------------------------
//  update(共通部分)
//-------------------------------------------------------
void MultiThread::update_common()
{
	// アイコンを回す
	switch (timer / 40 % 4)
	{
	case 1:
		angle += 4.5f;
		if (angle >= 180.0f) angle = 180.0f;
		break;
	case 3:
		angle += 4.5f;
		if (angle >= 360.0f) angle = 0.0f;
		break;
	}
	// 文字を点滅させる
	switch (timer / 40 % 2)
	{
	case 0:
		string_color_a -= 0.03f;
		if (string_color_a < 0.0f) string_color_a = 0.0f;
		break;
	case 1:
		string_color_a += 0.03f;
		if (string_color_a > 1.0f) string_color_a = 1.0f;
		break;
	}
}
//-------------------------------------------------------
//  draw(共通部分)
//-------------------------------------------------------
void MultiThread::draw_common(VECTOR3 clear_color)
{
	GameLib::clear(clear_color.x, clear_color.y, clear_color.z);

	texture::begin(THREAD_LOADING);
	texture::draw(THREAD_LOADING, { 1300.0f, 550.0f },
		{ 0.5f, 0.5f }, { 0.0f, 0.0f }, { 256.0f, 256.0f }, { 128.0f, 128.0f }, ToRadian(angle), { 1, 1, 1, 1 });
	texture::end(THREAD_LOADING);

	//文字の表示
	font::textOut(5, "now loading", { 1300.0f, 650.0f }, { 0.15f,0.15f },
		{ 0.8f, 0.9f, 0.0f, string_color_a }, TEXT_ALIGN::MIDDLE);


	//デバッグ用文字列の表示
	debug::display(1, 0, 0, 1, 1);
	// 画面を描画する(これが最後になるように)
	GameLib::present(1, 0);
}