#pragma once

// アニメデータ
struct animeData
{
    int animeNo;   // アニメ番号(texPos.y)を決める
    int frame;     // アニメ一個分のフレーム
    int total;     // アニメのトータル数
    bool isLoop;   // ループするか  true:ループ有  false:ループ無
};

