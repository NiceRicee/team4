#pragma once
//******************************************************************************
//
//
//      OBJ2Dクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <list>
#include "../GameLib/vector.h"
#include "../GameLib/obj2d_data.h"

// 前方宣言
class OBJ2D;
extern bool hantenFlg;
extern bool back_stage_hanten; //前のステージに戻った時に反転させる
//==============================================================================
//
//      OBJ2Dクラス
//
//==============================================================================

class OBJ2D
{
public:
    // メンバ変数
    VECTOR2                 position;           // 位置
    VECTOR2                 scale;              // スケール
    VECTOR2                 texPos;             // 元画像距離
    VECTOR2                 texSize;            // 元画像サイズ
    float                   angle;              // 角度
    VECTOR2                 pivot;              // 基準点
    VECTOR4                 color;              // 描画色

    VECTOR2                 size;               // あたり用サイズ（縦横）
    VECTOR2                 hitbox;
    int hp;

    VECTOR2                 speed;              // 瞬間の移動量ベクトル
    int                     state;              // ステート
    int                     timer;              // タイマー

    //int                     jumpTimer;          // 長押しジャンプタイマー

    int                 objAct;        // 行動遷移用
    int                 animeC;        // アニメが現在何コマ目か
    int                 animeTimer;    // アニメ用タイマー
    int                 jumpTimer;
   

    //bool hit;                       //敵に対しての攻撃の当たり判定
    //bool hitW;                       //敵に対しての攻撃の当たり判定
    //bool hitH;                       //敵に対しての攻撃の当たり判定
    VECTOR2 movetimer;              //移動する時間

public:
    // メンバ関数
    OBJ2D();        // コンストラクタ
    void clear();   // メンバ変数のクリア

     //アニメ初期設定
    void animeInit(int animeNo);
    //  アニメ遷移
    bool anime(int frame, int total, bool loop);

    ~OBJ2D();

};
