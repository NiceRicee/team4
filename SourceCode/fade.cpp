#include "all.h"


void FadeRectangle3::firstHalfMove(int NextScene)
{
	const float ADD_COLORW = 0.6f;
	const float SUB_COLORW = -0.6f;
	const int CONDITION    = 23;
	const int REMAIN       = 11;
	const int INTERVAL     = 2;

	assert(CONDITION * REMAIN == 253);

	if (fadeFlg)
	{
		switch (fadeState)
		{
		case 0: // 初期設定
			timer = 0;
			shuffle(values, size);

			++fadeState;
			/*fallthrough*/

		case 1:
			for (int i = 0; i < CONDITION * (timer / INTERVAL % REMAIN + 1); ++i)
			{
				if (color[values[i]].w >= 1.0f) continue;

				color[values[i]].w += ADD_COLORW;
				if (color[values[i]].w > 1.0f) { color[values[i]].w = 1.0f; }

				if (i == CONDITION * REMAIN - 1 && color[values[CONDITION * REMAIN - 1]].w >= 1.0f) { ++fadeState; nextScene = NextScene; }
			}
			++timer;

			break;

		case 6: // 初期設定
			timer = 0;
			shuffle(values, size);

			++fadeState;
			/*fallthrough*/

		case 7:
			for (int i = 0; i < CONDITION * (timer / INTERVAL % REMAIN + 1); ++i)
			{
				if (color[values[i]].w <= 0.0f) continue;

				color[values[i]].w += SUB_COLORW;
				if (color[values[i]].w < 0.0f) { color[values[i]].w = 0.0f; }

				if (i == CONDITION * REMAIN - 1 && color[values[CONDITION * REMAIN - 1]].w <= 0.0f) { ++fadeState; fadeFlg = false; }
			}
			++timer;

			break;
		}
	}
}

void FadeRectangle3::secondHalfMove(int NextScene)
{
	const float ADD_COLORW = 0.6f;
	const float SUB_COLORW = -0.6f;
	const int CONDITION    = 23;
	const int REMAIN       = 11;
	const int INTERVAL     = 2;

	assert(CONDITION * REMAIN == 253);

	if (fadeFlg)
	{
		switch (fadeState)
		{
		case 2: // 初期設定
			timer = 0;
			shuffle(values, size);

			++fadeState;
			/*fallthrough*/

		case 3:
			for (int i = 0; i < CONDITION * (timer / INTERVAL % REMAIN + 1); ++i)
			{
				if (color[values[i]].w <= 0.0f) continue;

				color[values[i]].w += SUB_COLORW;
				if (color[values[i]].w < 0.0f) { color[values[i]].w = 0.0f; }

				if (i == CONDITION * REMAIN - 1 && color[values[CONDITION * REMAIN - 1]].w <= 0.0f) { ++fadeState; fadeFlg = false; }
			}
			++timer;

			break;

		case 4: // 初期設定
			timer = 0;
			shuffle(values, size);

			++fadeState;
			/*fallthrough*/

		case 5:
			for (int i = 0; i < CONDITION * (timer / INTERVAL % REMAIN + 1); ++i)
			{
				if (color[values[i]].w >= 1.0f) continue;

				color[values[i]].w += ADD_COLORW;
				if (color[values[i]].w > 1.0f) { color[values[i]].w = 1.0f; }

				if (i == CONDITION * REMAIN - 1 && color[values[CONDITION * REMAIN - 1]].w >= 1.0f) { ++fadeState;   nextScene = NextScene; }
			}
			++timer;

			break;
		}
	}
}

void FadeRectangle3::shuffle(int array[], int size)
{
	int i = size;
	while (i > 1) {
		int j = rand() % i;
		i--;
		int t = array[i];
		array[i] = array[j];
		array[j] = t;
	}
}