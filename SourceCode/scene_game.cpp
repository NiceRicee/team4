#include "all.h"
#include "anime_object_behavior.h"
bool hantenFlg = false;
bool back_stage_hanten = false;

void SceneGame::moveArea()
{
	VECTOR2 plPos = playerManager()->position;

	if (plPos.y > 750)
	{
		switch (bgManager()->mapStatus)
		{
		case true:
			--bgManager()->areaNum;
			// 焚火の再生成
			bonfire_manager.reset(new Bonfire(bonfire_pos[bgManager()->areaNum]));
			back_stage_hanten = true;
			if (bgManager()->areaNum < 0) bgManager()->areaNum = 0;
			//max(bgManager()->areaNum, 0);
			break;

		case false:
			++bgManager()->areaNum;
			// 焚火の再生成
			bonfire_manager.reset(new Bonfire(bonfire_pos[bgManager()->areaNum]));
			if (bgManager()->areaNum > AREANUMMAX) bgManager()->areaNum = AREANUMMAX;

			//min(bgManager()->areaNum, 2);
			break;
		}
		plPos.y = 0.0f;


		state = 1;
		respawnState = 2;
	}


#if 1
	//TODO !!!!デバック用!!!!ステージ変更！！！！
	if (TRG(0) & PAD_R2)
	{
		--bgManager()->areaNum;
		// 焚火の再生成
		bonfire_manager.reset(new Bonfire(bonfire_pos[bgManager()->areaNum]));
		if (bgManager()->areaNum < 0) bgManager()->areaNum = 0;


		state = 1;
		respawnState = 10;

		bgManager()->mapStatus = false;
	}
	if (TRG(0) & PAD_R1)
	{
		++bgManager()->areaNum;
		// 焚火の再生成
		bonfire_manager.reset(new Bonfire(bonfire_pos[bgManager()->areaNum]));
		if (bgManager()->areaNum > AREANUMMAX) bgManager()->areaNum = AREANUMMAX;


		state = 1;
		respawnState = 10;

		bgManager()->mapStatus = false;
	}
#endif // 1

}

void SceneGame::respawn()
{
	switch (respawnState)
	{
	case 0: // 初めから
		operateFile.respawn_case = 0;
		operateFile.adapt_options();
		break;
	case 1: // 最後に触れたチェックポイントから
		operateFile.load_data();
		operateFile.adapt_options();
		continueFlg = false;
		break;

	case 2: // 通常プレイで次のステージ移動した時
		playerManager()->position.y = 0.0f;
		break;

	case 10:  // !!!デバック用!!!　respawnState = 10; にしていいのはmoveArea()内のデバック用のところだけ!!!!!
		playerManager()->position = bonfire_pos[bgManager()->areaNum];
		playerManager()->position = { 640,30 };

		break;

	}
}