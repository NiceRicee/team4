#ifndef	SCENE_TITLE_H
#define	SCENE_TITLE_H

class SceneTitle :public Scene
{
private:
    //--------<変数>--------
    int state = 0;
    int timer = 0;

    int divergence = 0;
    VECTOR2 arrow_pos;
    VECTOR2 arrow_pos_2;

    // ポーズ画面関連
    bool isPause               = false;
    bool is_next_string        = false;

    //--------<関数>--------
    void title_back_draw();
    void fall_light_draw();

public:
    //------< インスタンス >---------------------------------------------------------
    static SceneTitle* instance() { return &instance_; }
    //--------<コンストラクタ/関数等>-------
    void init();
    void deinit();
    void update();
    void draw();

    static SceneTitle instance_;
};

#endif//SCENE_TITLE_H