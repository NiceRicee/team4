#pragma once

//-------------------------------------------------------
//   マルチスレッド
//   (今回はスレッド間で共有して読み書きされる変数が
// 　ないので排他制御はしない)
//-------------------------------------------------------
class MultiThread
{
public:
    //--------< コンストラクタ/関数等 >--------//
    MultiThread();
    ~MultiThread();
    // 変数のクリア
    void clear();
    // スレッド
    void thread_texture_load_title(bool& is_load);                        //  テクスチャのロード(タイトル)
    void thread_texture_load_game(bool& is_load);                         //  テクスチャのロード(ゲーム画面)
    void thread_update_now_loading(bool& is_load, VECTOR3 clear_color);   //  テクスチャのロードしてる間出す画面(タイトル)

private:
    //--------< 変数 >--------//
    int state;
    int timer;
    float angle;
    float string_color_a;

    //--------< 関数 >--------//
    // 共通部分
    void update_common();
    void draw_common(VECTOR3 clear_color);
};