#ifndef  SCENE_STAGE1_H
#define	 SCENE_STAGE1_H

#include <list>
#include "anime_object_behavior.h"
#include "anime_object_manager.h"

class SceneGame :public Scene
{
public:
   void resetState() { state = 0; }
   void moveArea();
   void respawn();


public:
    //--------<コンストラクタ/関数等>--------
    void stage1_init();
    void stage1_deinit();
    void stage1_update();
    void stage1_draw();
    void stage1_draw_back();
    void stage1_draw_kako_up();
    void stage1_draw_kako_down();

public:
    //------< インスタンス >---------------------------------------------------------
    static SceneGame* instance() { return &instance_; }
    BG* bgManager() { return bgManager_; }
    Player* playerManager() { return playerManager_; }
    UI* UIManager() { return UIManager_; }
    //BaseAnimeObject& get_bonfire_manager() const { return *bonfire_manager; }

    VECTOR2 bonfire_pos[BONFIRE_NUM]{
        {535.0f,  145.0f},    {-500.0f,-500.0f},    {-500.0f,-500.0f},    {464.0f,560.0f},
        {-500.0f, -500.0f},   {-500.0f,-500.0f},    {-500.0f,-500.0f},    {560.0f,592.0f},
        {-500.0f, -500.0f},   {-500.0f, -500.0f},   {70.0f,528.0f},       {-500.0f, -500.0f},
        {790.0f,  432.0f},    {-500.0f,-500.0f},    {-500.0f,-500.0f},    {-500.0f,-500.0f},
        {670.0f,  79.0f},     {-500.0f,-500.0f},    {640.0f,48.0f},       {-500.0f,-500.0f},
        {640.0f,  368.0f},    {-500.0f,-500.0f},    {-500.0f,-500.0f},    {-500.0f,-500.0f},
    };

    int respawnState;
    int state;

private:
    //----< 構造体 >----//
    struct VECTOR4_INT
    {
        int x = 0;
        int y = 0;
        int z = 0;
        int w = 0;
    };
    //--------<関数>--------//
    void bgm_play(VECTOR4_INT area_num, int bgm_index);
    void inversion_mask_draw();
    void dying_in_fall(); // 落下死
    void game_clear();

    //--------<変数>-------
    // 変数
    int timer;
    CheckPoint operateFile;             //checkupoint通過時のファイル操作

    VECTOR2 pos;

    // ポーズ画面関連
    bool isPause = false;
    bool ShowSceneGame = false;
    float kakouScale;

    // クリア画面関連
    bool do_start_blackout;
    float trans_color_w;

    // 落下死時の処理関連
    bool do_start_fall_blackout;
    float trans_fall_color_w;
    int fall_blackout_timer;

private:
    BG* bgManager_;
    Player* playerManager_;
    UI* UIManager_;
    static SceneGame instance_;
    unique_ptr<Bonfire> bonfire_manager;
    unique_ptr<SignBoard> signBorad_manager;
};

#endif //  SCENE_STAGE1_H