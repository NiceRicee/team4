#include "all.h"
#include "checkPoint.h"

void CheckPoint::load_data()
{
    inputFile.open(respawn_file_name);
    if (respawn_file_name)
    {
        inputFile.read((char*)&respawn_case, sizeof(int) * 1);
        inputFile.close();
    }
    else
    {
        respawn_case = 0;
    }
}

void CheckPoint::save_data()
{
    outputFile.open(respawn_file_name);
    if (outputFile) {
        outputFile.write((char*)&respawn_case, sizeof(int) * 1);
        outputFile.close();
    }
    else {
        //もしミスったら
    }

}

void CheckPoint::adapt_options()
{
    SceneGame::instance()->playerManager()->position = SceneGame::instance()->bonfire_pos[respawn_case] + VECTOR2(50.0f, 45.0f);
    SceneGame::instance()->bgManager()->areaNum = respawn_case;
}
