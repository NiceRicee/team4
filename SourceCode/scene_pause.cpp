//******************************************************************************
//
//
//		PAUSE_CPP
//
//
//******************************************************************************

#include "all.h"
#include "user.h"
#include "audio.h"
#include "scene_pause.h"

//--------<変数>--------//
bool playEffect = true;
bool playBGM    = true;
bool playSE     = true;

// 変更された項目の変更
void ScenePause::AdaptOptions()
{
	debug::setString("!!!!!!!!!!!AdaptOptions!!!!!!!!!!!!");
	/* GAME */
	if (boxTypeX[0][0] == 1) playEffect = true;
	if (boxTypeX[0][0] == 2) playEffect = false;
	/* SOUND */
	if (boxTypeX[1][0] == 1) playBGM = true;
	if (boxTypeX[1][0] == 2) playBGM = false;
	if (boxTypeX[1][1] == 1) playSE = true;
	if (boxTypeX[1][1] == 2) playSE = false;

	// BGM の停止、再生
	if (!playBGM) {
		music::pause(BGM_TITLE);
		music::pause(BGM_STAGE1);
		music::pause(BGM_STAGE2);
		music::pause(BGM_STAGE3);
		music::pause(BGM_STAGE4);
		music::pause(BGM_STAGE5);
		music::pause(BGM_STAGE6);
		music::pause(BGM_END);
	}
	else {
		music::resume(BGM_TITLE);
		music::resume(BGM_STAGE1);
		music::resume(BGM_STAGE2);
		music::resume(BGM_STAGE3);
		music::resume(BGM_STAGE4);
		music::resume(BGM_STAGE5);
		music::resume(BGM_STAGE6);
		music::resume(BGM_END);
	}
	// スライダーによるBGMの音量設定
	music::setVolume(BGM_TITLE,   (sliderSizeX[0] / 180.0f) * 0.2f);
	music::setVolume(BGM_STAGE1,  (sliderSizeX[0] / 180.0f) * 0.1f);
	music::setVolume(BGM_STAGE2,  (sliderSizeX[0] / 180.0f) * 0.1f);
	music::setVolume(BGM_STAGE3,  (sliderSizeX[0] / 180.0f) * 0.1f);
	music::setVolume(BGM_STAGE4,  (sliderSizeX[0] / 180.0f) * 0.1f);
	music::setVolume(BGM_STAGE5,  (sliderSizeX[0] / 180.0f) * 0.1f);
	music::setVolume(BGM_STAGE6,  (sliderSizeX[0] / 180.0f) * 0.1f);
	music::setVolume(BGM_END,     (sliderSizeX[0] / 180.0f) * 0.1f);
	// スライダーによるSEの音量設定
	sound::setVolume(XWB_SOUNDS, XWB_SOUNDS_DESION,   (sliderSizeX[1] / 180.0f) * 0.5f);
	sound::setVolume(XWB_SOUNDS, XWB_SOUNDS_LANDING,  (sliderSizeX[1] / 180.0f) * 0.3f);
	sound::setVolume(XWB_SOUNDS, XWB_SOUNDS_CHOICE,   (sliderSizeX[1] / 180.0f) * 0.5f);
}
// データ読み込み
void ScenePause::loadData()
{
	input_File.open(Filename);
	if (Filename) {
		input_File.read((char*)&boxTypeX, sizeof(int) * 16);
		input_File.close();
	}
	else {
		for (int i = 0; i < 4; ++i) {
			for (int o = 0; o < 4; ++o) {
				boxTypeX[i][o] = 1;
			}
		}
	}

	input_File.open(Filename_Slider);
	if (Filename_Slider) {
		input_File.read((char*)&sliderSizeX, sizeof(float) * SLIDER_NUM);
		input_File.close();
	}
	else {
		for (int i = 0; i < SLIDER_NUM; ++i) {
			sliderSizeX[i] = 180.0f;
		}
	}
}
// データ保存
void ScenePause::saveData()
{
	output_File.open(Filename);
	if (output_File) {
		output_File.write((char*)&boxTypeX, sizeof(int) * 16);
		output_File.close();
	}
	else {
		//もしミスったら
	}

	output_File.open(Filename_Slider);
	if (output_File) {
		output_File.write((char*)&sliderSizeX, sizeof(float) * SLIDER_NUM);
		output_File.close();
	}
	else {
		//もしミスったら
	}
}
// ImGui
void ScenePause::imGui_context()
{
#ifdef USE_IMGUI
	// menu bar
	static bool is_show_boxTypeX = false;
	static bool is_show_boxCreate = false;
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("ImGui Windows"))
		{
			ImGui::MenuItem("boxTypeX", NULL, &is_show_boxTypeX);
			ImGui::MenuItem("boxCreate", NULL, &is_show_boxCreate);
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}
	// boxTypeX
	if(is_show_boxTypeX)
	{
		ImGui::Begin("boxTypeX");

		ImGui::BeginChild(ImGui::GetID((void*)0), ImVec2(300, 120), ImGuiWindowFlags_NoTitleBar);
		for (int i = 0; i < 4; ++i)
		{
			std::stringstream ss_menuType;  ss_menuType << "menuType " << i;
			char char_array_menuType[256];  ss_menuType.get(char_array_menuType, 256); // stringstream 型から char 型への変換
			if (ImGui::TreeNode(char_array_menuType)) {
				for (int o = 0; o < 4; ++o)
				{
					std::stringstream ss;  ss << "boxTypeX[" << i << "][" << o << "]";
					char char_array[256];  ss.get(char_array, 256); // stringstream 型から char 型への変換

					ImGui::InputInt(char_array, &boxTypeX[i][o]);
				}
				ImGui::TreePop();
			}
		}
		ImGui::EndChild();

		ImGui::End();
	}
	// boxCreate
	if (is_show_boxCreate)
	{
	    // box 詳細
		{
			ImGui::Begin("Box Details");
			const char* desc = u8"作ったボックスの詳細が確認できます";
			HelpMarker(desc);
			ImGui::BeginChild(ImGui::GetID((void*)0), ImVec2(300, 120), ImGuiWindowFlags_NoTitleBar);
			for (int i = 0; i < (int)Im_pos.size(); ++i)
			{
				std::stringstream ss;  ss << "box " << i + 1;
				char char_array[256];  ss.get(char_array, 256); // stringstream 型から char 型への変換
				if (ImGui::TreeNode(char_array)) {
					ImGui::SliderFloat2("pos", (float*)&Im_pos[i], 0.0f, 1000.0f);
					ImGui::SliderFloat2("size", (float*)&Im_size[i], 0.0f, 1000.0f);
					ImGui::SliderFloat("colorW", (float*)&Im_color[i].w, 0.0f, 1.0f);
					ImGui::ColorEdit3("color", (float*)&Im_color[i]);

					ImGui::TreePop();
				}
			}
			ImGui::EndChild();
		}
		ImGui::End();
		// box 制作
		{
			ImGui::Begin("Box Create");

			static bool isCreate = false;
			static int mode = 0;
			static int im_timer = 0;
			static std::vector<int> items;

			ImGui::Checkbox("isCreate", &isCreate);  ImGui::SameLine();
			const char* desc = u8"ボックスを作成できます";
			HelpMarker(desc);

			if (isCreate) {
				if (ImGui::Button("add")) {
					mode = (int)items.size();  // addを押したときは一番下に自動選択
					items.push_back(0);
					Im_pos.push_back({});
					Im_size.push_back({});
					Im_color.push_back({ (float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX, 0.8f });
				}
				ImGui::SameLine();
				if (ImGui::Button("remove")) {
					if (!items.empty()) {
						if (mode == (int)items.size() - 1)  --mode;  // removeを押したときに一番下が選択されていれば一つ上に自動選択
						items.pop_back();
						Im_pos.pop_back();
						Im_size.pop_back();
						Im_color.pop_back();
					}
				}

				ImGui::BeginChild(ImGui::GetID((void*)0), ImVec2(300, 120), ImGuiWindowFlags_NoTitleBar);
				for (int i = 0; i < (int)items.size(); ++i) {
					std::stringstream ss;  ss << "box " << i + 1;
					char char_array[256];  ss.get(char_array, 256);
					ImGui::RadioButton(char_array, &mode, i);
				}
				ImGui::EndChild();

				if (!items.empty()) {
					// 箱をマウス操作で制作する
					if (TRG(0) & MOUSE_RIGHT_CLICK)   Im_pos[mode] = pointerPos;
					if (STATE(0) & MOUSE_RIGHT_CLICK) Im_size[mode] = pointerPos - Im_pos[mode];
					// 選択された箱を点滅させる
					if (im_timer / 80 % 2 == 1) Im_color[mode].x = 0.7f;
					else Im_color[mode].x = 0.5f;
					++im_timer;  if (im_timer > 160)  im_timer = 0;
				}
			}
			ImGui::End();
		}
	}
#endif
}
// 共通パラメータ初期化
void ScenePause::parameters_init()
{
	timer = 0;
	stageBackFlg = false;
	// カーソル関連のパラメータの初期化
	cursolPos = {};
	point = {};
	pointerPos = {};
	oldPointerPos = {};
	doUseMause = false;
	conState = 0;

	// スティック関連のパラメータの初期化
	leftStick = {};
	rightStick = {};

	// メニュー関連のパラメータの初期化
	sub = 13;

	// 配置用変数
	for (int i = 0; i < BOX_NUM; ++i)
	{
		color[i] = { 1,1,1,1 };
	}
}
// デバイス関連
void ScenePause::device_relation()
{
	//-----カーソルの座標取得やデバイスの切り替え--------------------------------------------------------------------
	{
		// マウス
		GetCursorPos(&point);												// スクリーン座標を取得する
		ScreenToClient(window::getHwnd(), &point);                          // クライアント座標に変換する
		oldPointerPos = pointerPos;
		pointerPos.x = (float)point.x + 1.0f;
		pointerPos.y = (float)point.y + 1.0f;
		if (oldPointerPos != pointerPos) { doUseMause = true; nowDevice = KEYBOARD; }
		else doUseMause = false;
		// スティック
		leftStick = { input::getPadState(0)->leftX, -input::getPadState(0)->leftY };
		if (vec2Length(leftStick) > 1.0f)  leftStick = vec2Normalize(leftStick);
		rightStick = { input::getPadState(0)->rightX, -input::getPadState(0)->rightY };
		if (vec2Length(rightStick) > 1.0f)  rightStick = vec2Normalize(rightStick);

		if (leftStick.x > 0.2f || leftStick.x < -0.2f)    nowDevice = CONTROLLER;
		if (leftStick.y > 0.2f || leftStick.y < -0.2f)    nowDevice = CONTROLLER;
		if (rightStick.x > 0.2f || rightStick.x < -0.2f)  nowDevice = CONTROLLER;
		if (rightStick.y > 0.2f || rightStick.y < -0.2f)  nowDevice = CONTROLLER;
	}
	//------------------------------------------------------------------------------------------------------------
	// デバイス別の処理
	switch (nowDevice)
	{
	case KEYBOARD:
		// カーソルの位置
		cursolPos.x = (float)point.x + 1.0f;
		cursolPos.y = (float)point.y + 1.0f;
		//　カーソルの範囲
		cursolPos.x = clamp(cursolPos.x, 0.0f, SCREEN_W);
		cursolPos.y = clamp(cursolPos.y, 0.0f, SCREEN_H);
		break;

	case CONTROLLER:
		// カーソルの位置
		cursolPos += leftStick * 5.0f;
		//　カーソルの範囲
		cursolPos.x = clamp(cursolPos.x, 0.0f, SCREEN_W);
		cursolPos.y = clamp(cursolPos.y, 0.0f, SCREEN_H);
		break;
	}

	//---debug---//
	//debug::setString("doPushKey:%d", doPushKey());
	//debug::setString("doPushButton:%d", doPushButton());
	//debug::setString(" ");
	//debug::setString(" ");
	//debug::setString(" ");
	//debug::setString("nowDevice:%d", nowDevice);
	//debug::setString("doUseMause:%d", doUseMause);

	// マウスの操作
	switch (conState)
	{
	case 0:
		SetCursorPos((int)pointerPos.x, (int)pointerPos.y);
		SetCursorPos((int)cursolPos.x, (int)cursolPos.y);

		++conState;
		/*fallthrough*/

	case 1:
		//ShowCursor(false);     //カーソルを非表示にする

		break;
	}
}
// menuTypeの選択処理
void ScenePause::menuType_selection_process(int min, int max, int conditional)
{
	/*------移動処理(キーボード)------*/
	if (TRG(0) & PAD_R2) {
		if (menuType != max) { ++menuType;  boxTypeY = 0; }
		else if (menuType == max) { menuType = min;  boxTypeY = 0; }
	}
	if (TRG(0) & PAD_R1) {
		if (menuType != min) { --menuType;  boxTypeY = 0; }
		else if (menuType == min) { menuType = max;  boxTypeY = 0; }
	}
	menuType = (int)clamp((float)menuType, (float)min, (float)max);
	/*------移動処理(カーソル)------*/
	for (int i = 0; i <= conditional; ++i)
	{
		float left   = pos[i].x;
		float right  = pos[i].x + size[i].x;
		float top    = pos[i].y;
		float bottom = pos[i].y + size[i].y;

		if (left < cursolPos.x && right > cursolPos.x &&
			top < cursolPos.y && bottom > cursolPos.y) {
			if (nowDevice == KEYBOARD && TRG(0) & MOUSE_LEFT_CLICK)  menuType = i;
			if (nowDevice == CONTROLLER && TRG(0) & PAD_TRG1)  menuType = i;
		}
	}
	//-------選択された時の色替えなど-------//
	for (int i = 0; i <= conditional; ++i)
	{
		color[i] = { 0.0f, 0.56f, 0.35f, 1.0f };
	}
	color[menuType] = { 0.01f, 0.8f, 0.52f, 1.0f };
}
// boxTypeYの選択処理
void ScenePause::boxTypeY_selection_process(int min, int max)
{
	/*------移動処理(キーボード)------*/
	if (TRG(0) & PAD_DOWN) ++boxTypeY;
	if (TRG(0) & PAD_UP)   --boxTypeY;
	boxTypeY = (int)clamp((float)boxTypeY, (float)min, (float)max);
	/*------移動処理(カーソル)------*/
	for (int i = 6; i <= 12; ++i)
	{
		float left = pos[i].x;
		float right = pos[i].x + size[i].x;
		float top = pos[i].y;
		float bottom = pos[i].y + size[i].y;

		if ((leftStick != VECTOR2(0.0f, 0.0f) || doUseMause == true) && left < cursolPos.x && right > cursolPos.x &&
			top < cursolPos.y && bottom > cursolPos.y)    boxTypeY = i - 6;
	}
	//-------選択された時の色替えなど-------//
	for (int i = 6; i <= 12; ++i)
	{
		color[i] = { 1.0f, 1.0f, 1.0f, 0.5f };
	}
	color[boxTypeY + 6] = { 1.0f, 1.0f, 1.0f, 0.9f };
}
// boxTypeXの選択処理
void ScenePause::boxTypeX_selection_process(int min, int max)
{
	for (int i = 0; i < 4; ++i) {
		for (int o = 0; o < 4; ++o) {
			old_boxTypeX[i][o] = boxTypeX[i][o];
		}
	}
	/*------移動処理(キーボード)------*/
	if (boxTypeY % 2 == 0) {
		if (TRG(0) & PAD_RIGHT) ++boxTypeX[menuType][boxTypeY - boxTypeY / 2];
		if (TRG(0) & PAD_LEFT)  --boxTypeX[menuType][boxTypeY - boxTypeY / 2];
	}
	for (int i = 0; i < 4; ++i)
	{
		for (int o = 0; o < 4; ++o)
		{
			boxTypeX[i][o] = (int)clamp((float)boxTypeX[i][o], (float)min, (float)max);
		}
	}
	/*------移動処理(カーソル)------*/
	for (int i = 13; i <= 20; ++i)
	{
		static int subCursol = 0;
		if (i == 13) subCursol = 0;
		const int number = i - 13;
		if (number % 2 == 1) ++subCursol;

		float left = pos[i].x;
		float right = pos[i].x + size[i].x;
		float top = pos[i].y;
		float bottom = pos[i].y + size[i].y;

		if (left < cursolPos.x && right > cursolPos.x &&
			top < cursolPos.y && bottom > cursolPos.y) {
			if (nowDevice == KEYBOARD && TRG(0) & MOUSE_LEFT_CLICK) {
				if (number % 2 == 0)   boxTypeX[menuType][number - subCursol] = 1;
				else                   boxTypeX[menuType][number - subCursol] = 2;
			}
			if (nowDevice == CONTROLLER && TRG(0) & PAD_TRG1) {
				if (number % 2 == 0)   boxTypeX[menuType][number - subCursol] = 1;
				else                   boxTypeX[menuType][number - subCursol] = 2;
			}
		}
	}
	// 変更された時だけ AdaptOptions() を呼ぶ
	for (int i = 0; i < 4; ++i) {
		for (int o = 0; o < 4; ++o) {
			if (old_boxTypeX[i][o] != boxTypeX[i][o]) { AdaptOptions(); /* 変更された項目の適応*/ }
		}
	}
	//-------選択された時の色替えなど-------//
	if (menuType == 2 || menuType == 3) {
		for (int i = 13; i <= 20; ++i)
		{
			color[i] = { 1.0f, 1.0f, 1.0f, 0.0f };
		}
	}
	else {
		for (int i = 13; i <= 20; ++i)
		{
			color[i] = { 0.0f, 0.6f, 0.3f, 0.6f };
		}
		for (int i = 0; i <= 3; ++i)
		{
			if (menuType == i)
			{
				for (int o = 0; o < 4; ++o)
				{
					if (boxTypeX[i][o] == 1) color[o * 2 + 13] = { 0.5f, 1.0f, 0.5f, 0.8f };
					if (boxTypeX[i][o] == 2) color[o * 2 + 14] = { 0.5f, 1.0f, 0.5f, 0.8f };
				}
				break;
			}
		}
	}
}
// sliderの選択処理
void ScenePause::slider_process(int min, int max)
{
	if (menuType == 1) {
		static int long_press_timer = 0;
		for (int i = 0; i < SLIDER_NUM; ++i) {
			old_sliderSizeX[i] = sliderSizeX[i];
			/*------移動処理(キーボード)------*/
			if (boxTypeY == 1 + i * 2) {
				if (TRG(0) & PAD_LEFT) { sliderSizeX[i] -= 36.0f; }   // 一番初めは時間差なしで一回減る
				if (TRG(0) & PAD_RIGHT) { sliderSizeX[i] += 36.0f; }  // 一番初めは時間差なしで一回増える
				// long_press_timer の初期化 / 増加条件
				if (!(STATE(0) & (PAD_LEFT | PAD_RIGHT)))   long_press_timer = 0; // どっちも押してないと初期化
				else  ++long_press_timer; // どっちか押していると増加
				if ((STATE(0) & PAD_LEFT) && (STATE(0) & PAD_RIGHT))   long_press_timer = 0; // どっちも押していると初期化
				// 押し続け一定所間経つとスライダーの値が一定間隔で増減していく
				if (long_press_timer > 25 && long_press_timer / 5 % 2 == 1 && STATE(0) & PAD_LEFT) {
					sliderSizeX[i] -= 36.0f; long_press_timer = 40;
				}
				if (long_press_timer > 25 && long_press_timer / 5 % 2 == 1 && STATE(0) & PAD_RIGHT) {
					sliderSizeX[i] += 36.0f; long_press_timer = 40;
				}
			}
			/*------移動処理(カーソル) / スライダー(0% ~ 200% まで調整可)------*/
			{
				// スライダーは当たり判定に遊びを持たせる
				const int index{ 21 + i * 4 };
				float left = pos[index].x - 20.0f;
				float right = pos[index].x + size[index].x + 20.0f;
				float top = pos[index].y - 20.0f;
				float bottom = pos[index].y + size[index].y + 20.0f;
				if (left < cursolPos.x && right > cursolPos.x
					&& top < cursolPos.y && bottom > cursolPos.y) {
					if (nowDevice == KEYBOARD && STATE(0) & MOUSE_LEFT_CLICK) sliderSizeX[i] = cursolPos.x - 360.0f;
					if (nowDevice == CONTROLLER && STATE(0) & PAD_TRG1) sliderSizeX[i] = cursolPos.x - 360.0f;
				}
			}
			sliderSizeX[i] = clamp(sliderSizeX[i], (float)min, (float)max);
			// 変更された時だけ AdaptOptions() を呼ぶ
			if (old_sliderSizeX[i] != sliderSizeX[i]) { AdaptOptions(); /* 変更された項目の適応*/ }
		}
		//-------選択された時の色替えなど-------//
		for (int i = 0; i < SLIDER_NUM; ++i) {
			const int index{ 21 + i * 4 };
			color[index] = { 0.3f, 0.56f, 0.35f, 0.4f };
			size[index + 1] = { sliderSizeX[i], 52.0f };
			color[index + 1] = { 0.3f, 1.0f, 0.7f, 0.8f };
			pos[index + 2] = { size[index + 1].x + 350.0f, pos[index + 1].y - 4.0f };
			color[index + 2] = { 0.3f, 0.7f, 0.8f, 1.0f };
		}
	}
	else
	{
		for (int i = 0; i < SLIDER_NUM; ++i) {
			const int index{ 21 + i * 4 };
			color[index] = { 0.3f, 0.6f, 1.0f, 0.0f };
			color[index + 1] = { 0.2f, 0.3f, 1.0f, 0.0f };
			color[index + 2] = { 1.0f, 0.3f, 0.9f, 0.0f };
		}
	}
}
// 共通の描画
void ScenePause::draw_common()
{
	// 下絵
	{
		VECTOR2 pos{ 0,0 };
		VECTOR2 scale{ 1.0f,1.0f };
		VECTOR2 tp{ 0,0 };
		VECTOR2 ts{ 1450.0f,720.0f };
		VECTOR2 pi{ 0.0f,0.0f };
		VECTOR4 cl{ 0.2f, 0.2f, 0.2f, 0.3f };
		texture::begin(TRANS);
		texture::draw(TRANS, pos, scale, tp, ts, pi, ToRadian(0), cl);
		texture::end(TRANS);
	}

	// メニュー
	for (int i = 0; i < BOX_NUM; ++i)
	{
		primitive::rect(pos[i], size[i], pivot[i], ToRadian(0), color[i]);
	}
}

//***************************
//    コンストラクタ
//***************************
ScenePause::ScenePause()
    : state(0)
	, timer(0)
	, stageBackFlg(false)
	, menuType(0)
	, boxTypeY(0)
	, sub(13)
	, cursolPos()
	, point()
	, pointerPos()
	, oldPointerPos()
	, doUseMause(false)
	, conState()
	, leftStick()
	, rightStick()
	, nowDevice() {}

//***************************
//    デストラクタ
//***************************
ScenePause::~ScenePause() {}

//***************************
//    更新処理(gamescene)
//***************************
void ScenePause::update_game_scene()
{
	switch (state)
	{
	case 0:
		//////// 初期設定 ////////


		state++;
		/*fallthrough*/

	case 1:
		/////// パラメーターの設定 ////////
		parameters_init();

		//-------細かい設定-------//
		// 一番上のメニュー
		for (int i = 0; i <= 3; ++i)
		{
			pos[i]   = { 80.0f + 285.0f * (float)i, 10.0f };
			size[i]  = { 265.0f, 70.0f };
			color[i] = { 0.0f, 0.56f, 0.35f, 1.0f };
		}
		// 下の大きいボックス二つ
		pos[4]   = { 10.0f, 110.0f };
		size[4]  = { 760.0f, 600.0f };
		color[4] = { 0.0f, 0.56f, 0.35f, 1.0f };
		pos[5]   = { 790.0f, 110.0f };
		size[5]  = { 480.0f, 600.0f };
		color[5] = { 0.0f, 0.56f, 0.35f, 1.0f };
		// 下の長細いボックス七つ
		for (int i = 6; i <= 12; ++i)
		{
			pos[i]   = { 30.0f, 130.0f + 82.0f * ((float)i - 6) };
			size[i]  = { 720.0f, 70.0f };
			color[i] = { 1.0f, 1.0f, 1.0f, 0.5f };
		}
		// 下の長細いボックスの中にあるボックス2つ×4つ
		for (int i = 13; i <= 20; ++i)
		{
			const int number = i - 13;

			if (number % 2 == 0)   pos[i].x = { 360.0f };
			else {
				pos[i].x = { 560.0f };
				++sub;
			}

			pos[i].y = { 140.0f + 164.0f * (i - sub) };
			size[i]  = { 170.0f, 50.0f };
			color[i] = { 0.0f, 0.6f, 0.3f, 0.6f };
		}
		// スライダー
		pos[21]   = { 360.0f, 222.0f };
		size[21]  = { 360.0f, 52.0f };
		color[21] = { 0.3f, 0.56f, 0.35f, 0.4f };
		pos[22]   = { 360.0f, 222.0f };
		size[22]  = { sliderSizeX[0], 52.0f };
		color[22] = { 0.3f, 1.0f, 0.7f, 0.8f };
		pos[23]   = { size[22].x + 350.0f, pos[22].y - 4.0f };
		size[23]  = { 10.0f, 60.0f };
		color[23] = { 0.3f, 0.7f, 0.8f, 1.0f };
		// ×書いてある箱
		pos[24]   = { 1289.0f, 15.0f };
		size[24]  = { 60.0f, 60.0f };
		color[24] = { 0.0f, 0.56f, 0.35f, 1.0f };
		// スライダー二つ目
		pos[25] = { 360.0f, 222.0f + 82.0f * 2.0f };
		size[25] = { 360.0f, 52.0f };
		color[25] = { 0.3f, 0.56f, 0.35f, 0.4f };
		pos[26] = { 360.0f, 222.0f + 82.0f * 2.0f };
		size[26] = { sliderSizeX[1], 52.0f };
		color[26] = { 0.3f, 1.0f, 0.7f, 0.8f };
		pos[27] = { size[26].x + 350.0f, pos[26].y - 4.0f };
		size[27] = { 10.0f, 60.0f };
		color[27] = { 0.3f, 0.7f, 0.8f, 1.0f };

		state++;
		/*fallthrough*/

	case 2:
		//////// 通常時 ////////
		device_relation(); // デバイス関連

		//------------------------メニュー------------------------//
		//--< 一番上のメニュー >--//
		menuType_selection_process(0, 3, 3);
		//--< 下の長細いボックス >--//
		boxTypeY_selection_process(0, 6);
		//--< 下の長細いボックスの中にある小さいボックス(2個横に並んでるところ) >--//
		boxTypeX_selection_process(1, 2);
		//--< スライダー(一回押すと増減、そのまま押し続けると一定間隔で増減) >--//
		slider_process(0, 360);
		//--< ×書いてある箱 >--//
		{
			float left = pos[24].x;
			float right = pos[24].x + size[24].x;
			float top = pos[24].y;
			float bottom = pos[24].y + size[24].y;

			if (left < cursolPos.x && right > cursolPos.x &&
				top < cursolPos.y && bottom > cursolPos.y) {
				color[24] = { 0.01f, 0.8f, 0.52f, 1.0f };
				if (nowDevice == KEYBOARD && TRG(0) & MOUSE_LEFT_CLICK) { saveData(); stageBackFlg = true; }
				if (nowDevice == CONTROLLER && TRG(0) & PAD_TRG1) { saveData(); stageBackFlg = true; }
			}
			else {
				color[24] = { 0.0f, 0.56f, 0.35f, 1.0f };
			}
		}

		//-------選択時の処理-------//
		/* OTHER */
		if (menuType == 3) {
			bool terms1 = nowDevice == KEYBOARD && TRG(0) & PAD_TRG1;
			bool term2  = nowDevice == KEYBOARD && TRG(0) & MOUSE_LEFT_CLICK;
			bool term3  = nowDevice == CONTROLLER && TRG(0) & PAD_TRG1;
			bool terms4;
			switch (boxTypeY)
			{
			case 0: // ゲームに戻る
				terms4 = pos[6].x < cursolPos.x&& pos[6].x + size[6].x > cursolPos.x &&
					pos[6].y < cursolPos.y&& pos[6].y + size[6].y > cursolPos.y;
				if (terms1 || (term2 && terms4) || (term3 && terms4)) { saveData(); stageBackFlg = true; }
				break;

			case 1: // タイトルに戻る
				terms4 = pos[7].x < cursolPos.x&& pos[7].x + size[7].x > cursolPos.x &&
					pos[7].y < cursolPos.y&& pos[7].y + size[7].y > cursolPos.y;
				if (terms1 || (term2 && terms4) || (term3 && terms4)) {
					saveData();  nextScene = SCENE_TITLE;  stageBackFlg = true;
				}
				break;

			case 2: // コンテニュー
				terms4 = pos[8].x < cursolPos.x&& pos[8].x + size[8].x > cursolPos.x &&
					pos[8].y < cursolPos.y&& pos[8].y + size[8].y > cursolPos.y;
				if (terms1 || (term2 && terms4) || (term3 && terms4))
				{
					saveData();
					SceneGame::instance()->respawnState = 1;
					SceneGame::instance()->state  = 1;
					SceneGame::instance()->bgManager()->mapStatus = false;
					stageBackFlg = true;
				}
				break;

			case 3: // ゲームをやめる
				terms4 = pos[9].x < cursolPos.x&& pos[9].x + size[9].x > cursolPos.x &&
					pos[9].y < cursolPos.y&& pos[9].y + size[9].y > cursolPos.y;
				if (terms1 || (term2 && terms4) || (term3 && terms4)) { saveData();   window::close(); }
				break;
			}
		}

		// ポーズ中にもう一度ポーズボタン押したらポーズ解除
		if (nowDevice == KEYBOARD && timer >= 30 && TRG(0) & PAD_TRG4) { saveData(); stageBackFlg = true; }
		if (nowDevice == CONTROLLER && timer >= 30 && TRG(0) & PAD_START) { saveData(); stageBackFlg = true; }

		// 一定時間ごとにセーブする(0.5秒ごと)
		if (timer / 30 % 2 == 0) { saveData(); loadData(); }

		// 選択時SE
		if (playSE && TRG(0) & (PAD_UP | PAD_DOWN | PAD_R1 | PAD_R2))
			sound::play(XWB_SOUNDS, XWB_SOUNDS_CHOICE);

		if (playSE && TRG(0) & MOUSE_LEFT_CLICK)
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);

		if (playSE && menuType != 3 && TRG(0) & (PAD_RIGHT | PAD_LEFT))
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);

		if (playSE && menuType == 3 && TRG(0) & PAD_TRG1)
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);

		if (playSE && timer >= 30 && TRG(0) & (PAD_TRG4))
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);



		// debug
		//debug::setString("menuType:%d", menuType);
		//debug::setString("boxTypeY:%d", boxTypeY);
		//for (int i = 0; i < 4; ++i)
		//{
		//	for (int o = 0; o < 7; ++o)
		//	{
		//		debug::setString("boxTypeX[%d][%d]:%d", i, o, boxTypeX[i][o]);
		//	}
		//}
		//debug::setString("stopEffect:%d", playEffect);
		//debug::setString("stopBGM:%d", playBGM);
		//debug::setString("stopSE:%d", playSE);


		break;
	}
	++timer;

	imGui_context();
}

//***************************
//    描画処理(gamescene)
//***************************
void ScenePause::draw_game_scene()
{
	draw_common();

	// K L 書いてある三角
	{
		VECTOR2 a[4]{ {72.0f, 78.0f}, {72.0f, 12.0f}, {0, 42.0f}, {0, 42.0f} };
		primitive::quad(a, { 0.0f, 0.56f, 0.35f, 1.0f });
	}
	{
		VECTOR2 a[4]{ {1208.0f, 78.0f}, {1208.0f, 12.0f}, {1280.0f, 42.0f}, {1280.0f, 42.0f} };
		primitive::quad(a, { 0.0f, 0.56f, 0.35f, 1.0f });
	}

	// 戻るボタン
	texture::begin(BACK_MARK);
	texture::draw(BACK_MARK, pos[24] + VECTOR2(10, 10), { 0.6f, 0.6f }, { 0,0 }, { 60,60 }, { 0,0 }, ToRadian(0), { 1,1,1,1 });
	texture::end(BACK_MARK);

	// ImGui
	for (int i = 0; i < (int)Im_pos.size(); ++i)
	{
		primitive::rect(Im_pos[i], Im_size[i], { 0, 0 }, ToRadian(0), Im_color[i]);
		ostringstream ss;    ss << i + 1;
		font::textOut(5, ss.str(), Im_pos[i] + Im_size[i] / 2,
			{ fabsf(Im_size[i].x * 0.002f), fabsf(Im_size[i].y * 0.002f) }, { 0.0f, 0.0f, 0.0f, Im_color[i].w }, TEXT_ALIGN::MIDDLE);
	}

	//--------文字--------//
	VECTOR4 string_color{ 0.78f ,0.53f ,0.0f, 1.0f };
	// 一番上のメニュー
	font::textOut(5, "GAME",  { 200.0f, 45.0f }, { 0.3f, 0.3f },  string_color, TEXT_ALIGN::MIDDLE);
	font::textOut(5, "SOUND", { 500.0f, 45.0f }, { 0.3f, 0.3f },  string_color, TEXT_ALIGN::MIDDLE);
	font::textOut(5, "OTHER", { 1065.0f, 45.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
	switch (nowDevice)
	{
	case KEYBOARD:
		font::textOut(5, "K", { 50.0f, 42.0f }, { 0.25f, 0.25f },   string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "L", { 1230.0f, 42.0f }, { 0.25f, 0.25f }, string_color, TEXT_ALIGN::MIDDLE);
		break;
	case CONTROLLER:
		font::textOut(5, "RB", { 50.0f, 42.0f }, { 0.25f, 0.25f },   string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "LB", { 1230.0f, 42.0f }, { 0.25f, 0.25f }, string_color, TEXT_ALIGN::MIDDLE);
		break;
	}
	// 長細いボックス
	if (menuType == 0) {
		font::textOut(5, "EFFECT", { 187.0f, 165.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		if (boxTypeY == 0) {
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,0 }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
		}
	}
	if (menuType == 1) {
		font::textOut(5, "BGM", { 187.0f, 165.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "BGM", { 187.0f, 247.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		ostringstream ss_bgm;    ss_bgm << int(sliderSizeX[0] / 180.0f * 100);
		font::textOut(5, ss_bgm.str(), { 355.0f, 247.0f }, { 0.2f, 0.2f }, string_color, TEXT_ALIGN::MIDDLE_RIGHT);

		font::textOut(5, "SE", { 187.0f, 329.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "SE", { 187.0f, 329.0f + 82.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		ostringstream ss_se;    ss_se << int(sliderSizeX[1] / 180.0f * 100);
		font::textOut(5, ss_se.str(), { 355.0f, 329.0f + 82.0f }, { 0.2f, 0.2f }, string_color, TEXT_ALIGN::MIDDLE_RIGHT);

		switch (boxTypeY)
		{
		case 0:
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,530.0f * 1.0f }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
			break;
		case 1:
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,530.0f * 2.0f }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
			break;
		case 2:
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,530.0f * 3.0f }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
			break;
		}
	}
	if (menuType == 3) {
		font::textOut(5, "GO GAME",         { 376.0f, 165.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "BACK TITLE",      { 376.0f, 247.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "BACK CHECKPOINT", { 376.0f, 329.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "QUIT THE GAME",   { 376.0f, 411.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
	}
	// 長細いボックスの中にある二つのボックス(ON OFF)
	{
		static int basis = 0;
		switch (menuType) {
		case 0: basis = 1; break;
		case 1: basis = 2; break;
		case 2: basis = 0; break;
		case 3: basis = 0; break;
		}
		for (int menuTy = 0; menuTy <= 3; ++menuTy) {
			if (menuType == menuTy) {
				for (int i = 0; i < basis; ++i) {
					font::textOut(5, "ON", { 440.0f, 164.0f + 164.0f * (float)i }, { 0.25f, 0.25f },  string_color, TEXT_ALIGN::MIDDLE);
					font::textOut(5, "OFF", { 645.0f, 164.0f + 164.0f * (float)i }, { 0.25f, 0.25f }, string_color, TEXT_ALIGN::MIDDLE);
				}
				break;
			}
		}
	}

}

//***************************
//    更新処理(titlescene)
//***************************
void ScenePause::update_title_scene()
{
	switch (state)
	{
	case 0:
		//////// 初期設定 ////////

		state++;
		/*fallthrough*/

	case 1:
		/////// パラメーターの設定 ////////
		parameters_init();

		//-------細かい設定-------//
		// 一番上のメニュー
		for (int i = 0; i <= 2; ++i)
		{
			pos[i]   = { 80.0f + 285.0f * (float)i, 10.0f };
			size[i]  = { 265.0f, 70.0f };
			color[i] = { 0.0f, 0.56f, 0.35f, 1.0f };
		}
		// backspace 書いてる箱
		pos[3] = {1023.0f, 18.0f};
		size[3] = {325.0f, 86.0f};
		color[3] = { 0.0f, 0.56f, 0.35f, 1.0f };

		// 下の大きいボックス二つ
		pos[4]   = { 10.0f, 110.0f };
		size[4]  = { 760.0f, 600.0f };
		color[4] = { 0.0f, 0.56f, 0.35f, 1.0f };
		pos[5]   = { 790.0f, 110.0f };
		size[5]  = { 480.0f, 600.0f };
		color[5] = { 0.0f, 0.56f, 0.35f, 1.0f };
		// 下の長細いボックス七つ
		for (int i = 6; i <= 12; ++i)
		{
			pos[i]   = { 30.0f, 130.0f + 82.0f * ((float)i - 6) };
			size[i]  = { 720.0f, 70.0f };
			color[i] = { 1.0f, 1.0f, 1.0f, 0.5f };
		}
		// 下の長細いボックスの中にあるボックス2つ×4つ
		for (int i = 13; i <= 20; ++i)
		{
			const int number = i - 13;

			if (number % 2 == 0)   pos[i].x = { 360.0f };
			else {
				pos[i].x = { 560.0f };
				++sub;
			}

			pos[i].y = { 140.0f + 164.0f * (i - sub) };
			size[i]  = { 170.0f, 50.0f };
			color[i] = { 0.0f, 0.6f, 0.3f, 0.6f };
		}
		// スライダー
		pos[21]   = { 360.0f, 222.0f };
		size[21]  = { 360.0f, 52.0f };
		color[21] = { 0.3f, 0.56f, 0.35f, 0.4f };
		pos[22]   = { 360.0f, 222.0f };
		size[22]  = { sliderSizeX[0], 52.0f };
		color[22] = { 0.3f, 1.0f, 0.7f, 0.8f };
		pos[23]   = { size[22].x + 350.0f, pos[22].y - 4.0f };
		size[23]  = { 10.0f, 60.0f };
		color[23] = { 0.3f, 0.7f, 0.8f, 1.0f };
		// ×書いてある箱
		pos[24] = { 1365.0f, 32.0f };
		size[24] = { 60.0f, 60.0f };
		color[24] = { 0.0f, 0.56f, 0.35f, 1.0f };
		// スライダー二つ目
		pos[25]   = { 360.0f, 222.0f + 82.0f * 2.0f };
		size[25]  = { 360.0f, 52.0f };
		color[25] = { 0.3f, 0.56f, 0.35f, 0.4f };
		pos[26]   = { 360.0f, 222.0f + 82.0f * 2.0f };
		size[26]  = { sliderSizeX[1], 52.0f };
		color[26] = { 0.3f, 1.0f, 0.7f, 0.8f };
		pos[27]   = { size[26].x + 350.0f, pos[26].y - 4.0f };
		size[27]  = { 10.0f, 60.0f };
		color[27] = { 0.3f, 0.7f, 0.8f, 1.0f };

		state++;
		/*fallthrough*/

	case 2:
		//////// 通常時 ////////
		device_relation(); // デバイス関連

	    //------------------------メニュー------------------------//
		//--< 一番上のメニュー >--//
		menuType_selection_process(0, 2, 2);
		//--< 下の長細いボックス >--//
		boxTypeY_selection_process(0, 6);
		//--< 下の長細いボックスの中にある小さいボックス(2個横に並んでるところ) >--//
		boxTypeX_selection_process(1, 2);
		//--< スライダー(一回押すと増減、そのまま押し続けると一定間隔で増減) >--//
		slider_process(0, 360);
		//--< ×書いてある箱 >--//
		{
			float left   = pos[24].x;
			float right  = pos[24].x + size[24].x;
			float top    = pos[24].y;
			float bottom = pos[24].y + size[24].y;

			if (left < cursolPos.x && right > cursolPos.x &&
				top < cursolPos.y && bottom > cursolPos.y) {
				color[24] = { 0.01f, 0.8f, 0.52f, 1.0f };
				if (nowDevice == KEYBOARD && TRG(0) & MOUSE_LEFT_CLICK) { saveData(); stageBackFlg = true; }
				if (nowDevice == CONTROLLER && TRG(0) & PAD_TRG1) { saveData(); stageBackFlg = true; }
			}
			else {
				color[24] = { 0.0f, 0.56f, 0.35f, 1.0f };
			}
		}

		//-------選択時の処理-------//
		/* OTHER */
		if (menuType == 2) {
			bool terms1 = nowDevice == KEYBOARD && TRG(0) & PAD_TRG1;
			bool term2  = nowDevice == KEYBOARD && TRG(0) & MOUSE_LEFT_CLICK;
			bool term3  = nowDevice == CONTROLLER && TRG(0) & PAD_TRG1;
			bool terms4;
			switch (boxTypeY)
			{
			case 0: // ゲームをやめる
				terms4 = pos[6].x < cursolPos.x&& pos[6].x + size[6].x > cursolPos.x &&
					pos[6].y < cursolPos.y&& pos[6].y + size[6].y > cursolPos.y;
				if (terms1 || (term2 && terms4) || (term3 && terms4)) { saveData();   window::close(); }
				break;
			}
		}

		// backspaceでポーズ解除
		if (timer >= 30 && TRG(0) & PAD_SELECT) { saveData(); stageBackFlg = true; }

		// 一定時間ごとにセーブする(0.5秒ごと)
		if (timer / 30 % 2 == 0) { saveData(); loadData(); }

		// 選択時SE
		if (playSE && TRG(0) & (PAD_UP | PAD_DOWN | PAD_R1 | PAD_R2))
			sound::play(XWB_SOUNDS, XWB_SOUNDS_CHOICE);

		if (playSE && TRG(0) & MOUSE_LEFT_CLICK)
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);

		if (playSE && menuType != 2 && TRG(0) & (PAD_RIGHT | PAD_LEFT))
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);

		if (playSE && menuType == 2 && TRG(0) & PAD_TRG1)
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);

		if (playSE && timer >= 30 && TRG(0) & PAD_SELECT)
			sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);


		// debug
		//debug::setString("menuType:%d", menuType);
		//debug::setString("boxTypeY:%d", boxTypeY);
		//for (int i = 0; i < 4; ++i)
		//{
		//	for (int o = 0; o < 7; ++o)
		//	{
		//		debug::setString("boxTypeX[%d][%d]:%d", i, o, boxTypeX[i][o]);
		//	}
		//}
		//debug::setString("stopEffect:%d", playEffect);
		//debug::setString("stopBGM:%d", playBGM);
		//debug::setString("stopSE:%d", playSE);


		break;
	}
	++timer;


	imGui_context();
}

//***************************
//    描画処理(titlescene)
//***************************
void ScenePause::draw_title_scene()
{
	draw_common();

	// K L 書いてある三角
	{
		VECTOR2 a[4]{ {72.0f, 78.0f}, {72.0f, 12.0f}, {0, 42.0f}, {0, 42.0f} };
		primitive::quad(a, { 0.0f, 0.56f, 0.35f, 1.0f });
	}
	{
		VECTOR2 a[4]{ {943.0f, 78.0f}, {943.0f, 12.0f}, {1015.0f, 42.0f}, {1015.0f, 42.0f} };
		primitive::quad(a, { 0.0f, 0.56f, 0.35f, 1.0f });
	}

	// 戻るボタン
	texture::begin(BACK_MARK);
	texture::draw(BACK_MARK, pos[24] + VECTOR2(10, 10), { 0.6f, 0.6f }, { 0,0 }, { 60,60 }, { 0,0 }, ToRadian(0), { 1,1,1,1 });
	texture::end(BACK_MARK);

	// ImGui
	for (int i = 0; i < (int)Im_pos.size(); ++i)
	{
		primitive::rect(Im_pos[i], Im_size[i], { 0, 0 }, ToRadian(0), Im_color[i]);
		ostringstream ss;    ss << i + 1;
		font::textOut(5, ss.str(), Im_pos[i] + Im_size[i] / 2,
			{ fabsf(Im_size[i].x * 0.002f), fabsf(Im_size[i].y * 0.002f) }, { 0.0f, 0.0f, 0.0f, Im_color[i].w }, TEXT_ALIGN::MIDDLE);
	}

	//--------文字--------//
	VECTOR4 string_color{ 0.78f ,0.53f ,0.0f, 1.0f };
	font::textOut(5, "BACK SPACE", { 1180.0f, 45.0f }, { 0.2f, 0.2f }, string_color, TEXT_ALIGN::MIDDLE);
	font::textOut(5, "TITLE",      { 1180.0f, 80.0f }, { 0.2f, 0.2f }, string_color, TEXT_ALIGN::MIDDLE);

	// 一番上のメニュー
	font::textOut(5, "GAME",  { 200.0f, 45.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
	font::textOut(5, "SOUND", { 500.0f, 45.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
	font::textOut(5, "OTHER", { 780.0f, 45.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
	switch (nowDevice)
	{
	case KEYBOARD:
		font::textOut(5, "K", { 50.0f, 42.0f }, { 0.25f, 0.25f },   string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "L", { 965.0f, 42.0f }, { 0.25f, 0.25f }, string_color, TEXT_ALIGN::MIDDLE);
		break;
	case CONTROLLER:
		font::textOut(5, "RB", { 50.0f, 42.0f }, { 0.25f, 0.25f },   string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "LB", { 965.0f, 42.0f }, { 0.25f, 0.25f }, string_color, TEXT_ALIGN::MIDDLE);
		break;
	}
	// 長細いボックス
	if (menuType == 0) {
		font::textOut(5, "EFFECT", { 187.0f, 165.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		if (boxTypeY == 0) {
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,0 }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
		}
	}
	if (menuType == 1) {
		font::textOut(5, "BGM", { 187.0f, 165.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "BGM", { 187.0f, 247.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		ostringstream ss_bgm;    ss_bgm << int(sliderSizeX[0] / 180.0f * 100);
		font::textOut(5, ss_bgm.str(), { 355.0f, 247.0f }, { 0.2f, 0.2f }, string_color, TEXT_ALIGN::MIDDLE_RIGHT);

		font::textOut(5, "SE", { 187.0f, 329.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		font::textOut(5, "SE", { 187.0f, 329.0f + 82.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
		ostringstream ss_se;    ss_se << int(sliderSizeX[1] / 180.0f * 100);
		font::textOut(5, ss_se.str(), { 355.0f, 329.0f + 82.0f }, { 0.2f, 0.2f }, string_color, TEXT_ALIGN::MIDDLE_RIGHT);

		switch (boxTypeY)
		{
		case 0:
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,530.0f * 1.0f }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
			break;
		case 1:
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,530.0f * 2.0f }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
			break;
		case 2:
			texture::begin(PORSE_BOAD);
			texture::draw(PORSE_BOAD, { 827.0f, 146.0f }, { 1, 1 }, { 0,530.0f * 3.0f }, { 400.0f, 530.0f }, { 0, 0 }, 0, { 1, 1, 1, 1 });
			texture::end(PORSE_BOAD);
			break;
		}
	}
	if (menuType == 2) {
		font::textOut(5, "QUIT THE GAME", { 376.0f, 165.0f }, { 0.3f, 0.3f }, string_color, TEXT_ALIGN::MIDDLE);
	}
	// 長細いボックスの中にある二つのボックス(ON OFF)
	{
		static int basis = 0;
		switch (menuType) {
		case 0: basis = 1; break;
		case 1: basis = 2; break;
		case 2: basis = 0; break;
		case 3: basis = 0; break;
		}
		for (int menuTy = 0; menuTy <= 3; ++menuTy) {
			if (menuType == menuTy) {
				for (int i = 0; i < basis; ++i) {
					font::textOut(5, "ON", { 440.0f, 164.0f + 164.0f * (float)i }, { 0.25f, 0.25f },  string_color, TEXT_ALIGN::MIDDLE);
					font::textOut(5, "OFF", { 645.0f, 164.0f + 164.0f * (float)i }, { 0.25f, 0.25f }, string_color, TEXT_ALIGN::MIDDLE);
				}
				break;
			}
		}
	}
}