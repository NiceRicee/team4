//�@必要なファイルをインクルード
#include <thread>
#include "all.h"
#include "multi_thread.h"
#include "audio.h"
#include "scene_pause.h"

// 変数
int curScene            = SCENE_NONE;
int nextScene           = SCENE_TITLE;

bool Scene::continueFlg = false;

//実態宣言
SceneTitle SceneTitle::instance_;
SceneGame SceneGame::instance_;
SceneClear SceneClear::instance_;

// 軽減処理用
bool is_load_title  = false;
bool is_load_stage1 = false;
bool is_load_clear  = false;

// マルチスレッド用実体
MultiThread mlt_th;

//******************************************************************************
//
//
//		WinMain関数（エントリポイント）
//
//
//******************************************************************************

//�AWinMain関数を記述する
int APIENTRY wWinMain(HINSTANCE, HINSTANCE, LPWSTR, int)
{
	// 乱数のリセット
    // srand((unsigned int)time(NULL));
	//�Cゲームライブラリの初期設定
	GameLib::init(L"HOLLOWgotSUN", SCREEN_W, SCREEN_H);

	// オーディオの初期設定
	audio_init();

	// オプションのデータのロード
	ScenePause::getInstance()->loadData();

	//�Eゲームループ
	while (GameLib::gameLoop(true))
	{

		///シーン切り替え処理
		if (curScene != nextScene)
		{
			//　現在のシーンに応じた終了処理
			switch (curScene)
			{

			case SCENE_TITLE:
				SceneTitle::instance()->deinit();
				break;

			case SCENE_STAGE1:
				SceneGame::instance()->stage1_deinit();
				break;

			case SCENE_CLEAR:
				SceneClear::instance()->deinit();

				break;
			}

			//次のシーンに応じた初期設定処理
			switch (nextScene)
			{
			case SCENE_TITLE:
				if (!is_load_title) {
					// マルチスレッド
					texture::load(TEXNO::THREAD_LOADING, L"./Data/Maps/rogporse.png", 1U);
					mlt_th.clear();
					std::thread th_load([] { mlt_th.thread_texture_load_title(is_load_title); });
					std::thread th_draw([] { mlt_th.thread_update_now_loading(is_load_title, { 0.2f, 0.2f, 0.2f }); });

					th_load.join();
					th_draw.join();
				}
				SceneTitle::instance()->init();
				break;

			case SCENE_STAGE1:
				if (!is_load_stage1) {
					// マルチスレッド
					mlt_th.clear();
					std::thread th_load([] { mlt_th.thread_texture_load_game(is_load_stage1); });
					std::thread th_draw([] { mlt_th.thread_update_now_loading(is_load_stage1, { 0.0f, 0.0f, 0.0f }); });

					th_load.join();
					th_draw.join();
				}
				SceneGame::instance()->stage1_init();
				break;

			case SCENE_CLEAR:
				if (!is_load_clear) {
					texture::load(loadTexture);
					//texture::load(loadTexture_fade);

					is_load_clear = true;
				}
				SceneClear::instance()->init();
				break;

			}

			//nextSceneが curSceneになる
			curScene = nextScene;

			// オプションの設定
			ScenePause::getInstance()->AdaptOptions();
		}

		//入力を更新する
		input::update();
		// 音楽の更新処理
		music::update();

#ifdef USE_IMGUI
		ImGui_ImplDX11_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();
#endif

		// 更新処理
		switch (curScene)
		{
		case SCENE_TITLE:
			SceneTitle::instance()->update();
			SceneTitle::instance()->draw();
			break;

		case SCENE_STAGE1:
			SceneGame::instance()->stage1_update();
			SceneGame::instance()->stage1_draw();
			break;

		case SCENE_CLEAR:
			SceneClear::instance()->update();
			SceneClear::instance()->draw();
			break;

		}

		//デバッグ用文字列の表示
		debug::display(1, 0, 0, 1, 1);

#ifdef USE_IMGUI
		ImGui::Render();
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
#endif



		//�F画面を描画する
		GameLib::present(1, 0);
	}

	switch (curScene)
	{
	case SCENE_TITLE:
		SceneTitle::instance()->deinit();
		break;

	case SCENE_STAGE1:

		SceneGame::instance()->stage1_deinit();
		break;

	case SCENE_CLEAR:
		SceneClear::instance()->deinit();

		break;
	}

	// テクスチャの開放
	texture::releaseAll();

	// オーディオの終了処理
	audio_deinit();
    //�Dゲームライブラリの終了処理
	GameLib::uninit();

	//�B戻り値は0でよい
	return 0;

}

