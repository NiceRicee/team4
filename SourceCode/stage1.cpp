#include"all.h"
#include "user.h"
#include<sstream>
#include<sstream>
#include<cmath>
#include "audio.h"
#include "scene_pause.h"
#include "anime_object_behavior.h"
#include"StageData.h"


//----------------------------------------------------------------------
//  BgSt1クラス
//----------------------------------------------------------------------
void MapSt1::init()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	// BG用データのクリア
	clear();

	// マップデータのロード
	switch (areaNum)
	{
	case 0:
		if (!loadMapData(st1_1area, terrain))
		{
			assert(!"1_1データのロードに失敗");
		}
		break;
	case  1:
		if (!loadMapData(st1_2area, terrain))
		{
			assert(!"1_2データのロードに失敗");
		}
		break;

	case  2:
		if (!loadMapData(st1_3area, terrain))
		{
			assert(!"1_3データのロードに失敗");
		}
		break;

	case  3:
		if (!loadMapData(st1_4area, terrain))
		{
			assert(!"1_4データのロードに失敗");
		}
		break;

	case  4:
		if (!loadMapData(st2_1area, terrain))
		{
			assert(!"2_1データのロードに失敗");
		}
		break;

	case  5:
		if (!loadMapData(st2_2area, terrain))
		{
			assert(!"2_2データのロードに失敗");
		}
		break;

	case  6:
		if (!loadMapData(st2_3area, terrain))
		{
			assert(!"2_3データのロードに失敗");
		}
		break;

	case  7:
		if (!loadMapData(st2_4area, terrain))
		{
			assert(!"2_4データのロードに失敗");
		}
		break;

	case 8:
		if (!loadMapData(st3_1area, terrain))
		{
			assert(!"3_1データのロードに失敗");
		}
		break;

	case 9:
		if (!loadMapData(st3_2area, terrain))
		{
			assert(!"3_2データのロードに失敗");
		}
		break;

	case 10:
		if (!loadMapData(st3_3area, terrain))
		{
			assert(!"3_3データのロードに失敗");
		}
		break;

	case 11:
		if (!loadMapData(st3_4area, terrain))
		{
			assert(!"3_4データのロードに失敗");
		}
		break;

	case 12:
		if (!loadMapData(st4_1area, terrain))
		{
			assert(!"4_1データのロードに失敗");
		}
		break;

	case 13:
		if (!loadMapData(st4_2area, terrain))
		{
			assert(!"4_2データのロードに失敗");
		}
		break;

	case 14:
		if (!loadMapData(st4_3area, terrain))
		{
			assert(!"4_3データのロードに失敗");
		}
		break;

	case 15:
		if (!loadMapData(st4_4area, terrain))
		{
			assert(!"4_4データのロードに失敗");
		}
		break;

	case 16:
		if (!loadMapData(st5_1area, terrain))
		{
			assert(!"5_1データのロードに失敗");
		}
		break;

	case 17:
		if (!loadMapData(st5_2area, terrain))
		{
			assert(!"5_2データのロードに失敗");
		}
		break;

	case 18:
		if (!loadMapData(st5_3area, terrain))
		{
			assert(!"5_3データのロードに失敗");
		}
		break;

	case 19:
		if (!loadMapData(st5_4area, terrain))
		{
			assert(!"5_4データのロードに失敗");
		}
		break;

	case 20:
		if (!loadMapData(st6_1area, terrain))
		{
			assert(!"6_1データのロードに失敗");
		}
		break;

	case 21:
		if (!loadMapData(st6_2area, terrain))
		{
			assert(!"6_2データのロードに失敗");
		}
		break;

	case 22:
		if (!loadMapData(st6_3area, terrain))
		{
			assert(!"6_3データのロードに失敗");
		}
		break;

	case 23:
		if (!loadMapData(st6_4area, terrain))
		{
			assert(!"6_4データのロードに失敗");
		}
		break;

	}

}
//----------------------------------------------------------------------
//  背景描画
//----------------------------------------------------------------------
void MapSt1::drawTerrain()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	if (MapEditor::instance().mapEditorFlg == false)
	{
		//マップチップの画像切り替え
		{
			if (areaNum < 4) texNo = TEXNO::MAPCHIP1;

			else if (MoreAndLess(areaNum, 4, 7))texNo = TEXNO::MAPCHIP2;
			else if (MoreAndLess(areaNum, 8, 23))texNo = TEXNO::MAPCHIP1;


		}
	}

	//マップエディタ更新
	MapEditor::instance().update(terrain);

	//反転
	if (hantenFlg)
	{
		hanten(terrain);
		hantenFlg = false;
		mapStatus =! mapStatus;
	}

	 draw(texNo, terrain);

	if (MapEditor::instance().mapEditorFlg)
	{
		const int DIV_X = 1280 / CHIP_SIZE; // division x
		const int DIV_Y = SCREEN_H / CHIP_SIZE; // division y
		//const int REM_X = 1280 % CHIP_SIZE; // remainder x
		//const int REM_Y = SCREEN_H % CHIP_SIZE; // remainder y
		for (int y = 0; y < BG::LOOP_Y; y++)
		{
			for (int x = 0; x < BG::LOOP_X; x++)
			{
				//if (DIV_X + x >= CHIP_NUM_X || DIV_Y + y >= CHIP_NUM_Y)
				//	continue; // 添え字の範囲チェック

				//VECTOR2 pos = { static_cast<float>(x * CHIP_SIZE - REM_X), static_cast<float>(y * CHIP_SIZE - REM_Y) };
				VECTOR2 pos = { static_cast<float>(x * CHIP_SIZE), static_cast<float>(y * CHIP_SIZE) };
				VECTOR2 texSize = { static_cast<float>(CHIP_SIZE), static_cast<float>(CHIP_SIZE) };
				VECTOR4 color = { 1, 1, 1, 1 };
				primitive::line(pos, pos + VECTOR2(texSize.x, 0), color);
				primitive::line(pos + VECTOR2(texSize.x, 0), pos + VECTOR2(texSize.x, texSize.y), color);
				primitive::line(pos + VECTOR2(texSize.x, texSize.y), pos + VECTOR2(0, texSize.y), color);
				primitive::line(pos + VECTOR2(0, texSize.y), pos, color);


			}
		}
	}
}
//----------------------------------------------------------------------
//  データ取得
//----------------------------------------------------------------------
BG::TR_ATTR MapSt1::getTerrainAttr(const VECTOR2& pos)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);


	//getDataでインデックスを取得
	int chip = getData(terrain, pos);
	if (chip < 0) return TR_NONE;
	assert(chip < CHIP_NUM_PER_LINE* CHIP_LINE_NUM);
	return terrainAttrSt1[chip];
}


void SceneGame::stage1_init()
{
	state = 0;
	timer = 0;
	respawnState = 0;
	kakouScale = 1;

	do_start_blackout = false;
	trans_color_w = 0.0f;

	do_start_fall_blackout = false;
	trans_fall_color_w = 0.0f;
	fall_blackout_timer = 0;

	bgManager_ = new MapSt1();
	playerManager_ = new Player();
	UIManager_ = new UI();
}

void SceneGame::stage1_deinit()
{
	music::stop();
	safe_delete(bgManager_);
	safe_delete(playerManager_);
	safe_delete(UIManager_);
}

void SceneGame::bgm_play(VECTOR4_INT area_num, int bgm_index)
{
	if (bgManager()->areaNum == area_num.x || bgManager()->areaNum == area_num.y ||
		bgManager()->areaNum == area_num.z || bgManager()->areaNum == area_num.w) {
		if (music::isInUse(bgm_index) && music::getState(bgm_index) == DirectX::SoundState::STOPPED) {
			music::stop(); // 前のステージのBGMを止める
			music::play(bgm_index, true);
		}
	}
}

void SceneGame::stage1_update()
{
	if (MapEditor::instance().mapEditorFlg == false)
	{
		// ポーズ画面
		if (isPause) {
			ScenePause::getInstance()->update_game_scene();
			// メニューを閉じた時の1フレーム処理
			if (ScenePause::getInstance()->getStageBackFlg()) {
				isPause = false;
			}
			return;  // ポーズ中は操作を受け付けない
		}

		if (TRG(0) & PAD_TRG4) {
			isPause = true;
			ScenePause::getInstance()->resetState();
			if (playSE) sound::play(XWB_SOUNDS, XWB_SOUNDS_DESION);
		}

		switch (state)
		{
		case 0:
			//////// 初期設定 ////////
			if (!continueFlg) respawnState = 0;
			else respawnState = 1;


			state++;
			/*fallthrough*/

		case 1:
			// メニューデータの読み込み
			ScenePause::getInstance()->loadData();

			// 透過処理
			GameLib::setBlendMode(Blender::BS_ALPHA);

			/////// パラメーターの設定 ////////
			// リスポーン
			respawn();

			//--------< BGM >--------//
			/*stage1*/
			bgm_play(VECTOR4_INT{ 0,1,2,3 },     BGM_STAGE1);
			/*stage2*/
			bgm_play(VECTOR4_INT{ 4,5,6,7 },     BGM_STAGE2);
			/*stage3*/
			bgm_play(VECTOR4_INT{ 8,9,10,11 },   BGM_STAGE3);
			/*stage4*/
			bgm_play(VECTOR4_INT{ 12,13,14,15 }, BGM_STAGE4);
			/*stage5*/
			bgm_play(VECTOR4_INT{ 16,17,18,19 }, BGM_STAGE5);
			/*stage6*/
			bgm_play(VECTOR4_INT{ 20,21,22,23 }, BGM_STAGE6);

			// 変更された項目の適応(BGMの設定があるのでこの位置)
			ScenePause::getInstance()->AdaptOptions();

			//マップ初期化：データ読み込み
			bgManager()->init();
			if (back_stage_hanten)
			{
				bgManager()->hanten(bgManager()->terrain);
				bgManager()->mapStatus = true;

				back_stage_hanten = false;
			}
			// 焚火の生成
			if (bgManager()->areaNum == 0  || bgManager()->areaNum == 3  ||
				bgManager()->areaNum == 7  || bgManager()->areaNum == 10 ||
				bgManager()->areaNum == 12 || bgManager()->areaNum == 16 ||
				bgManager()->areaNum == 18 || bgManager()->areaNum == 20)
			{
				bonfire_manager.reset(new Bonfire(bonfire_pos[bgManager()->areaNum]));
			}
			else {
				bonfire_manager.reset(nullptr);
			}
			// 看板
			if (bgManager()->areaNum == 0) {
				signBorad_manager.reset(new SignBoard({}));
			}
			else {
				signBorad_manager.reset(nullptr);
			}
			pos = { 0, 0 };

			playerManager()->init();
			UIManager()->init();


			state++;
			/*fallthrough*/

		case 2:
			//////// 通常時 ////////
			// 前半の動き
			FadeRectangle3::getInstance()->firstHalfMove(SCENE_NONE);
			// 前シーン移行
			if (!do_start_blackout && TRG(0) & PAD_SELECT && FadeRectangle3::getInstance()->fadeFlg == false) {
				FadeRectangle3::getInstance()->fadeFlg = true; FadeRectangle3::getInstance()->fadeState = 4;
			}
			FadeRectangle3::getInstance()->secondHalfMove(SCENE_TITLE);

			//プレイヤーの落下死
			dying_in_fall();


			if (!do_start_fall_blackout) playerManager()->update();
			if (!do_start_blackout && !do_start_fall_blackout) bgManager()->update();
			UIManager()->update();
			moveArea();
			debug::setString("areanum %d", bgManager()->areaNum);

			//ゲームクリア
			game_clear();

			// 焚火の動き
			if (bonfire_manager) {
				bonfire_manager->anime_movement();
				// 焚火との当たり判定
				if (hitCheckCircle(playerManager_->position - VECTOR2(0, 32), 30.0f, bonfire_manager->anime_pos[0], 30.0f)) {
					//debug::setString("touch!!!");
					if (operateFile.respawn_case <= bgManager()->areaNum) // 最後に触れたのが3つめだとしたら2つ目に触れても上書きされない
					{
						operateFile.respawn_case = bgManager()->areaNum;
						operateFile.save_data();
					}
				}
				// 触ったことのあるチェックポイントを点火アニメーションにする
				for (int i = 0; i < operateFile.respawn_case + 1; ++i) {
					bonfire_manager->is_ignition_bonfire[i] = true;
				}
				debug::setString("operateFile.respawn_case:%d", operateFile.respawn_case);

			}
			if (signBorad_manager != nullptr) {
				signBorad_manager->anime_movement();
			}


			break;
		}


		timer++;
	}
#ifdef USE_IMGUI
	static bool showMenubar;
	ImGui::Begin(u8"メニューバー");
	ImGui::Checkbox(u8"メニューバー", &showMenubar);
	ImGui::End();




	//メニューバー
	if (showMenubar)
	{
		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::BeginMenu("Window"))
			{
				ImGui::MenuItem("SceneGameWindow", NULL, &ShowSceneGame);
				ImGui::MenuItem("MapEditorWindow", NULL, &MapEditor::instance().showMapEditWindow);
				ImGui::MenuItem("PlayerWindow", NULL, &playerManager()->showPlayerWindow);
				ImGui::MenuItem("UIWindow", NULL, &UIManager()->showUIWindow);
				ImGui::EndMenu();
			}
			ImGui::EndMainMenuBar();
		}
	}
	// ImGui::ShowDemoWindow();
	if (ShowSceneGame)
	{
		ImGui::Begin("scene_Game");

		if (ImGui::Button("go to clear scene")) {
			nextScene = SCENE_CLEAR;
		}

		ImGui::End();

		// bonfire_pos
		if(bonfire_manager)
		{
			ImGui::Begin("bonfire_pos");
			ImGui::SliderFloat2("bonfire_manager->anime_pos[0]", (float*)&bonfire_manager->anime_pos[0], 0.0f, 1280.0f);

			ImGui::BeginChild(ImGui::GetID((void*)0), ImVec2(300, 120), ImGuiWindowFlags_NoTitleBar);
			for (int i = 0; i < 8; ++i) {
				std::stringstream ss;  ss << "bonfire_pos" << i;
				char char_array[256];  ss.get(char_array, 256);
				ImGui::SliderFloat2(char_array, (float*)&bonfire_pos[i], 0.0f, 1280.0f);
			}
			ImGui::EndChild();
			ImGui::End();
		}
		// signBorad_manager
		if (signBorad_manager)
		{
			ImGui::Begin("signBorad_pos");
			ImGui::SliderFloat2("signBorad_manager->anime_pos[0]", (float*)&signBorad_manager->anime_pos[0], 0.0f, 1280.0f);
			ImGui::SliderFloat2("signBorad_manager->anime_pos[1]", (float*)&signBorad_manager->anime_pos[1], 0.0f, 1280.0f);

			ImGui::End();
		}
	}
#endif
}

// 落下死処理
void SceneGame::dying_in_fall()
{
	//if ((playerManager()->fallSpeed >= 18.0f && playerManager()->onGround))
	if (playerManager()->deathFlg)
	{
		if (playSE) sound::play(XWB_SOUNDS, XWB_SOUNDS_LANDING);

		playerManager()->fallSpeed = 0;
		do_start_fall_blackout = true;
		playerManager()->deathFlg = false;
	}
	if (do_start_fall_blackout) {
		++fall_blackout_timer;
		switch (fall_blackout_timer / 30 % 2)
		{
		case 0:
			trans_fall_color_w += 0.04f;
			if (trans_fall_color_w > 1.0f) {
				trans_fall_color_w = 1.0f;
				// リスポーン
				state = 1;
				respawnState = 1;
				bgManager()->mapStatus = false;
				playerManager()->speed = {};
			}
			break;
		case 1:
			trans_fall_color_w -= 0.04f;  if (trans_fall_color_w < 0.0f)  trans_fall_color_w = 0.0f;
			break;
		}
		if (fall_blackout_timer > 60) do_start_fall_blackout = false;
	}
	else { // 初期化
		fall_blackout_timer = 0;
		trans_fall_color_w = 0.0f;
	}

	//debug::setString(" ");
	//debug::setString("fall_flg %d", do_start_fall_blackout);
	//debug::setString("fall_blackout_timer %d", fall_blackout_timer);
	//debug::setString("trans_fall_color_w %f", trans_fall_color_w);
}

void SceneGame::game_clear()
{
	const float POS_Y_INIT = 635.0f;
	static VECTOR2 goal_pos{ 637.0f, POS_Y_INIT };
	if (!bgManager()->mapStatus) {/* 反転前*/ goal_pos.y = POS_Y_INIT; }
	else { /* 反転後*/ goal_pos.y = abs(720.0f - POS_Y_INIT); }
	// 落下死したときはクリアできない
	if (do_start_fall_blackout) {
		do_start_blackout = false;
	}
	if (!do_start_blackout) trans_color_w += 0.0f;

	if (bgManager()->areaNum == AREANUMMAX - 1 && playerManager()->onGround &&
		hitCheckCircle(playerManager_->position - VECTOR2(0, 32), 30.0f, goal_pos, 55.0f)) {
		do_start_blackout = true;
	}
	if (do_start_blackout) {
		trans_color_w += 0.01f;
		if (trans_color_w >= 1.0f) { trans_color_w = 1.0f;  nextScene = SCENE_CLEAR; }
	}
}

void SceneGame::stage1_draw()
{
	GameLib::clear(0.2f, 0.2f, 0.2f);

	if (bgManager()->mapStatus)
	{
		kakouScale = -1;
	}
	else
	{
		kakouScale = 1;
	}

	float texY = bgManager()->getScrollPos().y;
	if (texY > 2160)
	{
		texY = 2160;
	}
	stage1_draw_back();

	if (signBorad_manager != nullptr) {
		signBorad_manager->anime_draw();
	}

	stage1_draw_kako_down();

	bgManager()->drawTerrain();



	if (bonfire_manager) bonfire_manager->anime_draw();

	playerManager()->render();

	stage1_draw_kako_up();
	if (signBorad_manager != nullptr) {
		for (int i = 0; i < (int)signBorad_manager->anime_pos.size(); ++i) {
			if (signBorad_manager->is_display[i]) {
				texture::begin(TUTORIAL);
				texture::draw(TUTORIAL, { 95,305 }, { 1,1 },
					{ 0, (float)i * 128 }, { 516,128 }, { 0,0 }, ToRadian(0), { 1,1,1,1 });
				texture::end(TUTORIAL);
			}
		}
	}


	if (bgManager()->areaNum == AREANUMMAX - 1 )
	{
		const float POS_Y_INIT = 635.0f;
		static VECTOR2 goal_pos{ 637.0f, POS_Y_INIT };
		if (!bgManager()->mapStatus) {/* 反転前*/ goal_pos.y = POS_Y_INIT; }
		else { /* 反転後*/ goal_pos.y = abs(720.0f - POS_Y_INIT); }
		// 当たり判定の描画
		/*primitive::circle(playerManager_->position - VECTOR2(0, 32), 30.0f, { 1,1 }, 0, { 1,0,0,0.3f });
		primitive::circle(goal_pos, 55.0f, { 1,1 }, 0, { 0,0,1,0.3f });*/
	}


	UIManager()->draw();

	// 反転時のアニメーション
	inversion_mask_draw();


	// ポーズ画面
	if (isPause)  ScenePause::getInstance()->draw_game_scene();

	// フェードインフェードアウト
	if (FadeRectangle3::getInstance()->fadeFlg)
	{
		texture::begin(RECTANGLE);
		for (int i = 0; i < 11; ++i)
		{
			for (int o = 0; o < 23; ++o)
			{
				FadeRectangle3* fade = FadeRectangle3::getInstance();
				texture::draw(RECTANGLE, { 32.0f + 64.0f * o , 33.0f + 66.0f * i }, { 1.0f, 1.0f },
					{ 0, 0 }, { 64.0f, 66.0f }, { 32.0f, 33.0f }, ToRadian(0), fade->color[o + i * 20]);
			}
		}
		texture::end(RECTANGLE);
	}


	// クリア画面に行くときの蓋絵
	if (do_start_blackout) {
		texture::begin(TRANS);
		texture::draw(TRANS, { 0,0 }, { 1,1 },
			{ 0, 0 }, { 1450.0f, 720.0f }, { 0,0 }, ToRadian(0), { 0, 0, 0, trans_color_w });
		texture::end(TRANS);
	}
	// 落下死した時の蓋絵
	if (do_start_fall_blackout) {
		texture::begin(TRANS);
		texture::draw(TRANS, { 0,0 }, { 1,1 },
			{ 0, 0 }, { 1450.0f, 720.0f }, { 0,0 }, ToRadian(0), { 0, 0, 0, trans_fall_color_w });
		texture::end(TRANS);
	}
}

void SceneGame::stage1_draw_back()
{
#ifdef USE_IMGUI
	/*if (ShowSceneGame)
	{*/
		ImGui::Begin("mapHanten");
		static float posx;
		static float posy;
		static float ofx;
		static float ofy;
		static float scaleY;

		ImGui::SliderFloat("posx", &posx, 0, 1280);
		ImGui::SliderFloat("posy", &posy, 500, 850);
		ImGui::SliderFloat("ofx", &ofx, 0, 1280);
		ImGui::SliderFloat("ofy", &ofy, 0, 1280);
		ImGui::SliderFloat("kakouScale", &scaleY, -1, 1);
		ImGui::End();
	//}
#endif // USE_IMGUI
	float PosY;
	float ScaleY;
	if (SceneGame::instance()->bgManager()->mapStatus)
	{
		PosY = 720.0f;
		ScaleY = -1.0f;
	}
	else
	{
		PosY = 0.0f;
		ScaleY = 1.0f;
	}

	if (MoreAndLess(bgManager()->areaNum, 0, 3))
	{
		texture::begin(MAP_ST1);
		texture::draw(MAP_ST1, { 0,PosY }, { 1, ScaleY }, { 0, (bgManager()->areaNum % 4) * 720.0f }, { 1280, 720 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST1);
	}

	else if (MoreAndLess(bgManager()->areaNum, 4, 7))
	{
		texture::begin(MAP_ST2);
		texture::draw(MAP_ST2, { 0, PosY }, { 1, ScaleY }, { 0, (bgManager()->areaNum % 4) * 720.0f }, { 1280, 720 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2);
	}

	else if (MoreAndLess(bgManager()->areaNum, 8, 11))
	{
		texture::begin(MAP_ST3);
		texture::draw(MAP_ST3, { 0,PosY }, { 1, ScaleY }, { 0, (bgManager()->areaNum % 4) * 720.0f }, { 1280, 720 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3);
	}

	else if (MoreAndLess(bgManager()->areaNum, 12, 15))
	{
		texture::begin(MAP_ST4);
		texture::draw(MAP_ST4, { 0,PosY }, { 1, ScaleY }, { 0, (bgManager()->areaNum % 4) * 720.0f }, { 1280, 720 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4);
	}

	else if (MoreAndLess(bgManager()->areaNum, 16, 19))
	{
		texture::begin(MAP_ST5);
		texture::draw(MAP_ST5, { 0, PosY }, { 1,ScaleY }, { 0, (bgManager()->areaNum % 4) * 720.0f }, { 1280, 720 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST5);
	}

	else if (MoreAndLess(bgManager()->areaNum, 20, 23))
	{
		texture::begin(MAP_ST6);
		texture::draw(MAP_ST6, { 0, PosY }, { 1, ScaleY }, { 0, (bgManager()->areaNum % 4) * 720.0f }, { 1280, 720 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST6);
	}
}

void SceneGame::stage1_draw_kako_up()
{
#ifdef USE_IMGUI
	if (ShowSceneGame)
	{
		ImGui::Begin("kakou");
		static float posx;
		static float posy;
		static float ofx;
		static float ofy;

		ImGui::SliderFloat("posx", &posx, 0, 1280);
		ImGui::SliderFloat("posy", &posy, 0, 1280);
		ImGui::SliderFloat("ofx", &ofx, 0, 1280);
		ImGui::SliderFloat("ofy", &ofy, 0, 1280);
		ImGui::SliderFloat("kakouScale", &kakouScale, -1, 1);
		ImGui::End();
	}

#endif // USE_IMGUI
	if (bgManager()->areaNum == 0)
	{
		texture::begin(MAP_ST1_UP);
		texture::draw(MAP_ST1_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST1_UP);
	}
	else if (bgManager()->areaNum == 1)
	{
		texture::begin(MAP_ST2_UP);
		texture::draw(MAP_ST2_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_UP);
	}
	else if (bgManager()->areaNum == 2)
	{
		texture::begin(MAP_ST3_UP);
		texture::draw(MAP_ST3_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_UP);
	}
	else if (bgManager()->areaNum == 3)
	{
		texture::begin(MAP_ST4_UP);
		texture::draw(MAP_ST4_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_UP);
	}
	else if (bgManager()->areaNum == 4)
	{
		texture::begin(MAP_ST2_1_UP);
		texture::draw(MAP_ST2_1_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_1_UP);
	}
	else if (bgManager()->areaNum == 5)
	{
		texture::begin(MAP_ST2_2_UP);
		texture::draw(MAP_ST2_2_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_2_UP);
	}
	else if (bgManager()->areaNum == 6)
	{
		texture::begin(MAP_ST2_3_UP);
		texture::draw(MAP_ST2_3_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_3_UP);
	}
	else if (bgManager()->areaNum == 7)
	{
		texture::begin(MAP_ST2_4_UP);
		texture::draw(MAP_ST2_4_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_4_UP);
	}
	else if (bgManager()->areaNum == 8)
	{
		texture::begin(MAP_ST3_1_UP);
		texture::draw(MAP_ST3_1_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_1_UP);
	}
	else if (bgManager()->areaNum == 9)
	{
		texture::begin(MAP_ST3_2_UP);
		texture::draw(MAP_ST3_2_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_2_UP);
	}
	else if (bgManager()->areaNum == 10)
	{
		texture::begin(MAP_ST3_3_UP);
		texture::draw(MAP_ST3_3_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_3_UP);
	}
	else if (bgManager()->areaNum == 11)
	{
		texture::begin(MAP_ST3_4_UP);
		texture::draw(MAP_ST3_4_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_4_UP);
	}
	else if (bgManager()->areaNum == 12)
	{
		texture::begin(MAP_ST4_1_UP);
		texture::draw(MAP_ST4_1_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_1_UP);
	}
	else if (bgManager()->areaNum == 13)
	{
		texture::begin(MAP_ST4_2_UP);
		texture::draw(MAP_ST4_2_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_2_UP);
	}
	else if (bgManager()->areaNum == 14)
	{
		texture::begin(MAP_ST4_3_UP);
		texture::draw(MAP_ST4_3_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_3_UP);
	}
	else if (bgManager()->areaNum == 15)
	{
		texture::begin(MAP_ST4_4_UP);
		texture::draw(MAP_ST4_4_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_4_UP);
	}
	else if (bgManager()->areaNum == 20)
	{
		texture::begin(MAP_ST6_1_UP);
		texture::draw(MAP_ST6_1_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST6_1_UP);
	}
	else if (bgManager()->areaNum == 21)
	{
		texture::begin(MAP_ST6_2_UP);
		texture::draw(MAP_ST6_2_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST6_2_UP);
	}
	else if (bgManager()->areaNum == 22)
	{
		texture::begin(MAP_ST6_3_UP);
		texture::draw(MAP_ST6_3_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST6_3_UP);
	}
	else if (bgManager()->areaNum == 23)
	{
		texture::begin(MAP_ST6_4_UP);
		texture::draw(MAP_ST6_4_UP, { 0,368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST6_4_UP);
	}




}

void SceneGame::stage1_draw_kako_down()
{
	if (bgManager()->areaNum == 0)
	{
		texture::begin(MAP_ST1_DOWN);
		texture::draw(MAP_ST1_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST1_DOWN);
	}
	else if (bgManager()->areaNum == 1)
	{
		texture::begin(MAP_ST2_DOWN);
		texture::draw(MAP_ST2_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_DOWN);
	}
	else if (bgManager()->areaNum == 2)
	{
		texture::begin(MAP_ST3_DOWN);
		texture::draw(MAP_ST3_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_DOWN);
	}
	else if (bgManager()->areaNum == 3)
	{
		texture::begin(MAP_ST4_DOWN);
		texture::draw(MAP_ST4_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_DOWN);
	}
	else if (bgManager()->areaNum == 4)
	{
		texture::begin(MAP_ST2_1_DOWN);
		texture::draw(MAP_ST2_1_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_1_DOWN);
	}
	else if (bgManager()->areaNum == 5)
	{
		texture::begin(MAP_ST2_2_DOWN);
		texture::draw(MAP_ST2_2_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_2_DOWN);
	}
	else if (bgManager()->areaNum == 6)
	{
		texture::begin(MAP_ST2_3_DOWN);
		texture::draw(MAP_ST2_3_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_3_DOWN);
	}
	else if (bgManager()->areaNum == 7)
	{
		texture::begin(MAP_ST2_4_DOWN);
		texture::draw(MAP_ST2_4_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST2_4_DOWN);
	}
	else if (bgManager()->areaNum == 8)
	{
		texture::begin(MAP_ST3_1_DOWN);
		texture::draw(MAP_ST3_1_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_1_DOWN);
	}
	else if (bgManager()->areaNum == 9)
	{
		texture::begin(MAP_ST3_2_DOWN);
		texture::draw(MAP_ST3_2_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_2_DOWN);
	}
	else if (bgManager()->areaNum == 10)
	{
		texture::begin(MAP_ST3_3_DOWN);
		texture::draw(MAP_ST3_3_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_3_DOWN);
	}
	else if (bgManager()->areaNum == 11)
	{
		texture::begin(MAP_ST3_4_DOWN);
		texture::draw(MAP_ST3_4_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST3_4_DOWN);
	}
	else if (bgManager()->areaNum == 12)
	{
		texture::begin(MAP_ST4_1_DOWN);
		texture::draw(MAP_ST4_1_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_1_DOWN);
	}
	else if (bgManager()->areaNum == 13)
	{
		texture::begin(MAP_ST4_2_DOWN);
		texture::draw(MAP_ST4_2_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_2_DOWN);
	}
	else if (bgManager()->areaNum == 14)
	{
		texture::begin(MAP_ST4_3_DOWN);
		texture::draw(MAP_ST4_3_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_3_DOWN);
	}
	else if (bgManager()->areaNum == 15)
	{
		texture::begin(MAP_ST4_4_DOWN);
		texture::draw(MAP_ST4_4_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST4_4_DOWN);
	}
	else if (bgManager()->areaNum == 23)
	{
		texture::begin(MAP_ST6_4_DOWN);
		texture::draw(MAP_ST6_4_DOWN, { 0, 368 }, { 1, kakouScale }, { 0, 0 }, { 1280, 736 }, { 0, 368 }, 0, { 1, 1, 1, 1 });
		texture::end(MAP_ST6_4_DOWN);
	}
}

void SceneGame::inversion_mask_draw()
{
	static int mask_timer = 0;
	static bool is_start = false;
	if (playerManager_->get_is_start_inversion_mask_anime()) {
		is_start = true;
	}
	if (is_start) {
		++mask_timer;

		const int MASK_TOTAL = 20; // アニメーションが何コマか
		const int MASK_FRAME = 4; // アニメーションの１コマが何フレームか
		const int mask_now_frame = mask_timer / MASK_FRAME % MASK_TOTAL; // アニメーションが今何コマ目か

		if (mask_now_frame == MASK_TOTAL - 1) {
			is_start = false;
			mask_timer = 0;
		}

		const VECTOR2 pos{ 0,0 };
		const VECTOR2 scale{ 1.0f,1.0f };
		const VECTOR2 ts{ 1280.0f,736.0f };
		const VECTOR2 tp{ (float)mask_now_frame * ts.x, 0 };
		const VECTOR2 pi{ 0.0f,0.0f };
		const VECTOR4 cl{ 1.0f,1.0f,1.0f,1.0f };
		texture::begin(mask_now_frame + MASK_1);
		texture::draw(mask_now_frame + MASK_1, pos, scale, { 0,0 }, ts, pi, ToRadian(0), cl);
		texture::end(mask_now_frame + MASK_1);

	}
	else {
		mask_timer = 0;
	}
}