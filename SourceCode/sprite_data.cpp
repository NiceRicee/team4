//******************************************************************************
//
//
//		SPRITE_DATA
//
//
//******************************************************************************

//------< インクルード >----------------------------------------------------------
#include "all.h"

//------< using >---------------------------------------------------------------

//------< データ >---------------------------------------------------------------
//-------2D画像ロードデータ--------//
//TODO　背景画像差し替えること！！！！！
// ゲーム画面
LoadTexture loadTexture[] = {

    /*マップチップ*/
    { TEXNO::MAPCHIP1,          L"./Data/Maps/maptip_1.png",           static_cast<UINT>(BG::LOOP_X * BG::LOOP_Y) }, // マップチップ
    { TEXNO::MAPCHIP2,          L"./Data/Maps/mapchip_2.png",           static_cast<UINT>(BG::LOOP_X * BG::LOOP_Y) }, // マップチップ
    { TEXNO::MAP_ST1,         L"./Data/Maps/map_s1.png",                1U}, // ステージ1背景
    //{ TEXNO::MAP_ST1,           L"./Data/Maps/stage1.png",              1U}, // ステージ1枠
    { TEXNO::MAP_ST2,         L"./Data/Maps/map_s2.png",                1U}, // ステージ2背景
    //{ TEXNO::MAP_ST2,           L"./Data/Maps/stage2.png",              1U}, // ステージ2枠
    { TEXNO::MAP_ST3,         L"./Data/Maps/map_s3.png",                1U}, // ステージ3背景
    //{ TEXNO::MAP_ST3,           L"./Data/Maps/3stage.png",              1U}, // ステージ3枠
    { TEXNO::MAP_ST4,         L"./Data/Maps/map_s4.png",                1U}, // ステージ4背景
    //{ TEXNO::MAP_ST4,           L"./Data/Maps/stage4.png",            1U}, // ステージ4枠
    { TEXNO::MAP_ST5,         L"./Data/Maps/map_s5.png",              1U}, // ステージ5背景
    //{ TEXNO::MAP_ST5,           L"./Data/Maps/stage5.png",              1U}, // ステージ5枠
    { TEXNO::MAP_ST6,         L"./Data/Maps/map_s6.png",              1U}, // ステージ6背景
   // { TEXNO::MAP_ST6,           L"./Data/Maps/stage6.png",              1U}, // ステージ6枠
   
    { TEXNO::CHECK_POINT,       L"./Data/Maps/fire_point.png",           1U}, // チェックポイント
    { TEXNO::PLAYER,            L"./Data/Images/chara_array.png",           1U}, // プレイヤー
    { TEXNO::SIGNBOARD,         L"./Data/Maps/kanban.png",           5U}, // 看板
    { TEXNO::MINIMAP,           L"./Data/Maps/minimap.png",              1U}, // ミニマップ
    { TEXNO::MINI_CHARA,        L"./Data/Maps/face_minimap.png",              1U}, // ミニマップキャラ
    { TEXNO::MIST0,             L"./Data/Maps/mist0.png",              10U}, // 霧
    { TEXNO::MIST1,             L"./Data/Maps/mist1.png",              10U}, // 霧
    { TEXNO::MIST2,             L"./Data/Maps/mist2.png",              10U}, // 霧
    { TEXNO::MIST3,             L"./Data/Maps/mist3.png",              10U}, // 霧
    { TEXNO::SPEEDMATOR,        L"./Data/Maps/speed_mater.png",           1U}, //スピードメーター
    { TEXNO::LOGPOCE,           L"./Data/Maps/rogporse.png",           5U}, // 方向示すやつ
    { TEXNO::DANGER,            L"./Data/Maps/emargency.png",           1U}, //危険マーク
    { TEXNO::KEY,               L"./Data/Maps/key_icon.png",           10U}, //危険マーク

    // クリア画面
    { TEXNO::TEX_CLEAR,         L"./Data/Images/ed.png",           1U},
    // 風
    { TEXNO::TEX_WIND_OBJ,      L"./Data/Images/wind_ob.png",      1U},

   { -1, nullptr }	// 終了フラグ
};

// フェードインフェードアウト
LoadTexture loadTexture_fade[] = {

    // フェードインフェードアウト
    { TEXNO::RECTANGLE,                 L"./Data/Images/rectangle.png",           300U },

   { -1, nullptr }	// 終了フラグ
};

// タイトル画面
LoadTexture loadTexture_title[] = {

    // タイトル
    { TEXNO::TEX_TITLE_STRING_NEW,        L"./Data/Images/new.png",             1U}, // タイトル文字
    { TEXNO::TEX_TITLE_STRING_CON,        L"./Data/Images/continue.png",        1U}, // タイトル文字
    { TEXNO::TEX_TITLE_SELECT,            L"./Data/Images/select.png",          2U}, // タイトル文字
    { TEXNO::TEX_CONFIRMATION_COMMAND,    L"./Data/Images/press.png",           1U}, // タイトル文字

    //--------<title>--------//
    /*0~10*/
    { TEXNO::TITLE_0,                     L"./Data/Images/title/title000.png",                1U}, // タイトル
    { TEXNO::TITLE_1,                     L"./Data/Images/title/title001.png",                1U}, // タイトル
    { TEXNO::TITLE_2,                     L"./Data/Images/title/title002.png",                1U}, // タイトル
    { TEXNO::TITLE_3,                     L"./Data/Images/title/title003.png",                1U}, // タイトル
    { TEXNO::TITLE_4,                     L"./Data/Images/title/title004.png",                1U}, // タイトル
    { TEXNO::TITLE_5,                     L"./Data/Images/title/title005.png",                1U}, // タイトル
    { TEXNO::TITLE_6,                     L"./Data/Images/title/title006.png",                1U}, // タイトル
    { TEXNO::TITLE_7,                     L"./Data/Images/title/title007.png",                1U}, // タイトル
    { TEXNO::TITLE_8,                     L"./Data/Images/title/title008.png",                1U}, // タイトル
    { TEXNO::TITLE_9,                     L"./Data/Images/title/title009.png",                1U}, // タイトル
    { TEXNO::TITLE_10,                    L"./Data/Images/title/title010.png",                1U}, // タイトル
    /*11~20*/
    { TEXNO::TITLE_11,                    L"./Data/Images/title/title011.png",                1U}, // タイトル
    { TEXNO::TITLE_12,                    L"./Data/Images/title/title012.png",                1U}, // タイトル
    { TEXNO::TITLE_13,                    L"./Data/Images/title/title013.png",                1U}, // タイトル
    { TEXNO::TITLE_14,                    L"./Data/Images/title/title014.png",                1U}, // タイトル
    { TEXNO::TITLE_15,                    L"./Data/Images/title/title015.png",                1U}, // タイトル
    { TEXNO::TITLE_16,                    L"./Data/Images/title/title016.png",                1U}, // タイトル
    { TEXNO::TITLE_17,                    L"./Data/Images/title/title017.png",                1U}, // タイトル
    { TEXNO::TITLE_18,                    L"./Data/Images/title/title018.png",                1U}, // タイトル
    { TEXNO::TITLE_19,                    L"./Data/Images/title/title019.png",                1U}, // タイトル
    { TEXNO::TITLE_20,                    L"./Data/Images/title/title020.png",                1U}, // タイトル
    /*21~30*/
    { TEXNO::TITLE_21,                    L"./Data/Images/title/title021.png",                1U}, // タイトル
    { TEXNO::TITLE_22,                    L"./Data/Images/title/title022.png",                1U}, // タイトル
    { TEXNO::TITLE_23,                    L"./Data/Images/title/title023.png",                1U}, // タイトル
    { TEXNO::TITLE_24,                    L"./Data/Images/title/title024.png",                1U}, // タイトル
    { TEXNO::TITLE_25,                    L"./Data/Images/title/title025.png",                1U}, // タイトル
    { TEXNO::TITLE_26,                    L"./Data/Images/title/title026.png",                1U}, // タイトル
    { TEXNO::TITLE_27,                    L"./Data/Images/title/title027.png",                1U}, // タイトル
    { TEXNO::TITLE_28,                    L"./Data/Images/title/title028.png",                1U}, // タイトル
    { TEXNO::TITLE_29,                    L"./Data/Images/title/title029.png",                1U}, // タイトル
    { TEXNO::TITLE_30,                    L"./Data/Images/title/title030.png",                1U}, // タイトル
    /*31~40*/
    { TEXNO::TITLE_31,                    L"./Data/Images/title/title031.png",                1U}, // タイトル
    { TEXNO::TITLE_32,                    L"./Data/Images/title/title032.png",                1U}, // タイトル
    { TEXNO::TITLE_33,                    L"./Data/Images/title/title033.png",                1U}, // タイトル
    { TEXNO::TITLE_34,                    L"./Data/Images/title/title034.png",                1U}, // タイトル
    { TEXNO::TITLE_35,                    L"./Data/Images/title/title035.png",                1U}, // タイトル
    { TEXNO::TITLE_36,                    L"./Data/Images/title/title036.png",                1U}, // タイトル
    { TEXNO::TITLE_37,                    L"./Data/Images/title/title037.png",                1U}, // タイトル
    { TEXNO::TITLE_38,                    L"./Data/Images/title/title038.png",                1U}, // タイトル
    { TEXNO::TITLE_39,                    L"./Data/Images/title/title039.png",                1U}, // タイトル
    { TEXNO::TITLE_40,                    L"./Data/Images/title/title040.png",                1U}, // タイトル
    /*41~50*/
    { TEXNO::TITLE_41,                    L"./Data/Images/title/title041.png",                1U}, // タイトル
    { TEXNO::TITLE_42,                    L"./Data/Images/title/title042.png",                1U}, // タイトル
    { TEXNO::TITLE_43,                    L"./Data/Images/title/title043.png",                1U}, // タイトル
    { TEXNO::TITLE_44,                    L"./Data/Images/title/title044.png",                1U}, // タイトル
    { TEXNO::TITLE_45,                    L"./Data/Images/title/title045.png",                1U}, // タイトル
    { TEXNO::TITLE_46,                    L"./Data/Images/title/title046.png",                1U}, // タイトル
    { TEXNO::TITLE_47,                    L"./Data/Images/title/title047.png",                1U}, // タイトル
    { TEXNO::TITLE_48,                    L"./Data/Images/title/title048.png",                1U}, // タイトル
    { TEXNO::TITLE_49,                    L"./Data/Images/title/title049.png",                1U}, // タイトル
    { TEXNO::TITLE_50,                    L"./Data/Images/title/title050.png",                1U}, // タイトル
    /*51~60*/
    { TEXNO::TITLE_51,                    L"./Data/Images/title/title051.png",                1U}, // タイトル
    { TEXNO::TITLE_52,                    L"./Data/Images/title/title052.png",                1U}, // タイトル
    { TEXNO::TITLE_53,                    L"./Data/Images/title/title053.png",                1U}, // タイトル
    { TEXNO::TITLE_54,                    L"./Data/Images/title/title054.png",                1U}, // タイトル
    { TEXNO::TITLE_55,                    L"./Data/Images/title/title055.png",                1U}, // タイトル
    { TEXNO::TITLE_56,                    L"./Data/Images/title/title056.png",                1U}, // タイトル
    { TEXNO::TITLE_57,                    L"./Data/Images/title/title057.png",                1U}, // タイトル
    { TEXNO::TITLE_58,                    L"./Data/Images/title/title058.png",                1U}, // タイトル
    { TEXNO::TITLE_59,                    L"./Data/Images/title/title059.png",                1U}, // タイトル
    { TEXNO::TITLE_60,                    L"./Data/Images/title/title060.png",                1U}, // タイトル
    /*61~70*/
    { TEXNO::TITLE_61,                    L"./Data/Images/title/title061.png",                1U}, // タイトル
    { TEXNO::TITLE_62,                    L"./Data/Images/title/title062.png",                1U}, // タイトル
    { TEXNO::TITLE_63,                    L"./Data/Images/title/title063.png",                1U}, // タイトル
    { TEXNO::TITLE_64,                    L"./Data/Images/title/title064.png",                1U}, // タイトル
    { TEXNO::TITLE_65,                    L"./Data/Images/title/title065.png",                1U}, // タイトル
    { TEXNO::TITLE_66,                    L"./Data/Images/title/title066.png",                1U}, // タイトル
    { TEXNO::TITLE_67,                    L"./Data/Images/title/title067.png",                1U}, // タイトル
    { TEXNO::TITLE_68,                    L"./Data/Images/title/title068.png",                1U}, // タイトル
    { TEXNO::TITLE_69,                    L"./Data/Images/title/title069.png",                1U}, // タイトル
    { TEXNO::TITLE_70,                    L"./Data/Images/title/title070.png",                1U}, // タイトル
    /*71~80*/
    { TEXNO::TITLE_71,                    L"./Data/Images/title/title071.png",                1U}, // タイトル
    { TEXNO::TITLE_72,                    L"./Data/Images/title/title072.png",                1U}, // タイトル
    { TEXNO::TITLE_73,                    L"./Data/Images/title/title073.png",                1U}, // タイトル
    { TEXNO::TITLE_74,                    L"./Data/Images/title/title074.png",                1U}, // タイトル
    { TEXNO::TITLE_75,                    L"./Data/Images/title/title075.png",                1U}, // タイトル
    { TEXNO::TITLE_76,                    L"./Data/Images/title/title076.png",                1U}, // タイトル
    { TEXNO::TITLE_77,                    L"./Data/Images/title/title077.png",                1U}, // タイトル
    { TEXNO::TITLE_78,                    L"./Data/Images/title/title078.png",                1U}, // タイトル
    { TEXNO::TITLE_79,                    L"./Data/Images/title/title079.png",                1U}, // タイトル
    { TEXNO::TITLE_80,                    L"./Data/Images/title/title080.png",                1U}, // タイトル
    /*81~83*/
    { TEXNO::TITLE_81,                    L"./Data/Images/title/title081.png",                1U }, // タイトル
    { TEXNO::TITLE_82,                    L"./Data/Images/title/title082.png",                1U }, // タイトル
    { TEXNO::TITLE_83,                    L"./Data/Images/title/title083.png",                1U }, // タイトル

    //--------<fall_light>--------//
    /*1~10*/
    { TEXNO::FALL_LIGHT_1,                L"./Data/Images/fall_light/fall_light_01.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_2,                L"./Data/Images/fall_light/fall_light_02.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_3,                L"./Data/Images/fall_light/fall_light_03.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_4,                L"./Data/Images/fall_light/fall_light_04.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_5,                L"./Data/Images/fall_light/fall_light_05.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_6,                L"./Data/Images/fall_light/fall_light_06.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_7,                L"./Data/Images/fall_light/fall_light_07.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_8,                L"./Data/Images/fall_light/fall_light_08.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_9,                L"./Data/Images/fall_light/fall_light_09.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_10,               L"./Data/Images/fall_light/fall_light_10.png",                1U }, // タイトル
    /*11~20*/
    { TEXNO::FALL_LIGHT_11,                L"./Data/Images/fall_light/fall_light_11.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_12,                L"./Data/Images/fall_light/fall_light_12.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_13,                L"./Data/Images/fall_light/fall_light_13.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_14,                L"./Data/Images/fall_light/fall_light_14.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_15,                L"./Data/Images/fall_light/fall_light_15.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_16,                L"./Data/Images/fall_light/fall_light_16.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_17,                L"./Data/Images/fall_light/fall_light_17.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_18,                L"./Data/Images/fall_light/fall_light_18.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_19,                L"./Data/Images/fall_light/fall_light_19.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_20,                L"./Data/Images/fall_light/fall_light_20.png",                1U }, // タイトル
    /*21~25*/
    { TEXNO::FALL_LIGHT_21,                L"./Data/Images/fall_light/fall_light_21.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_22,                L"./Data/Images/fall_light/fall_light_22.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_23,                L"./Data/Images/fall_light/fall_light_23.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_24,                L"./Data/Images/fall_light/fall_light_24.png",                1U }, // タイトル
    { TEXNO::FALL_LIGHT_25,                L"./Data/Images/fall_light/fall_light_25.png",                1U }, // タイトル


   { -1, nullptr }	// 終了フラグ
};

// ポーズ画面画面
LoadTexture loadTexture_pause[] = {
    { TEXNO::TRANS,                     L"./Data/Images/trans.png",               1U },

    // ポーズ説明
    { TEXNO::PORSE_BOAD,         L"./Data/Images/porse_boad.png",           10U}, // ポーズ説明
    { TEXNO::TUTORIAL,           L"./Data/Maps/kanban_moji.png",           10U}, // ポーズ説明

    // 戻るボタン
    { TEXNO::BACK_MARK,          L"./Data/Images/back_mark.png",           1U}, // ポーズ説明


   { -1, nullptr }	// 終了フラグ
};

// ポーズ画面画面
LoadTexture loadTexture_mask[] = {
    { TEXNO::MASK_1,   L"./Data/Images/inversion_mask/mask_1.png",           1U},
    { TEXNO::MASK_2,   L"./Data/Images/inversion_mask/mask_2.png",           1U},
    { TEXNO::MASK_3,   L"./Data/Images/inversion_mask/mask_3.png",           1U},
    { TEXNO::MASK_4,   L"./Data/Images/inversion_mask/mask_4.png",           1U},
    { TEXNO::MASK_5,   L"./Data/Images/inversion_mask/mask_5.png",           1U},
    { TEXNO::MASK_6,   L"./Data/Images/inversion_mask/mask_6.png",           1U},
    { TEXNO::MASK_7,   L"./Data/Images/inversion_mask/mask_7.png",           1U},
    { TEXNO::MASK_8,   L"./Data/Images/inversion_mask/mask_8.png",           1U},
    { TEXNO::MASK_9,   L"./Data/Images/inversion_mask/mask_9.png",           1U},
    { TEXNO::MASK_10,  L"./Data/Images/inversion_mask/mask_10.png",           1U},
    { TEXNO::MASK_11,  L"./Data/Images/inversion_mask/mask_11.png",           1U},
    { TEXNO::MASK_12,  L"./Data/Images/inversion_mask/mask_12.png",           1U},
    { TEXNO::MASK_13,  L"./Data/Images/inversion_mask/mask_13.png",           1U},
    { TEXNO::MASK_14,  L"./Data/Images/inversion_mask/mask_14.png",           1U},
    { TEXNO::MASK_15,  L"./Data/Images/inversion_mask/mask_15.png",           1U},
    { TEXNO::MASK_16,  L"./Data/Images/inversion_mask/mask_16.png",           1U},
    { TEXNO::MASK_17,  L"./Data/Images/inversion_mask/mask_17.png",           1U},
    { TEXNO::MASK_18,  L"./Data/Images/inversion_mask/mask_18.png",           1U},
    { TEXNO::MASK_19,  L"./Data/Images/inversion_mask/mask_19.png",           1U},
    { TEXNO::MASK_20,  L"./Data/Images/inversion_mask/mask_20.png",           1U},


     { TEXNO::MAP_ST1_UP,        L"./Data/Maps/kako/1-1_up.png",              1U}, // ステージ1被せ
    { TEXNO::MAP_ST1_DOWN,      L"./Data/Maps/kako/1-1_down.png",              1U}, // ステージ1被せ
    { TEXNO::MAP_ST2_UP,        L"./Data/Maps/kako/1-2_up.png",              1U}, // ステージ2被せ
    { TEXNO::MAP_ST2_DOWN,      L"./Data/Maps/kako/1-2_back.png",              1U}, // ステージ2被せ
    { TEXNO::MAP_ST3_UP,        L"./Data/Maps/kako/1-3_up.png",              1U}, // ステージ3被せ
    { TEXNO::MAP_ST3_DOWN,      L"./Data/Maps/kako/1-3_back.png",              1U}, // ステージ3被せ
    { TEXNO::MAP_ST4_UP,        L"./Data/Maps/kako/1-4_up.png",              1U}, // ステージ4被せ
    { TEXNO::MAP_ST4_DOWN,      L"./Data/Maps/kako/1-4_back.png",              1U}, // ステージ4被せ
    { TEXNO::MAP_ST2_1_UP,      L"./Data/Maps/kako/2-1_up.png",              1U}, // ステージ2-1被せ
    { TEXNO::MAP_ST2_1_DOWN,    L"./Data/Maps/kako/2-1_back.png",              1U}, // ステージ2-1被せ
    { TEXNO::MAP_ST2_2_UP,      L"./Data/Maps/kako/2-2_up.png",              1U}, // ステージ2-2被せ
    { TEXNO::MAP_ST2_2_DOWN,    L"./Data/Maps/kako/2-2_back.png",              1U}, // ステージ2-2被せ
    { TEXNO::MAP_ST2_3_UP,      L"./Data/Maps/kako/2-3_up.png",              1U}, // ステージ2-3被せ
    { TEXNO::MAP_ST2_3_DOWN,    L"./Data/Maps/kako/2-3_back.png",              1U}, // ステージ2-3被せ
    { TEXNO::MAP_ST2_4_UP,      L"./Data/Maps/kako/2-4_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST2_4_DOWN,    L"./Data/Maps/kako/2-4_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_1_UP,      L"./Data/Maps/kako/3-1_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_1_DOWN,    L"./Data/Maps/kako/3-1_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_2_UP,      L"./Data/Maps/kako/3-2_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_2_DOWN,    L"./Data/Maps/kako/3-2_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_3_UP,      L"./Data/Maps/kako/3-3_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_3_DOWN,    L"./Data/Maps/kako/3-3_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_4_UP,      L"./Data/Maps/kako/3-4_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST3_4_DOWN,    L"./Data/Maps/kako/3-4_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_1_UP,      L"./Data/Maps/kako/4-1_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_1_DOWN,    L"./Data/Maps/kako/4-1_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_2_UP,      L"./Data/Maps/kako/4-2_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_2_DOWN,    L"./Data/Maps/kako/4-2_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_3_UP,      L"./Data/Maps/kako/4-3_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_3_DOWN,    L"./Data/Maps/kako/4-3_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_4_UP,      L"./Data/Maps/kako/4-4_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST4_4_DOWN,    L"./Data/Maps/kako/4-4_back.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST6_1_UP,      L"./Data/Maps/kako/6-1_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST6_2_UP,      L"./Data/Maps/kako/6-2_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST6_3_UP,      L"./Data/Maps/kako/6-3_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST6_4_UP,      L"./Data/Maps/kako/6-4_up.png",              1U}, // ステージ2-4被せ
    { TEXNO::MAP_ST6_4_DOWN,    L"./Data/Maps/kako/stage_6-4_back.png",              1U}, // ステージ2-4被せ

   { -1, nullptr }	// 終了フラグ
};

//******************************************************************************
