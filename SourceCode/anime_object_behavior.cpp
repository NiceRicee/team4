#include "all.h"
#include "user.h"
#include"anime_object_behavior.h"

bool Bonfire::anime_movement()
{
    switch (anime_switch)
    {
    case 0: // 初期設定
        anime_case = BEFORE_IGNITION;

        for (int i = 0; i < PARTICLE_NUM; ++i) {
            anime_pos.push_back(center);
            anime_scale.push_back({ 1.0f, 1.0f });
            anime_texpos.push_back({ 0.0f, 0.0f });
            anime_texsize.push_back({ 32.0f, 96.0f });
            anime_pivot.push_back({ 32.0f / 2.0f, 96.0f / 2.0f });
            anime_angle.push_back(0.0f);
            anime_color.push_back({ 1.0f, 1.0f, 1.0f, 1.0f });
        }

        anime_switch = 3;
        break;

    case 1:
        animeSet(AFTER_IGNITION);

        anime_switch = 3;
        break;

    case 2:
        animeSet(BEFORE_IGNITION);

        anime_switch = 3;
        break;

    case 3: // 通常状態
        if (anime_case == BEFORE_IGNITION && is_ignition_bonfire[SceneGame::instance()->bgManager()->areaNum]) {
            anime_switch = 1;
        }
        if (anime_case == AFTER_IGNITION && !is_ignition_bonfire[SceneGame::instance()->bgManager()->areaNum]) {
            anime_switch = 2;
        }

        //debug::setString("is_ignition_bonfire[%d]:%d",
        //    SceneGame::instance()->bgManager()->areaNum, is_ignition_bonfire[SceneGame::instance()->bgManager()->areaNum]);

        switch (anime_case)
        {
        case BEFORE_IGNITION:
            animation(before_bonfire_anime_data); break;
        case AFTER_IGNITION:
            animation(after_bonfire_anime_data);  break;
        }


        if (!SceneGame::instance()->bgManager()->mapStatus) {
            // 反転前
            for (int i = 0; i < (int)anime_pos.size(); ++i) {
                anime_scale[i].y = 1.0f;
                anime_pos[i].y = center.y;
            }
        }
        else {
            // 反転後
            for (int i = 0; i < (int)anime_pos.size(); ++i) {
                anime_scale[i].y = -1.0f;
                anime_pos[i].y = abs(720.0f - center.y) + 17.0f;
            }
        }

        //debug::setString("anime_pos[0].y:%f", anime_pos[0].y);

        ++timer;
        break;
    }
    return false;
}

void Bonfire::anime_draw()
{
    for (int i = 0; i < (int)anime_pos.size(); ++i) {
        texture::begin(CHECK_POINT);
        texture::draw(CHECK_POINT, anime_pos[i], anime_scale[i],
            anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
        texture::end(CHECK_POINT);
    }
}

bool SignBoard::anime_movement()
{
    switch (anime_switch)
    {
    case 0: // 初期設定
        anime_case = 0;

        for (int i = 0; i < OBJ_NUM; ++i) {
            anime_pos.push_back(pos_init[i]);
            anime_scale.push_back({ 1.0f, 1.0f });
            anime_texpos.push_back({ 0.0f, 0.0f });
            anime_texsize.push_back({ 64.0f, 64.0f });
            anime_pivot.push_back({ 64.0f / 2.0f, 64.0f / 2.0f });
            anime_angle.push_back(0.0f);
            anime_color.push_back({ 1.0f, 1.0f, 1.0f, 1.0f });
        }

        ++anime_switch;
        /*fallthrough*/

    case 1: // 通常状態
        switch (anime_case)
        {
        case 0:
            animation(before_signBoard_anime_data); break;
        }


        if (!SceneGame::instance()->bgManager()->mapStatus) {
            // 反転前
            for (int i = 0; i < (int)anime_pos.size(); ++i) {
                anime_scale[i].y = 1.0f;
                anime_pos[i].y = pos_init[i].y;
            }
        }
        else {
            // 反転後
            for (int i = 0; i < (int)anime_pos.size(); ++i) {
                anime_scale[i].y = -1.0f;
                anime_pos[i].y = abs(720.0f - pos_init[i].y) + 17.0f;

            }
        }

        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            if (hitCheckCircle(anime_pos[i], 30.0f, SceneGame::instance()->playerManager()->position - VECTOR2(0, 32), 30.0f)) {
                is_display[i] = true;
            }
            else {
                is_display[i] = false;
            }
        }


        //debug::setString("anime_pos[0].y:%f", anime_pos[0].y);


        ++timer;
        break;
    }
    return false;
}

void SignBoard::anime_draw()
{
    for (int i = 0; i < (int)anime_pos.size(); ++i) {
        texture::begin(SIGNBOARD);
        texture::draw(SIGNBOARD, anime_pos[i], anime_scale[i],
            anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
        texture::end(SIGNBOARD);
    }
}

bool Wind_obj1::anime_movement()
{
    switch (anime_switch)
    {
    case 0: // 初期設定
        for (int i = 0; i < OBJ_NUM; ++i) {
            center.y = static_cast<float>((rand() % 12 + 1)) * 57.0f + 10.0f;
            anime_pos.push_back(center);
            anime_scale.push_back({ 1.0f, 1.0f });
            anime_texpos.push_back({ 0.0f, 0.0f });
            anime_texsize.push_back({ 32.0f, 32.0f });
            anime_pivot.push_back({ 32.0f / 2.0f, 32.0f / 2.0f });
            anime_angle.push_back(0.0f);
            anime_color.push_back({ 1.0f, 1.0f, 1.0f, 1.0f });
        }

        ++anime_switch;
        /*fallthrough*/

    case 1: // 通常状態
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            VECTOR2 speed;
            if (wind_dire) { speed = { -OBJ_SPEED,0 }; }
            else { speed = { OBJ_SPEED,0 }; }
            anime_pos[i] += speed;
        }

        // オブジェクトの終了合図
        if (wind_dire) {
            if (anime_pos[0].x < -50) {
                for (int i = 0; i < (int)anime_pos.size(); ++i) {
                    anime_pos.pop_back();
                    anime_scale.pop_back();
                    anime_texpos.pop_back();
                    anime_texsize.pop_back();
                    anime_pivot.pop_back();
                    anime_angle.pop_back();
                    anime_color.pop_back();
                }
                return true;
            }
        }
        else {
            if (anime_pos[0].x > 1280) {
                for (int i = 0; i < (int)anime_pos.size(); ++i) {
                    anime_pos.pop_back();
                    anime_scale.pop_back();
                    anime_texpos.pop_back();
                    anime_texsize.pop_back();
                    anime_pivot.pop_back();
                    anime_angle.pop_back();
                    anime_color.pop_back();
                }
                return true;
            }
        }


        ++timer;
        break;
    }
    return false;
}

void Wind_obj1::anime_draw()
{
    if (wind_dire) {
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            texture::begin(TEX_WIND_OBJ);
            texture::draw(TEX_WIND_OBJ, anime_pos[i], anime_scale[i],
                anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
            texture::end(TEX_WIND_OBJ);
        }
    }
    else {
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            texture::begin(TEX_WIND_OBJ);
            texture::draw(TEX_WIND_OBJ, anime_pos[i], { -anime_scale[i].x, anime_scale[i].y },
                anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
            texture::end(TEX_WIND_OBJ);
        }
    }
}

bool Wind_obj2::anime_movement()
{
    switch (anime_switch)
    {
    case 0: // 初期設定
        for (int i = 0; i < OBJ_NUM; ++i) {
            center.y = static_cast<float>((rand() % 12 + 1)) * 57.0f + 10.0f;
            anime_pos.push_back(center);
            anime_scale.push_back({ 1.0f, 1.0f });
            anime_texpos.push_back({ 32.0f, 0.0f });
            anime_texsize.push_back({ 32.0f, 32.0f });
            anime_pivot.push_back({ 32.0f / 2.0f, 32.0f / 2.0f });
            anime_angle.push_back(static_cast<float>((rand() % 10 + 1)) * 36.0f);
            anime_color.push_back({ 1.0f, 1.0f, 1.0f, 1.0f });
        }

        ++anime_switch;
        /*fallthrough*/

    case 1: // 通常状態
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            VECTOR2 speed;
            if (wind_dire) { speed = { -OBJ_SPEED,0 }; }
            else { speed = { OBJ_SPEED,0 }; }
            anime_pos[i] += speed;
            switch (move_state)
            {
            case 0: // 最初真っ直ぐ動く
                if (timer > 80) ++move_state;
                break;
            case 1:
                angle_base = anime_angle[i];
                ++move_state;
                /*fallthrough*/
            case 2:
                if (wind_dire) {
                    anime_angle[i] -= OBJ_SPEED_ROTATION;
                    if (anime_angle[i] <= angle_base - 360.0f) { anime_angle[i] = angle_base; move_state = 0; timer = 0; };
                }
                else {
                    anime_angle[i] += OBJ_SPEED_ROTATION;
                    if (anime_angle[i] >= angle_base + 360.0f) { anime_angle[i] = angle_base; move_state = 0; timer = 0; };
                }
                break;
            }
        }

        // オブジェクトの終了合図
        if (wind_dire) {
            if (anime_pos[0].x < -50) {
                for (int i = 0; i < (int)anime_pos.size(); ++i) {
                    anime_pos.pop_back();
                    anime_scale.pop_back();
                    anime_texpos.pop_back();
                    anime_texsize.pop_back();
                    anime_pivot.pop_back();
                    anime_angle.pop_back();
                    anime_color.pop_back();
                }
                return true;
            }
        }
        else {
            if (anime_pos[0].x > 1280) {
                for (int i = 0; i < (int)anime_pos.size(); ++i) {
                    anime_pos.pop_back();
                    anime_scale.pop_back();
                    anime_texpos.pop_back();
                    anime_texsize.pop_back();
                    anime_pivot.pop_back();
                    anime_angle.pop_back();
                    anime_color.pop_back();
                }
                return true;
            }
        }

        ++timer;
        break;
    }
    return false;
}

void Wind_obj2::anime_draw()
{
    if (wind_dire) {
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            texture::begin(TEX_WIND_OBJ);
            texture::draw(TEX_WIND_OBJ, anime_pos[i], anime_scale[i],
                anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
            texture::end(TEX_WIND_OBJ);
        }
    }
    else {
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            texture::begin(TEX_WIND_OBJ);
            texture::draw(TEX_WIND_OBJ, anime_pos[i], { -anime_scale[i].x, anime_scale[i].y },
                anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
            texture::end(TEX_WIND_OBJ);
        }
    }
}

bool Wind_obj3::anime_movement()
{
    switch (anime_switch)
    {
    case 0: // 初期設定
        for (int i = 0; i < OBJ_NUM; ++i) {
            center.y = static_cast<float>((rand() % 12 + 1)) * 57.0f + 10.0f;
            anime_pos.push_back(center);
            anime_scale.push_back({ 1.0f, 1.0f });
            anime_texpos.push_back({ 64.0f, 0.0f });
            anime_texsize.push_back({ 32.0f, 32.0f });
            anime_pivot.push_back({ 32.0f / 2.0f, 32.0f / 2.0f });
            anime_angle.push_back(0.0f);
            anime_color.push_back({ 1.0f, 1.0f, 1.0f, 1.0f });
        }

        ++anime_switch;
        /*fallthrough*/

    case 1: // 通常状態
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            VECTOR2 speed;
            if (wind_dire) { speed = { -OBJ_SPEED,0 }; }
            else { speed = { OBJ_SPEED,0 }; }
            anime_pos[i] += speed;
            switch (move_state)
            {
            case 0: // 最初真っ直ぐ動く
                if (timer > 20) ++move_state;
                break;

            case 1:
                if (wind_dire) {
                    anime_angle[i] -= OBJ_SPEED_ROTATION;
                    if (anime_angle[i] <= -360.0f) { anime_angle[i] = 0.0f; move_state = 0; timer = 0; };
                }
                else {
                    anime_angle[i] += OBJ_SPEED_ROTATION;
                    if (anime_angle[i] >= 360.0f) { anime_angle[i] = 0.0f; move_state = 0; timer = 0; }
                }
                break;
            }
        }

        // オブジェクトの終了合図
        if (wind_dire) {
            if (anime_pos[0].x < -50) {
                for (int i = 0; i < (int)anime_pos.size(); ++i) {
                    anime_pos.pop_back();
                    anime_scale.pop_back();
                    anime_texpos.pop_back();
                    anime_texsize.pop_back();
                    anime_pivot.pop_back();
                    anime_angle.pop_back();
                    anime_color.pop_back();
                }
                return true;
            }
        }
        else {
            if (anime_pos[0].x > 1280) {
                for (int i = 0; i < (int)anime_pos.size(); ++i) {
                    anime_pos.pop_back();
                    anime_scale.pop_back();
                    anime_texpos.pop_back();
                    anime_texsize.pop_back();
                    anime_pivot.pop_back();
                    anime_angle.pop_back();
                    anime_color.pop_back();
                }
                return true;
            }
        }

        ++timer;
        break;
    }
    return false;
}

void Wind_obj3::anime_draw()
{
    if (wind_dire) {
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            texture::begin(TEX_WIND_OBJ);
            texture::draw(TEX_WIND_OBJ, anime_pos[i], anime_scale[i],
                anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
            texture::end(TEX_WIND_OBJ);
        }
    }
    else {
        for (int i = 0; i < (int)anime_pos.size(); ++i) {
            texture::begin(TEX_WIND_OBJ);
            texture::draw(TEX_WIND_OBJ, anime_pos[i], { -anime_scale[i].x, anime_scale[i].y },
                anime_texpos[i], anime_texsize[i], anime_pivot[i], ToRadian(anime_angle[i]), anime_color[i]);
            texture::end(TEX_WIND_OBJ);
        }
    }
}