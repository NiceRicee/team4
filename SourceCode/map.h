//******************************************************************************
//
//
//      bg.h
//
//
//******************************************************************************

#include <sstream>
#include <bitset>
#include<cassert>
using namespace std;


//==============================================================================
//
//      BGクラス
//
//==============================================================================
class BG
{
public:
    //------< 定数 >------------------------------------------------------------
    static const int WINDOW_W = 1280;                   // ウインドウの幅
    static const int WINDOW_H = 720;                    // ウインドウの高さ
    static const int CLIENT_W = 1280;                   // クライアント領域の幅
    static const int CLIENT_H = 720;                    // クライアント領域の高さ

    static const int CHIP_SIZE = 32;                    // %演算子を使用するためint型を使用する
    static const int LOOP_X = CLIENT_W / CHIP_SIZE + (CLIENT_W % CHIP_SIZE ? 2 : 1);
    static const int LOOP_Y = CLIENT_H / CHIP_SIZE + (CLIENT_H % CHIP_SIZE ? 2 : 1);

    static const int CHIP_NUM_X = 40;                    // マップの横方向のチップ数
    static const int CHIP_NUM_Y = 23;                    // マップの縦方向のチップ数
    static const int CHIP_NUM_PER_LINE = 4;             // マップチップの1列が4個
    static const int CHIP_LINE_NUM = 5;                 // マップチップが5行

    static constexpr float WIDTH = static_cast<float>(CHIP_NUM_X * CHIP_SIZE);  // マップの幅（ドット）
    static constexpr float HEIGHT = static_cast<float>(CHIP_NUM_Y * CHIP_SIZE); // マップの高さ（ドット）
    static constexpr float SCROLL_MERGIN_X = 440.f;     // この数値より画面端に近づいたらスクロールする（横）
    static constexpr float SCROLL_MERGIN_Y = 360.f;     // この数値より画面端に近づいたらスクロールする（縦）
    static constexpr float ADJUST_X = 0.25f;           // あたり判定での位置調整用(X)
    static constexpr float ADJUST_Y = 0.025f;            // あたり判定での位置調整用(Y)


        // 地形（Terrain）の属性
    enum TR_ATTR
    {
        TR_NONE = -1,   // -1:何もなし
        ALL_BLOCK,      //  0:床ブロック
        WALL_BLOCK,      //  1:壁(仮)ブロック
        L_SLIP_BLOCK, // 3:チェックポイント
        R_SLIP_BLOCK, // 3:チェックポイント
        WATER,
        KOORI
    };

    // 背景（Back）の属性
    enum BG_ATTR
    {
        BG_NONE = -1,   // -1:何もなし
        NORMAL,         //  0:普通（特に何もなし）
    };
private:
    //------< 変数 >------------------------------------------------------------
    VECTOR2 scroll;                                         // 現在表示されている左上の地点の座標
protected:
   
public:
    bool isDebugDraw;                                       // デバックフラグ

    char back[CHIP_NUM_Y][CHIP_NUM_X];                      // 背景データ
    char terrain[CHIP_NUM_Y][CHIP_NUM_X];                   // 地形データ
    BG();
    virtual ~BG();

    // 純粋仮想関数
    virtual void init() = 0;
                                        // 背景描画
    virtual void drawTerrain() = 0;                                     // 地形描画
    
    virtual TR_ATTR getTerrainAttr(const VECTOR2&) = 0;

    // 更新
    void update();

    //エリア移動
    int areaNum;

    //マップチップ変数
    int texNo;
    //反転
    bool mapStatus;
    void hanten(char terrain[CHIP_NUM_Y][CHIP_NUM_X]);

    // スクロール位置取得
    float getScrollX() { return scroll.x; }
    float getScrollY() { return scroll.y; }
    const VECTOR2& getScrollPos() { return scroll; }


    // 当たり判定
   // 下方向
    bool isFloor(float, float, float);          // 床にめり込んでいるか
    void mapHoseiDown(VECTOR2*, VECTOR2* );                  // 下方向補正処理

    // 上方向
    bool isCeiling(float, float, float);        // 天井にあたっているか
    void mapHoseiUp(VECTOR2*, VECTOR2*, VECTOR2*);                    // 上方向補正処理

    // 横方向
    bool isWall(float, float, float);           // 横方向に壁にめり込んでいるか
    void mapHoseiRight(VECTOR2* , VECTOR2* , VECTOR2* );                 // 右方向補正処理
    void mapHoseiLeft(VECTOR2* , VECTOR2* , VECTOR2* );                  // 左方向補正処理

    //滑る床
    bool isSlipfloor(float, float, float);
    void slipRight(VECTOR2*, VECTOR2*);
    void slipLeft(VECTOR2*, VECTOR2* );
    bool SlipFloorisRight;

    bool isSuberuFloor(float, float, float);
    // 抵抗
    float calcResistance(VECTOR2* position, VECTOR2* size, float speed);
    float calcResistanceX(VECTOR2* position, VECTOR2* size, float speed);
    float getTeikou(TR_ATTR, float);
    float getTeikouX(TR_ATTR, float);
    bool isInWater(const VECTOR2 position);
   
protected:
    // クリア
    void clear();
    // マップデータのロード
    bool loadMapData(const char[][CHIP_NUM_X], char[][CHIP_NUM_X]);
    
    int getData(char[][BG::CHIP_NUM_X], const VECTOR2&);
    // BG、Terrain共通の描画関数

    void draw(int, char[][CHIP_NUM_X]);  // 描画処理（スムーズなスクロール）

private:
    // マップスクロール用
    //void scrollMap0();//デバック用スクロール（カーソルキーでスクロール）
    void scrollMap();//プレイヤーの動きに合わせてスクロール

    bool isHitAll(float, float);

    bool isHitDown(float, float);

    bool isHitSlope(float, float);//坂用

    bool isHitSuberu(float, float);//統べる床用

   
};



