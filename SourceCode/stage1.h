#pragma once
#include "checkPoint.h"

//==============================================================================
//
//      MapSt1
//
//==============================================================================

class MapSt1 : public BG
{
private:
    // 地形チップテクスチャの各部分の属性を定義する
    const TR_ATTR terrainAttrSt1[CHIP_LINE_NUM * CHIP_NUM_PER_LINE] = {
        // 1行目
        WALL_BLOCK, WALL_BLOCK, WALL_BLOCK, TR_NONE, //0~3
        L_SLIP_BLOCK, R_SLIP_BLOCK,WALL_BLOCK,WALL_BLOCK,  //4~7
        KOORI,WATER,TR_NONE,TR_NONE, //8~11
        L_SLIP_BLOCK,R_SLIP_BLOCK,KOORI,KOORI, //12~15
        WATER,WATER,WATER,WATER //16~19
    };
    

public:
    MapSt1() : BG() {}
    // 初期化
    void init() override;

    void drawTerrain() override;                    // 地形描画
    TR_ATTR getTerrainAttr(const VECTOR2&) override;

    ~MapSt1() {}

};
