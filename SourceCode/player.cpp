//**************************************************************************
//
//
//      プレイヤークラス
//
//
//**************************************************************************

//------< インクルード >-----------------------------------------------------
#include"all.h"
#include "user.h"
//------< using >-----------------------------------------------


//--------------------------------------------------------------
//  コンストラクタ
//--------------------------------------------------------------
Player::Player() : OBJ2D()
{
    state = 0;
    timer = 0;
    coolTimer = 0;

    graviTimer = 0;
    fallSpeed = 0;
    reverseLimit = 2;
    slopeTimer = 0;
    slopeTimerMAX = 15;
    fallTimer = 0;
    reverseState = 0;
    reverseAnimeFlg = false;
    suberuFlg = false;
    slopeFlg = false;
    deathFlg = false;
    is_start_inversion_mask_anime = false;

    pClear();
}
//--------------------------------------------------------------
//  メンバ変数のクリア
//--------------------------------------------------------------
void Player::pClear()
{

}
//--------------------------------------------------------------
//  初期設定
//--------------------------------------------------------------
void Player::init()
{
    state = 0;
    timer = 0;
    deathFlg = false;
}
//--------------------------------------------------------------
//  終了処理
//--------------------------------------------------------------
void Player::deinit()
{

}
//--------------------------------------------------------------
//  更新処理
//--------------------------------------------------------------
void Player::update()
{
	switch (state)
	{
	case 0:
		//////// 初期設定 ///////

		++state;
		/*fallthrough*/

	case 1:
		//////// パラメーターの設定 ////////
		objAct = Player::IDLE_INIT;
		scale = { 1.0f, 1.0f };
		texPos = { 0, 0 };
		texSize = { PLAYER_TEX_W, PLAYER_TEX_H };
		pivot = { PLAYER_PIVOT_X, PLAYER_PIVOT_Y };
		color = { 1.0f,1.0f,1.0f,1.0f };
		size = { PLAYER_SIZE_X, PLAYER_SIZE_Y };
	    attackbox = { 64.0f };
        //respawnPos = { 0, 0 };
        onGround = false;
        //graviTimer = 0;

        reverseLimit = 2;

        fallTimer = 0;
        reverseState = 0;
        airStopTimer = 0;
        slopeTimerMAX = 15;
        reverseAnimeFlg = false;
        airStopStay = 120;


        is_start_inversion_mask_anime = false;

		++state;

    case 2:
        //////// 通常時 ///////
       


        // 当たり判定(Y)
        hitCheckY();
        // 当たり判定(X)
        hitCheckX();

        // エリアチェック
       areaCheck();
       //プレイヤーのアニメーション
       if (objAct != DEATH)
       {
           reverse();
       }

       if (reverseState != 1)
       {
           act();
       }


       // 空ノードの削除(アニメオブジェクト)
       {
           auto iter = wind_obj_list.begin();
           while (iter != wind_obj_list.end())
           {
               if (!(*iter)->get_anime_object_manager().anime_movement()) { ++iter; }
               else {
                   safe_delete(*iter);
                   iter = wind_obj_list.erase(iter);
               }
           }
       }
      


        debug::setString("posX%f", position.x);

        //debug::setString("onGround%d", onGround);
        //debug::setString("hantenFlg%d", hantenFlg);
        //debug::setString("reverseLimit%d", reverseLimit);
        //debug::setString("airStopTimer:%d", airStopTimer);
        //debug::setString("reverseAnimeFlg:%d", reverseAnimeFlg);
        //debug::setString("reverseState:%d", reverseState);
        //debug::setString("coolTimer:%d", coolTimer);
        //debug::setString("pivot:%f", pivot.x);

        break;
    }
    timer++;
    debug::setString("posX:%f", position.x);
    debug::setString("posY:%f", position.y);

#ifdef USE_IMGUI

    if (showPlayerWindow)
    {
        /* static ImDrawList lis = ImDrawList();*/

        static bool flg = false;

        {
            ImGui::Begin("sceneGame");
            if (ImGui::CollapsingHeader("player"))
            {
                if (ImGui::TreeNode("playerPalam"))
                {
                    ImGui::SliderFloat("position.x", &position.x, 0, 1280.0f);
                    ImGui::SliderFloat("position.y", &position.y, 0, 720.0f);
                    ImGui::SliderFloat("speed.x", &speed.x, 0, 50.0f);
                    ImGui::SliderFloat("speed.y", &speed.y, 0, 50.0f);
                    ImGui::SliderInt(u8"坂道後の拘束時間", &slopeTimerMAX, 0, 30);

                    ImGui::TreePop();

                }
                if (ImGui::TreeNode("moveY"))
                {
                    ImGui::SliderFloat("graviTimer", &graviTimer, 0, 100.0f);
                    if (ImGui::Button("falltimerReset"))
                    {
                        fallTimer = 0;
                    }
                    ImGui::TreePop();
                }

                if (ImGui::TreeNode("reverse"))
                {
                    ImGui::Checkbox("reverseAnimeFlg", &reverseAnimeFlg);
                    ImGui::SliderInt("coolTimer", &coolTimer, 0, 100);
                    ImGui::SliderInt("airStopTimer", &airStopTimer, 0, 100);
                    ImGui::SliderInt("airStopStay", &airStopStay, 0, 100);
                    if (ImGui::Button("reverseStateReset"))
                    {
                        reverseState = 0;
                        coolTimer = 0;
                        reverseAnimeFlg = false;
                        airStopTimer = 0;
                    }
                    if (ImGui::Button("fallSpeed"))
                    {
                        fallSpeed = 0;
                    }
                    ImGui::TreePop();
                }
            }

            ImGui::End();
        }


        // プレビューテスト
        {
            ImGui::Begin("player preview");
            ImGui::draw_preview(PLAYER, { scale.x * 2, scale.y * 2 }, texPos, texSize, color, false);
            ImGui::End();
        }

    }
#endif
}

//--------------------------------------------------------------
//  描画処理
//--------------------------------------------------------------
void Player::render()
{
    debug::setString("fallspeed:%f", fallSpeed);
    texture::begin(PLAYER);
    texture::draw(PLAYER, position , scale, texPos, texSize, pivot, angle, color);
    texture::end(PLAYER);

    //　アニメオブジェクト
    {
        int area_num = SceneGame::instance()->bgManager()->areaNum;
        if (area_num == 16 || area_num == 17 || area_num == 18 || area_num == 19) { // ステージ5の時だけ
            for (auto& iter : wind_obj_list) {
                BaseAnimeObject* aManager = &iter->get_anime_object_manager();
                aManager->anime_draw();
            }
        }
    }

    ///* プレイヤーの当たり判定の幅*/
    if (SceneGame::instance()->bgManager()->isDebugDraw)
    {

        primitive::circle(position.x, position.y + attackbox,
            1.0f,
            2.0f, 2.0f,
            angle,
            1.0f, 0, 0, 0.5f);

        primitive::circle(position.x, position.y - attackbox,
            1.0f,
            2.0f, 2.0f,
            angle,
            1.0f, 0, 0, 0.5f);

        primitive::circle(position.x + attackbox, position.y,
            1.0f,
            2.0f, 2.0f,
            angle,
            1.0f, 0, 0, 0.5f);


        primitive::circle(position.x - attackbox, position.y,
            1.0f,
            2.0f, 2.0f,
            angle,
            1.0f, 0, 0, 0.5f);

        //}
    }
}
//--------------------------------------------------------------
// エリアチェック
//--------------------------------------------------------------
void Player::areaCheck()
{
    if (position.x < 0 + size.x)
    {
        position.x = size.x;
        speed.x = 0;
    }
    if (position.x > BG::WIDTH - size.x)
    {
        position.x = BG::WIDTH - size.x;
        speed.x = 0;
    }

}

void Player::wind()
{
    //--------< 定数 >--------//
    float WIND_ACCEL  = 0.01f;
    float WIND_MAX    = 1.5f;

    if (onGround) { WIND_ACCEL = 0.01f; WIND_MAX = 0.5f; }
    else { WIND_ACCEL = 0.025f; WIND_MAX = 1.5f; }

    //--------< 変数 >--------//
    int area_num = SceneGame::instance()->bgManager()->areaNum;
    static int wind_timer   = 0;
    static float wind_power = 0;
    static int wind_case    = 0;
    static bool wind_dire   = false;   // false:右向き   true:左向き
    static bool is_generation = true;

    if (area_num == 16 || area_num == 17 || area_num == 18 || area_num == 19) { // ステージ5の時だけ
        // timerによるケースの分岐
        if (wind_timer == 0) { wind_case = 0; }
        if (wind_timer > 0 && wind_timer < 60) { wind_case = 1; }
        if (wind_timer >= 60 && wind_timer < 150) { wind_case = 2; }
        if (wind_timer >= 150 && wind_timer < 210) { wind_case = 3; }
        if (wind_timer >= 210 && wind_timer < 270) { wind_case = 4; }
        // プレイヤーのスピードに足す
        speed.x += wind_power;
        if (wind_power > WIND_MAX) { wind_power = WIND_MAX; }
        if (wind_power < -WIND_MAX) { wind_power = -WIND_MAX; }
        // オブジェクトの生成
        if (wind_timer < 80 && wind_timer / 6 % 2 == 0 && is_generation) {
            if (wind_dire) {
                wind_obj_list.emplace_back(new AnimeObjectManager(new Wind_obj1({ 1280,0 }, wind_dire)));
                wind_obj_list.emplace_back(new AnimeObjectManager(new Wind_obj2({ 1280,0 }, wind_dire)));
                wind_obj_list.emplace_back(new AnimeObjectManager(new Wind_obj3({ 1280,0 }, wind_dire)));

                is_generation = false;
            }
            else {
                wind_obj_list.emplace_back(new AnimeObjectManager(new Wind_obj1({ -50,0 }, wind_dire)));
                wind_obj_list.emplace_back(new AnimeObjectManager(new Wind_obj2({ -50,0 }, wind_dire)));
                wind_obj_list.emplace_back(new AnimeObjectManager(new Wind_obj3({ -50,0 }, wind_dire)));

                is_generation = false;
            }
        }
        else if (wind_timer / 6 % 2 == 1) {
            is_generation = true;
        }
        // 風力処理
        switch (wind_case)
        {
        case 0: // 初期設定

            ++wind_case;
            /*fallthrough*/

        case 1: // ０から最大風力になる
            if (!wind_dire) { wind_power += WIND_ACCEL; }
            else { wind_power -= WIND_ACCEL; }
            break;

        case 2: // 最大風力継続
            break;

        case 3: // 最大風力から０になる
            if (!wind_dire) {
                wind_power -= WIND_ACCEL;
                if (wind_power < 0) wind_power = 0;
            }
            else {
                wind_power += WIND_ACCEL;
                if (wind_power > 0) wind_power = 0;
            }
            break;

        case 4: // 無風状態
            // 繰り返しさせる
            if (wind_timer >= 265) {
                wind_timer = 0;
                // 風向きを逆転させる
                if (!wind_dire) { wind_dire = true; }
                else if (wind_dire) { wind_dire = false; }
            }
            break;
        }

        ++wind_timer;
    }
    else { // 初期化
        wind_timer = 0;
        wind_power = 0;
        wind_case  = 0;
        wind_dire  = false;
    }


#ifdef USE_IMGUI

    if (showPlayerWindow)
    {
        ImGui::Begin("wind_manager");
        if (ImGui::TreeNode("windowPalam"))
        {
            //--------<定数>--------//
            ImGui::InputFloat("WIND_ACCEL", &WIND_ACCEL);
            ImGui::InputFloat("WIND_MAX", &WIND_MAX);
            //--------<変数>--------//
            ImGui::InputInt("wind_timer", &wind_timer);
            ImGui::InputFloat("wind_power", &wind_power);
            ImGui::InputInt("wind_case", &wind_case);
            ImGui::InputInt("wind_dire", (int*)&wind_dire);

            ImGui::TreePop();
        }
        ImGui::End();
    }

#endif // USE_IMGUI
}


//--------------------------------------------------------------
// X軸の行動
//--------------------------------------------------------------
void Player::moveX()
{
    static int decel_timer = 0;
    switch (STATE(0) & (PAD_LEFT | PAD_RIGHT))
    {
    case PAD_LEFT:  //左だけが押されている場合
        decel_timer = 0;
        scale.x = 1.0f;
        speed.x -= PLAYER_ACCEL_X;
        break;

    case PAD_RIGHT: //右だけが押されている場合
        decel_timer = 0;
        scale.x = -1.0f;
        speed.x += PLAYER_ACCEL_X;
        break;
    default:
        if (speed.x > 0.0f && onGround)
        {
            if (suberuFlg)
            {
                if (stopWatch(PLAYER_SLIP_DECEL_START_FRAME, decel_timer)) {
                    speed.x -= PLAYER_SLIP_DECEL_X;
                }
                else
                {
                    speed.x = 3.0f;
                }
            }
            else
            {
                speed.x -= PLAYER_DECEL_X;
            }

            if (speed.x < 0.0f) speed.x = 0;
        }
        if (speed.x < 0.0f && onGround)
        {
            if (suberuFlg)
            {
                if (stopWatch(PLAYER_SLIP_DECEL_START_FRAME, decel_timer)) {
                    speed.x += PLAYER_SLIP_DECEL_X;
                }
                else
                {
                    speed.x = -3.0f;
                }
            }
            else
            {
                speed.x += PLAYER_DECEL_X;
            }

            if (speed.x > 0.0f) speed.x = 0;
        }

        break;
    }
    // 風の処理
    wind();
    if (speed.x > PLAYER_SPEED_X_MAX) speed.x = PLAYER_SPEED_X_MAX;
    if (speed.x < -PLAYER_SPEED_X_MAX) speed.x = -PLAYER_SPEED_X_MAX;

    debug::setString("decel_timer:%d", decel_timer);
}

//--------------------------------------------------------------
// Y軸の行動
//--------------------------------------------------------------
void Player::moveY()
{
    if (onGround == false)
    {
        graviTimer++;
        fallTimer++;

    }
    speed.y = GRAVITY * graviTimer;
    // speed.y = (std::min)(speed.y, GRAVITY_MAX);
    debug::setString("graviTimer%f", graviTimer);
    debug::setString("gravity%f", GRAVITY);
    debug::setString("fallSpeed%f", fallSpeed);
    debug::setString("onGround%d", onGround);
    debug::setString("slopeFlg%d", slopeFlg);


    if (onGround)
    {
        graviTimer = 0;
        speed.y = 0.3f;
    }
    if (speed.y > PLAYER_SPEED_Y_MAX) speed.y = PLAYER_SPEED_Y_MAX;
    if (speed.y < -PLAYER_SPEED_Y_MAX) speed.y = -PLAYER_SPEED_Y_MAX;
}

//--------------------------------------------------------------
// 反転能力
//--------------------------------------------------------------
void Player::reverse()
{
    if (TRG(0) & PAD_TRG1)
    {
        if (reverseLimit > 0)
        {
            reverseAnimeFlg = true;
            slopeFlg = false;
        }
    }
    else {
        is_start_inversion_mask_anime = false;
    }

    if (reverseAnimeFlg)
    {
        switch (reverseState)
        {
        case 0://アニメ初期化
            is_start_inversion_mask_anime = true;
            reverseState++;
            break;

        case 1:     //空中静止
            airStopTimer++;

            speed = { 0.0f, 0.0f };
            fallSpeed = 0.0f;
            graviTimer = 0;

            if (airStopTimer == airStopStay / 3)
            {
                if (position.y < SCREEN_H / 2)
                {
                    position.y += abs((position.y) - SCREEN_H / 2) * 2 + size.y;
                }
                else if (position.y > SCREEN_H / 2)
                {
                    position.y -= abs((position.y) - SCREEN_H / 2) * 2 - size.y;
                }

                hantenFlg = true;
            }

            if (airStopTimer > airStopStay)
            {
                reverseLimit--;
                reverseState++;
            }

            break;

        case 2:

            if (stopWatch(20, coolTimer))
            {
                reverseState = 0;
                coolTimer = 0;
                reverseAnimeFlg = false;
                airStopTimer = 0;

            }
            break;
        }
    }

    if (onGround)
    {
        reverseLimit = 2;
    }
}

//--------------------------------------------------------------
// 状態遷移を行う
//--------------------------------------------------------------
void Player::act()
{
    bool term1;
    bool term2;
    //----------------------------------------------
    //常にしていてほしい動き
    //----------------------------------------------
    switch (objAct)
    {
        //--------------------------------
        // 待機
        //--------------------------------
    case PLAYER_ACT::IDLE_INIT:
        animeInit(0);


        ++objAct;
    case PLAYER_ACT::IDLE:
        //アニメーション
        anime(8, 16, true);

        /*行動の遷移*/
        moveY();
        moveX();

        if (fallSpeed >= 18.0f && onGround)
        {
            objAct = PLAYER_ACT::DEATH_INIT;
            break;
        }

        // 歩き
        term1 = STATE(0) & (PAD_LEFT | PAD_RIGHT);
        if (abs(speed.x) >= PLAYER_WALK_BASE && term1)
        {
            objAct = PLAYER_ACT::WALK_INIT;
            break;
        }
        // 落下
        if (abs(speed.y) > PLAYER_FALL_BASE)
        {
            objAct = PLAYER_ACT::FALL_KEEP_INIT;
            break;
        }
        break;

    case PLAYER_ACT::WALK_INIT:
        animeInit(1);

        movetimer.x = -4.0f;
        ++objAct;
    case PLAYER_ACT::WALK:

        moveY();
        moveX();

        if (fallSpeed >= 18.0f && onGround)
        {
            objAct = PLAYER_ACT::DEATH_INIT;
            break;
        }

        //アニメーション
        anime(3, 12, true);
        {
            const float speedX = fabsf(speed.x);
            term2 = !(STATE(0) & (PAD_LEFT | PAD_RIGHT));
            if (speedX < PLAYER_WALK_BASE || term2)     // アイドル状態へ
            {
                objAct = PLAYER_ACT::IDLE_INIT;
                break;
            }
            // 落下
            if (abs(speed.y) > PLAYER_FALL_BASE)
            {
                objAct = PLAYER_ACT::FALL_START_INIT;
                break;
            }
        }
        break;

        //--------------------------------
        // 落下開始
        //--------------------------------
    case PLAYER_ACT::FALL_START_INIT:
        animeInit(2);

        ++objAct;

    case PLAYER_ACT::FALL_START:
        /*-------- この行動での動作 --------*/
        if (slopeFlg == false)
        {
            moveX();
        }
            moveY();

        /*-------     アニメ処理     -------*/
        if (anime(3, 3, false))
        {
            /*--------    行動の遷移    --------*/
            objAct = PLAYER_ACT::FALL_KEEP_INIT;
            break;
        }

        if (fallSpeed >= 18.0f && onGround)
        {
            objAct = PLAYER_ACT::DEATH_INIT;
            break;
        }
        else if (onGround)
        {
            objAct = PLAYER_ACT::FALL_END_INIT;
            break;
        }
        break;

    //--------------------------------
    // 落下中
    //--------------------------------
    case PLAYER_ACT::FALL_KEEP_INIT:
        animeInit(3);

        ++objAct;

    case PLAYER_ACT::FALL_KEEP:
        /*-------- この行動での動作 --------*/
        if (slopeFlg == false)
        {
            moveX();
        }
            moveY();


        /*-------     アニメ処理     -------*/
        anime(3, 3, true);

        /*--------    行動の遷移    --------*/
        if (fallSpeed >= 18.0f && onGround)
        {
            objAct = PLAYER_ACT::DEATH_INIT;
            break;
        }
        else if (onGround)
        {
            objAct = PLAYER_ACT::FALL_END_INIT;
            break;
        }
        break;

      //--------------------------------
      // ジャンプ着地
      //--------------------------------
    case PLAYER_ACT::FALL_END_INIT:
        animeInit(4);

        ++objAct;
        /*fallthrough*/

    case PLAYER_ACT::FALL_END:
        /*-------- この行動での動作 --------*/

        moveX();

        moveY();

        /*-------     アニメ処理     -------*/
        if (anime(3, 3, false))
        {
            objAct = PLAYER_ACT::IDLE_INIT;
            break;
        }

        if (fallSpeed >= 18.0f && onGround)
        {
            objAct = PLAYER_ACT::DEATH_INIT;
            break;
        }

        /*--------    行動の遷移    --------*/
        if (TRG(0) & PAD_TRG1)
        {
            objAct = PLAYER_ACT::FALL_START_INIT;
            break;
        }

        switch (STATE(0) & (PAD_LEFT | PAD_RIGHT))
        {
        case PAD_LEFT:
            objAct = PLAYER_ACT::IDLE_INIT;
            break;
        case PAD_RIGHT:
            objAct = PLAYER_ACT::IDLE_INIT;
            break;
        }


        break;

    case DEATH_INIT:
        animeInit(5);

        ++objAct;

    case DEATH:
        if (anime(2, 1, false))
        {
            deathFlg = true;
        }

        break;
    }

}

//--------------------------------------------------------------
// プレイヤー同士と敵とマップチップの当たり判定(X)
//--------------------------------------------------------------
void Player::hitCheckX()
{
    //水の抵抗
    speed.x += SceneGame::instance()->bgManager()->calcResistanceX(&position, &size, speed.x );
    // 位置更新
    old.x = position.x;  // 移動前の位置を保持
    // 位置に速度を足す
    position.x += speed.x;
    float deltaX = position.x - old.x;  // 移動後の位置から移動前の位置を引く


    //地形当たり判定
    if (deltaX > 0)
    {
        if (SceneGame::instance()->bgManager()->isWall(position.x + size.x, position.y, size.y))
        {
            SceneGame::instance()->bgManager()->mapHoseiRight(&position, &size, &speed); //右方向の補正処理

        }
    }
    //地形当たり判定
    if (deltaX < 0)
    {
        if (SceneGame::instance()->bgManager()->isWall(position.x - size.x, position.y, size.y) )
        {
            SceneGame::instance()->bgManager()->mapHoseiLeft(&position, &size, &speed);  //左方向の補正処理

        }
    }

}
/*--------------------------------------------------------------*/
// プレイヤー同士とと敵とマップチップの当たり判定(Y)
//--------------------------------------------------------------
void Player::hitCheckY()
{
    speed.y += SceneGame::instance()->bgManager()->calcResistance(&position, &size, speed.y * 2);

    // 位置更新
    old.y = position.y;  // 移動前の位置を保持
    // 位置に速度を足す
    position.y += speed.y;
    float deltaY = position.y - old.y; // 移動後の位置から移動前の位置を引く
    //
    onGround = false;   // 地面フラグ
    slopeTimer++;

    if (onGround == false)
        fallSpeed = speed.y;

    if (slopeTimer > slopeTimerMAX)
    {
        slopeFlg = false;
    }

    //滑る床
    if (SceneGame::instance()->bgManager()->isSuberuFloor(position.x, position.y, size.x))
    {
        suberuFlg = true;
    }
    else
    {
        suberuFlg = false;
    }


    ////地形当たり判定
    if (deltaY > 0)
    {
        if(SceneGame::instance()->bgManager()->isFloor(position.x, position.y, size.x))
        {
            // 床にあたっていたら
            SceneGame::instance()->bgManager()->mapHoseiDown(&position, &speed);    //  下方向の補正処理


            onGround = true;   // 地面フラグ

        }

        if (SceneGame::instance()->bgManager()->isSlipfloor(position.x, position.y, size.x))
        {
            // 床が滑り床だったら
            if (SceneGame::instance()->bgManager()->SlipFloorisRight)
            {
                SceneGame::instance()->bgManager()->slipRight(&position, &speed);    //  右に滑る
            }
            else
            {
                SceneGame::instance()->bgManager()->slipLeft(&position, &speed);    // 左に滑る

            }
            fallSpeed = 0;
            slopeTimer = 0;
            slopeFlg = true;

            if (speed.x > PLAYER_SPEED_X_MAX) speed.x = PLAYER_SPEED_X_MAX;
            if (speed.x < -PLAYER_SPEED_X_MAX) speed.x = -PLAYER_SPEED_X_MAX;
        }
        else
        {

          if(onGround)
          {

              slopeFlg = false;
          }
        }


    }

    //地形当たり判定
    if (deltaY <= 0)
    {
        if (SceneGame::instance()->bgManager()->isCeiling(position.x, position.y - size.y, size.x))
        {
            SceneGame::instance()->bgManager()->mapHoseiUp(&position, &size, &speed);
        }
    }



    debug::setString("fallSpeed:%f", fallSpeed);
}

//--------------------------------------------------------------
// デストラクタ
//--------------------------------------------------------------
Player::~Player() {}
