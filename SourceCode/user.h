#pragma once

//******************************************************************************
//
//
//      ユーザー（ユーティリティー）
//
//
//******************************************************************************

//------< インクルード >----------------------------------------------------------
#include <sstream>
#include <bitset>
#include <cassert>
#include <math.h>
//------< inline function >-----------------------------------------------------

//--------------------------------------------------------------
//  strBit16    整数を2進数（16bit）のstringに変換する
//--------------------------------------------------------------
//  　引数：const int n     変換する整数
//  戻り値：std::string     数値を2進数に変換したもの(string)
//--------------------------------------------------------------
inline std::string strBit16(const int n)
{
    std::stringstream ss;
    ss << static_cast<std::bitset<16>>(n);
    return ss.str();
}

//--------------------------------------------------------------
//  値を範囲内に収める関数
//--------------------------------------------------------------
//    引数：const float& v  入力する数値
//        ：const float& lo 最小値
//        ：const float& hi 最大値
//  戻り値：const float&    範囲内に収まった数値
//--------------------------------------------------------------
inline const float& clamp(const float& v, const float& lo, const float& hi)
{
    assert(hi >= lo);
    return (std::max)((std::min)(v, hi), lo);
}

//--------------------------------------------------------------
//  値をラップアラウンド（範囲を繰り返す）させる
//--------------------------------------------------------------
//  const int v  範囲を繰り返させたい値
//  const int lo 下限値
//  const int hi 上限値（loより大きい値でなければならない）
//--------------------------------------------------------------
//  戻り値：int    vをloからhiまでの範囲でラップアラウンドさせた数値
//--------------------------------------------------------------
inline int wrap(const int v, const int lo, const int hi)
{
    assert(hi > lo);
    const int n = (v - lo) % (hi - lo); // 負数の剰余はc++11から使用可になった
    return n >= 0 ? (n + lo) : (n + hi);
}

// float版
inline float wrap(const float v, const float lo, const float hi)
{
    assert(hi > lo);
    const float n = std::fmod(v - lo, hi - lo);
    return n >= 0 ? (n + lo) : (n + hi);
}


//--------------------------------------------------------------
//  円同士の当たり判定
//--------------------------------------------------------------
inline bool hitCheckCircle(VECTOR2 pos1, float r1, VECTOR2 pos2, float r2)
{
	float x = pos1.x - pos2.x;
	float y = pos1.y - pos2.y;
	float r = r1 + r2;

	return (x * x) + (y * y) <= (r * r);
}

//--------------------------------------------------------------
//  矩形の当たり判定
//--------------------------------------------------------------
inline bool  hitCheckRect(VECTOR2 center1, VECTOR2 center2, VECTOR2 hitbox1, VECTOR2 hitbox2)
{
	VECTOR2 dis = center1 - center2;			         //中心の差を求める

	VECTOR2 centerdif = { hitbox1.x + hitbox2.x, hitbox1.y + hitbox2.y };

	////まずは当たり判定を行う
	if (abs(dis.x) <= centerdif.x && abs(dis.y) <= centerdif.y)
	{
		//debug::setString("HIT");
		return true;
	}
	return false;
	/*-------- デバック --------*/
	//debug::setString("disX:%f  disY:%f", dis.x, dis.y);
}


//--------------------------------------------------------------
//  任意のフレーム後にtrueを返す
//--------------------------------------------------------------
inline bool stopWatch(const int frame, int& timer)
{
    assert(frame > 0);
    assert(frame >= timer);

    if (timer < frame) ++timer;
    return timer >= frame;
}

//--------------------------------------------------------------
//  値をラップアラウンド（範囲を繰り返す）させる
//--------------------------------------------------------------
//  float (int) matri  比較される値
//  float (int) low 比較する下限値
//  float (int) high 比較される上限値
//--------------------------------------------------------------
//  戻り値：bool    範囲内に収まっていればtrue 範囲外ならfalse
//--------------------------------------------------------------
inline bool MoreAndLess(float matri, float low, float high)
{
	if (matri >= low && matri <= high)
	{
		return true;
	}
	return false;
}

inline bool MoreAndLess(int matri, int low, int high)
{
	if (matri >= low && matri <= high)
	{
		return true;
	}
	return false;
}

#ifdef USE_IMGUI
//---------------------------------------------------//
// 　　　　　　　自作 imgui 関数						 //
//---------------------------------------------------//
namespace ImGui {

	//----------------------------------------------------------------------------------
	//  ImGuiウィンドウ内で画像の表示
	//-----------------------------------------------------------------------------------
	//  int   texNo                テクスチャのインデックス
	//  const VECTOR2& scale       スケール
	//  const VECTOR2& texPos      切抜位置
	//  const VECTOR2& texSize     切抜サイズ
	//  const VECTOR4& color       色
	//  bool  is_flexible          imguiのウィンドウの大きさに合わせて画像も大きさを変えるか
	//                             true:変える     false:変えない
	//----------------------------------------------------------------------------------
	inline void draw_preview(int tex_no, const VECTOR2& scale = { 1.0f,1.0f },
		const VECTOR2& tex_pos = { 0.0f,0.0f }, const VECTOR2& tex_size = { 0.0f,0.0f },
		const VECTOR4& color = { 1.0f,1.0f,1.0f,1.0f }, bool is_flexible = false)
	{
		// ID3D11ShaderResourceView / D3D11_TEXTURE2D_DESC の取得
		SpriteBatch* sprite_batch = texture::get_texture(tex_no)->get_sprBat();
		ID3D11ShaderResourceView* SRV = sprite_batch->get_shaderResourceView();
		D3D11_TEXTURE2D_DESC tex_desk = sprite_batch->get_tex2dDesc();
		// 描画する際に切り取る四点の計算
		float u0, v0, u1, v1;
		if (scale.x >= 0) { // x軸
			u0 = { tex_pos.x / tex_desk.Width };
			u1 = { (tex_pos.x + tex_size.x) / tex_desk.Width };
		}
		else {
			u0 = { (tex_pos.x + tex_size.x) / tex_desk.Width };
			u1 = { tex_pos.x / tex_desk.Width };
		}
		if (scale.y >= 0) { // y軸
			v0 = { tex_pos.y / tex_desk.Height };
			v1 = { (tex_pos.y + tex_size.y) / tex_desk.Height };
		}
		else {
			v0 = { (tex_pos.y + tex_size.y) / tex_desk.Height };
			v1 = { tex_pos.y / tex_desk.Height };
		}
		// imguiのウィンドウの大きさに合わせて画像も大きさを変える為の基準計算
		const float base_screen_w = 300.0f;  // x,yともにimguiのウィンドウが300の時に100%になる
		const float base_screen_h = 300.0f;
		float im_screen_per_w = ImGui::GetWindowSize().x / base_screen_w;
		float im_screen_per_h = ImGui::GetWindowSize().y / base_screen_h;
		ImVec2 size;
		switch (is_flexible)
		{
		case true: // 可変
			size = { im_screen_per_w * (tex_size.x * fabsf(scale.x)),
			im_screen_per_h * (tex_size.y * fabsf(scale.y)) };  break;
		case false: // 不変
			size = { tex_size.x * fabsf(scale.x), tex_size.y * fabsf(scale.y) };  break;
		}
		ImVec4 clr = { color.x,color.y,color.z,color.w };
		// 画像の描画
		ImGui::Image((void*)SRV, size, ImVec2(u0, v0), ImVec2(u1, v1), clr);
	}





}

inline void HelpMarker(const char* desc)
{
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted(desc);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}


#endif