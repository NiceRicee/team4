#pragma once
#include "checkPoint.h"
#include "anime_object_manager.h"
//******************************************************************************
//
//
//      Player
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------

// 前方宣言

//==============================================================================
//
//      Playerクラス
//
//==============================================================================
class Player :public OBJ2D
{
private:
    //------< 定数 >----------------------------------------------------------------
    static constexpr float PLAYER_TEX_W = 64.0f;   // プレイヤーの画像1つの幅
    static constexpr float PLAYER_TEX_H = 64.0f;   // プレイヤーの画像1つの高さ
    static constexpr float PLAYER_PIVOT_X = 32.0f;    // プレイヤーの横方向の中心
    static constexpr float PLAYER_PIVOT_Y = 64.0f;   // プレイヤーの縦方向の下端

    static constexpr float PLAYER_SIZE_X = 14.0f;   // プレイヤーのsize
    static constexpr float PLAYER_SIZE_Y = 60.0f;   // プレイヤーのsize
    // プレイヤーの加速・減速の定数
    static constexpr float PLAYER_ACCEL_X = 3.5f; // プレイヤーの横方向の加速度（加速用）
    static constexpr float PLAYER_DECEL_X = 0.8f; // プレイヤーの横方向の加速度（減速用)
    static constexpr float PLAYER_SLIP_DECEL_X = 0.34f; // プレイヤーの横方向の加速度（減速用)
    static const int PLAYER_SLIP_DECEL_START_FRAME = 15; // プレイヤーの横方向の減速が始まるまでのフレーム
    static constexpr float PLAYER_ACCEL_Y = 0.4f; // プレイヤーの縦方向の加速度（加速用）
    static constexpr float PLAYER_SPEED_X_MAX = 5.5f;       // プレイヤーの横方向の速度の最大値
    static constexpr float PLAYER_SPEED_Y_MAX = 18.5f;      // プレイヤーの横方向の速度の最大値
    static constexpr float PLAYER_JUMP_POWER = -9.0f;   // プレイヤーのジャンプ力
    //  プレイヤーの移動限界値
    static constexpr float PLAYER_SCREEN_W_MAX = 1248.0f; // プレイヤーのX座標最大値
    static constexpr float PLAYER_SCREEN_W_MIN = 32.0f; // プレイヤーの  X座標最小値
    static constexpr float PLAYER_SCREEN_H_MAX = 672.0f; // プレイヤーの Y座標最大値
    static constexpr float PLAYER_SCREEN_H_MIN = 32.0f; // プレイヤーの  Y座標最小値

     //プレイヤーのアニメーション切り替え用定数
    static constexpr float PLAYER_FALL_BASE = 0.5f;
    static constexpr float PLAYER_WALK_BASE = 0.125f;
    static constexpr float PLAYER_RUN_BASE = 7.5f;

    //重力の定数
    static constexpr float GRAVITY = 0.74f;
    static constexpr float GRAVITY_MAX = 10.0f;

private:
    void pClear();          // メンバ変数の初期化
    void hitCheckY();       // 当たり判定Y
    void hitCheckX();       // 当たり判定X

    void moveX();
    void moveY();
    void reverse();              //マップ反転
    void act();             // 状態遷移を行う
    void areaCheck();       // エリアチェック
    void wind();


    //void damage();          //プレイヤーのダメージ

    //------< 変数 >----------------------------------------------------------------
    VECTOR2     leftLever;          // コントローラー左スティック用変数
    bool  slopeFlg;     //坂道にいるかのフラグ


    bool is_start_inversion_mask_anime;

    VECTOR2     old;
    float       attackbox;                     //自分の位置からどの場所で攻撃判定を行うか

    int         reverseState;
    bool        reverseAnimeFlg;

    std::list<AnimeObjectManager*> wind_obj_list;

      //------< public変数 >----------------------------------------------------------------
public:
    //VECTOR2     respawnPos;              //各ステージ開始時にスタート地点の初期化が必要
    //int         respawnArea;
    //CheckPoint  operateFile;             //checkupoint通過時のファイル操作
    float graviTimer;
    int slopeTimer;               //坂道に一定時間触れていなかったらslopeflgをfalseにするようの変数；
    int slopeTimerMAX; //坂道後の拘束時間（フレーム）
    int       coolTimer;               //クールタイム
    int         fallTimer;
    int         airStopTimer;
    int         airStopStay;
    int         reverseLimit;            //反転の制限
    bool onGround;
    float fallSpeed;
    bool suberuFlg;
    bool deathFlg;
    //imgui系
    bool showPlayerWindow = false;

    //------< 行動 >--------------------------------------------------------------
public:

    Player();
    void init();
    void deinit();
    void update();
    void render();
    ~Player();

    VECTOR2 getPos() { position; }
    VECTOR2 getPosX() { position.x; }
    VECTOR2 getPosY() { position.y; }
    bool get_is_start_inversion_mask_anime() { return is_start_inversion_mask_anime; }


    enum PLAYER_ACT
    {
        IDLE_INIT,
        IDLE,
        WALK_INIT,
        WALK,
        FALL_START_INIT,
        FALL_START,
        FALL_KEEP_INIT,
        FALL_KEEP,
        FALL_END_INIT,
        FALL_END,

        RUN_INIT,
        RUN,
        DEATH_INIT,
        DEATH

    };

};