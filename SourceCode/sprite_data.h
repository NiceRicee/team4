#pragma once

//******************************************************************************
//
//
//		SPRITE_DATA
//
//
//******************************************************************************

#include "../GameLib/texture.h"

// ラベル定義
enum TEXNO
{
    MAPCHIP1,
    MAPCHIP2,

    MASK_1,
    MASK_2,
    MASK_3,
    MASK_4,
    MASK_5,
    MASK_6,
    MASK_7,
    MASK_8,
    MASK_9,
    MASK_10,
    MASK_11,
    MASK_12,
    MASK_13,
    MASK_14,
    MASK_15,
    MASK_16,
    MASK_17,
    MASK_18,
    MASK_19,
    MASK_20,

    // マップチップ
    /*stage1*/

    MAP_ST1,
    MAP_ST2,
    MAP_ST3,
    MAP_ST4,
    MAP_ST5,
    MAP_ST6,
    CHECK_POINT,

    MAP_ST1_UP,
    MAP_ST1_DOWN,
    MAP_ST2_UP,
    MAP_ST2_DOWN,
    MAP_ST3_UP,
    MAP_ST3_DOWN,
    MAP_ST4_UP,
    MAP_ST4_DOWN,
    MAP_ST2_1_UP,
    MAP_ST2_1_DOWN,
    MAP_ST2_2_UP,
    MAP_ST2_2_DOWN,
    MAP_ST2_3_UP,
    MAP_ST2_3_DOWN,
    MAP_ST2_3_SECRET,
    MAP_ST2_4_UP,
    MAP_ST2_4_DOWN,
    MAP_ST3_1_UP,
    MAP_ST3_1_DOWN,
    MAP_ST3_2_UP,
    MAP_ST3_2_DOWN,
    MAP_ST3_3_UP,
    MAP_ST3_3_DOWN,
    MAP_ST3_4_UP,
    MAP_ST3_4_DOWN,
    MAP_ST4_1_UP,
    MAP_ST4_1_DOWN,
    MAP_ST4_2_UP,
    MAP_ST4_2_DOWN,
    MAP_ST4_3_UP,
    MAP_ST4_3_DOWN,
    MAP_ST4_4_UP,
    MAP_ST4_4_DOWN,
    MAP_ST6_1_UP,
    MAP_ST6_2_UP,
    MAP_ST6_3_UP,
    MAP_ST6_4_UP,
    MAP_ST6_4_DOWN,


    //キャラクター
    /*player*/
    PLAYER,           //プレイヤー１

    SIGNBOARD,

    MINIMAP,
    MINI_CHARA,
    MIST0,
    MIST1,
    MIST2,
    MIST3,
    SPEEDMATOR,
    DANGER,
    LOGPOCE,        //方向さすやつ
    KEY,        //ポーズキー

    // フェードインフェードアウト
    RECTANGLE,
    TRANS,

    // タイトル
    TEX_TITLE_STRING_NEW,
    TEX_TITLE_STRING_CON,
    TEX_TITLE_SELECT,
    TEX_CONFIRMATION_COMMAND,
    // ポーズ説明
    PORSE_BOAD,

    // チュートリアル
    TUTORIAL,

    // 戻るボタン(ポーズ)
    BACK_MARK,

    // タイトル
    TITLE_0,
    TITLE_1,
    TITLE_2,
    TITLE_3,
    TITLE_4,
    TITLE_5,
    TITLE_6,
    TITLE_7,
    TITLE_8,
    TITLE_9,
    TITLE_10,
    TITLE_11,
    TITLE_12,
    TITLE_13,
    TITLE_14,
    TITLE_15,
    TITLE_16,
    TITLE_17,
    TITLE_18,
    TITLE_19,
    TITLE_20,
    TITLE_21,
    TITLE_22,
    TITLE_23,
    TITLE_24,
    TITLE_25,
    TITLE_26,
    TITLE_27,
    TITLE_28,
    TITLE_29,
    TITLE_30,
    TITLE_31,
    TITLE_32,
    TITLE_33,
    TITLE_34,
    TITLE_35,
    TITLE_36,
    TITLE_37,
    TITLE_38,
    TITLE_39,
    TITLE_40,
    TITLE_41,
    TITLE_42,
    TITLE_43,
    TITLE_44,
    TITLE_45,
    TITLE_46,
    TITLE_47,
    TITLE_48,
    TITLE_49,
    TITLE_50,
    TITLE_51,
    TITLE_52,
    TITLE_53,
    TITLE_54,
    TITLE_55,
    TITLE_56,
    TITLE_57,
    TITLE_58,
    TITLE_59,
    TITLE_60,
    TITLE_61,
    TITLE_62,
    TITLE_63,
    TITLE_64,
    TITLE_65,
    TITLE_66,
    TITLE_67,
    TITLE_68,
    TITLE_69,
    TITLE_70,
    TITLE_71,
    TITLE_72,
    TITLE_73,
    TITLE_74,
    TITLE_75,
    TITLE_76,
    TITLE_77,
    TITLE_78,
    TITLE_79,
    TITLE_80,
    TITLE_81,
    TITLE_82,
    TITLE_83,

    // fall_light
    FALL_LIGHT_1,
    FALL_LIGHT_2,
    FALL_LIGHT_3,
    FALL_LIGHT_4,
    FALL_LIGHT_5,
    FALL_LIGHT_6,
    FALL_LIGHT_7,
    FALL_LIGHT_8,
    FALL_LIGHT_9,
    FALL_LIGHT_10,
    FALL_LIGHT_11,
    FALL_LIGHT_12,
    FALL_LIGHT_13,
    FALL_LIGHT_14,
    FALL_LIGHT_15,
    FALL_LIGHT_16,
    FALL_LIGHT_17,
    FALL_LIGHT_18,
    FALL_LIGHT_19,
    FALL_LIGHT_20,
    FALL_LIGHT_21,
    FALL_LIGHT_22,
    FALL_LIGHT_23,
    FALL_LIGHT_24,
    FALL_LIGHT_25,

    // マルチスレッドで使う画像
    THREAD_LOADING,

    // クリア画面
    TEX_CLEAR,

    // 風
    TEX_WIND_OBJ,
};


extern GameLib::LoadTexture loadTexture[];
extern GameLib::LoadTexture loadTexture_fade[];
extern GameLib::LoadTexture loadTexture_title[];
extern GameLib::LoadTexture loadTexture_pause[];
extern GameLib::LoadTexture loadTexture_mask[];


//******************************************************************************

