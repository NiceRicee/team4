//******************************************************************************
//
//
//		TITLE_CPP
//
//
//******************************************************************************

#include "all.h"
#include "audio.h"
#include "scene_pause.h"



void SceneClear::init()
{
	music::play(BGM_END, true);

	state = 0;
	timer = 0;
	do_start_brightspot = true;
	trans_colorW        = 1.0f;
}

void SceneClear::deinit()
{
	music::stop(BGM_END);
}

//***************************
//    更新処理
//***************************
void SceneClear::update()
{
	// ポーズ画面

	switch (state)
	{
	case 0:
		//////// 初期設定 ////////

		++state;
		/*fallthrough*/

	case 1:
		/////// パラメーターの設定 ////////
		GameLib::setBlendMode(Blender::BS_ALPHA);



		++state;
		/*fallthrough*/

	case 2:
		//////// 通常時 ////////
		// 前半の動き
		// 次シーン移行
		if (!do_start_brightspot && timer > 600 && FadeRectangle3::getInstance()->fadeFlg == false) {
			FadeRectangle3::getInstance()->fadeFlg = true; FadeRectangle3::getInstance()->fadeState = 0;
		}
		FadeRectangle3::getInstance()->firstHalfMove(SCENE_TITLE);
		FadeRectangle3::getInstance()->secondHalfMove(SCENE_NONE);


		if (do_start_brightspot) {
			trans_colorW -= 0.01f;
			if (trans_colorW <= 0.0f) { trans_colorW = 0.0f; do_start_brightspot = false; }
		}

		break;
	}

#ifdef USE_IMGUI

#endif

	++timer;
}

//***************************
//    描画処理
//***************************
void SceneClear::draw()
{
	GameLib::clear(0.0f, 0.0f, 0.0f);

	// タイトル
	texture::begin(TEX_CLEAR);
	texture::draw(TEX_CLEAR, { 0.0f + 85.0f, 0.0f }, { 1, 1 }, { 0.0f, 0.0f }, { 1280.0f, 720.0f}, { 0, 0 }, 0, { 1, 1, 1, 1 });
	texture::end(TEX_CLEAR);

	// フェードインフェードアウト
	if (FadeRectangle3::getInstance()->fadeFlg)
	{
		texture::begin(RECTANGLE);
		for (int i = 0; i < 11; ++i)
		{
			for (int o = 0; o < 23; ++o)
			{
				FadeRectangle3* fade = FadeRectangle3::getInstance();
				texture::draw(RECTANGLE, { 32.0f + 64.0f * o , 33.0f + 66.0f * i }, { 1.0f, 1.0f },
					{ 0, 0 }, { 64.0f, 66.0f }, { 32.0f, 33.0f }, ToRadian(0), fade->color[o + i * 20]);
			}
		}
		texture::end(RECTANGLE);
	}

	// 蓋絵
	if (do_start_brightspot) {
		texture::begin(TRANS);
		texture::draw(TRANS, { 0,0 }, { 1,1 },
			{ 0, 0 }, { 1450.0f, 720.0f }, { 0,0 }, ToRadian(0), { 0, 0, 0, trans_colorW });
		texture::end(TRANS);
	}
}