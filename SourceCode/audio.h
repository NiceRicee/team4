#pragma once

//******************************************************************************
//
//
//      audio.h
//
//
//******************************************************************************

// 定数の定義
// BGMの種類
#define BGM_TITLE            0
#define BGM_STAGE1           1
#define BGM_STAGE2           2
#define BGM_STAGE3           3
#define BGM_STAGE4           4
#define BGM_STAGE5           5
#define BGM_STAGE6           6
#define BGM_END              7

// XWBの種類
#define XWB_SOUNDS           0

// FIRSTの効果音
#define XWB_SOUNDS_DESION    0
#define XWB_SOUNDS_LANDING   1
#define XWB_SOUNDS_CHOICE	 2



// 関数のプロトタイプ宣言
void audio_init();
void audio_deinit();
