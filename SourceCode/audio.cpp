#include "all.h"
#include "audio.h"

void audio_init()
{
    //音楽読み込み
    music::load(BGM_TITLE,  L"./Data/Musics/title.wav",  0.2f);
    music::load(BGM_STAGE1, L"./Data/Musics/stage1.wav", 0.1f);
    music::load(BGM_STAGE2, L"./Data/Musics/stage2.wav", 0.1f);
    music::load(BGM_STAGE3, L"./Data/Musics/stage3.wav", 0.1f);
    music::load(BGM_STAGE4, L"./Data/Musics/stage4.wav", 0.1f);
    music::load(BGM_STAGE5, L"./Data/Musics/stage5.wav", 0.1f);
    music::load(BGM_STAGE6, L"./Data/Musics/stage6.wav", 0.1f);
    music::load(BGM_END,    L"./Data/Musics/end.wav",    0.1f);


    // 効果音を読み込む
    sound::load(XWB_SOUNDS, L"./Data/Sounds/se.xwb");   // SE

    // 効果音の音量調整
    //first
    sound::setVolume(XWB_SOUNDS, XWB_SOUNDS_DESION,  0.5f);
    sound::setVolume(XWB_SOUNDS, XWB_SOUNDS_LANDING, 0.3f);
    sound::setVolume(XWB_SOUNDS, XWB_SOUNDS_CHOICE,  0.5f);
}

void audio_deinit()
{
    music::clear();
}