#ifndef	SCENE_CLEAR_H
#define	SCENE_CLEAR_H

class SceneClear :public Scene
{
private:
    //--------<変数>--------
    int state = 0;
    int timer = 0;

    // trans関連
    bool do_start_brightspot;
    float trans_colorW;

public:
    //------< インスタンス >---------------------------------------------------------
    static SceneClear* instance() { return &instance_; }
    //--------<コンストラクタ/関数等>-------
    void init();
    void deinit();
    void update();
    void draw();

    static SceneClear instance_;
};

#endif//SCENE_CLEAR_H