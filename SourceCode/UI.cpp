#include "all.h"
#include "user.h"

float color_w_2;
float color_w_3;
float color_w_4;


//--------------------------------------------------------------
//  コンストラクタ
//--------------------------------------------------------------
UI::UI() 
{
	state = 0;
	timer = 0;
	bg = SceneGame::instance()->bgManager();
	pl = SceneGame::instance()->playerManager();
}

//--------------------------------------------------------------
//  初期設定
//--------------------------------------------------------------
void UI::init()
{
	state = 0;
	timer = 0;
	logScale = 0;
	stayMisttimer = 0;
	mist_color_w = 0;
	mist_color_w2 = 0;

	mist1X   = 190;
	mist2X = 420;
	mist2_2X = 247;
	mist3X = 640;
	mist1XRightFlg = false;;
	mist2XRightFlg = false;;
	mist2_2XRightFlg = false;;
	mist3XRightFlg = false;;
	graduateflg = true;
}



//--------------------------------------------------------------
//  更新処理
//--------------------------------------------------------------
void UI::update()
{
	switch (state)
	{
	case 0:
		//////// 初期設定 ///////
		
		++state;
		/*fallthrough*/

	case 1:
		//////// パラメーターの設定 ////////
		minimap.texSize = { 64.0f, 256.0f };
		minimap.scale = { 1.5f, 1.5f };
		minimap.position = { 1370.0f, 230.0f };
		minichara.position = minimap.position;
		logScale = 0.25f;
		++state;

	case 2:
		//////// 通常時 ///////
		
		minimap.scale.y = 1.5f;
			//ミニキャラ位置　=　（ミニマップの位置 - (ミニマップの中心 * ミニマップのスケール) + ミニキャラの半サイズ) + (エリアナンバー * ミニマップの階層の幅)
		minichara.position.y = (minimap.position.y - (128 * minimap.scale.y) + 16.0f) + (bg->areaNum % 8 * 48.0f) ; //ミニマップ内のキャラをareaNumに応じて移動
			
		//霧の移動
		{
			
			//mist1
			 
			 switch (mist1XRightFlg)
			 {
			 case false:
				 mist1X		+= 0.4f;

				 if (mist1X > 740)
				 {
					 mist1XRightFlg = true;
				 }
				 break;
				
			 case true:
				 mist1X		-= 0.4f;
				 

				 if (mist1X < -90)
				 {
					 mist1XRightFlg = false;
				 }
				 break;
				
			 }
			 clamp(mist1X, -100,   768);
		

			 //mist2X

			 switch (mist2XRightFlg)
			 {
			 case false:
				 mist2X -= 0.7f;
				
				 if (mist2X < -100)
				 {
					 mist2XRightFlg = true;
				 }
				 break;
			 case true:
				
				 mist2X += 0.7f;
				

				 if (mist2X > 740)
				 {
					 mist2XRightFlg = false;
				 }
				 break;
			 }
			 clamp(mist2X, -108, 750);


			 //mist2_2X

			 switch (mist2_2XRightFlg)
			 {
			 case false:	
				 mist2_2X += 0.9f;

				 if (mist2_2X > 740)
				 {
					 mist2_2XRightFlg = true;
				 }
				 break;
				
			 case true:
				
				 mist2_2X -= 0.9f;
				
				

				 if (mist2_2X < -120)
				 {
					 mist2_2XRightFlg = false;
				 }
				 break;
			 }
			 clamp(mist2_2X, -130, 750);

			 //mist3X

			 switch (mist3XRightFlg)
			 {
			 case false:
				
				 mist3X -= 0.3f;
				 if (mist3X < -200)
				 {
					 mist3XRightFlg = true;
				 }
				 break;
			 case true:
				 
				 mist3X += 0.3f;

				 if (mist3X > 740)
				 {
					 mist3XRightFlg = false;
				 }
				 break;
			 }
			 clamp(mist3X, -210, 750);

		}

		switch (graduateflg)
		{
		case true:
			//濃くなっていく
			/*mist_color_w += 0.0017f;
			mist_color_w2  -= 0.005f;*/

			mist_color_w += 0.0017f;
			mist_color_w2 -= 0.005f;

			if (mist_color_w2 < 0.2f)
			{
				mist_color_w2 = 0.2f;
			}

			if (mist_color_w >= 0.95f)
			{
				mist_color_w = 0.95f;
				stayMisttimer++;
				if (stayMisttimer > 200)
				{
					graduateflg = false;
					stayMisttimer = 0;
				}
			}

			break;
		case false:
			//晴れていく
			mist_color_w -= 0.0017f;
			mist_color_w2 += 0.005f;

			if (mist_color_w2 >=0.95f)
			{
				mist_color_w2 = 0.95f;
			}

			if (mist_color_w < 0.2f)
			{
				mist_color_w = 0.2f;
				graduateflg = true;
			}
			break;
		}
	
		switch (bg->mapStatus)
		{
		case false:
			logScale = 0.25f;
			break;

		case true:
			logScale = -0.25f;
			break;
		}

		break;
	}
	timer++;

#ifdef USE_IMGUI
	if (showUIWindow)
	{
		ImGui::Begin("UI");
		
			ImGui::SliderFloat("posX", &minimap.position.x, 0.0f, 1500.0f);
			ImGui::SliderFloat("posY", &minimap.position.y, 0.0f, 720.0f);
			ImGui::SliderFloat("angle", &minimap.scale.y, -2.0f, 1.0f);
			ImGui::SliderFloat("position.y", &minichara.position.y, 0.0f, 720.0f);

		{
			ImGui::Begin("MISTstatus");
			ImGui::SliderFloat("color_w_1", &mist_color_w, 0.0f, 1.0f);
			ImGui::SliderFloat("color_w_2", &color_w_2, 0.0f, 1.0f);
			ImGui::SliderFloat("color_w_3", &color_w_3, 0.0f, 1.0f);
			ImGui::SliderFloat("color_w_4", &color_w_4, 0.0f, 1.0f);
			ImGui::End();
		}

		ImGui::End();
	}
#endif
}



//--------------------------------------------------------------
//  描画処理
//--------------------------------------------------------------
void UI::draw()
{
	
	//ミニマップ
	{
		float texPosY = 0;
		if (MoreAndLess(SceneGame::instance()->bgManager()->areaNum, 0, 7))
		{
			texPosY = 0;
		}
		else if (MoreAndLess(SceneGame::instance()->bgManager()->areaNum, 8 , 15))
		{
			texPosY = 256;
		}
		else if (MoreAndLess(SceneGame::instance()->bgManager()->areaNum, 16, 23))
		{
			texPosY = 512;
		}
		texture::begin(MINIMAP);
		texture::draw(MINIMAP, { minimap.position.x, minimap.position.y }, { 1.5f, minimap.scale.y }, { 0, texPosY }, { 64, 256 }, { 32, 128 }, ToRadian(0), { 1, 1, 1, 1 });
		texture::end(MINIMAP);

		texture::begin(MINI_CHARA);
		texture::draw(MINI_CHARA, { minichara.position.x, minichara.position.y }, { 1, 1 }, { 0, 0 }, { 32, 32 }, { 16, 16 }, 0, { 1, 1, 1, 1 });
		texture::end(MINI_CHARA);
	}
	
	{
		////スピードメーター
		//float mater = (pl->fallSpeed / 18) * 512;
		//texture::begin(SPEEDMATOR);
		//texture::draw(SPEEDMATOR, { 1315.0f, 38.0f }, { 1, 1 }, { 0, 0 }, { 64, mater }, { 32, 0 }, 0, { 1, 1, 1, 0.6f });
		//texture::end(SPEEDMATOR);

		////危険マーク
		//texture::begin(DANGER);
		//texture::draw(DANGER, { 1283.0f, 510.0f }, { 1, 1 }, { 0, 0 }, { 64, 64 }, { 0, 0 }, 0, { 2, 1, 1, 1 });
		//texture::end(DANGER);

		//方向示すやつ
		switch (pl->reverseLimit)
		{
		case 0:

			break;
		case 1:
			texture::begin(LOGPOCE);
			texture::draw(LOGPOCE, { 1335.0f,627.0f }, { 0.25f, logScale }, { 0, 0 }, { 256, 256 }, { 128, 128 }, 0, { 1, 1, 1, 1 });
			texture::end(LOGPOCE);
			break;

		case 2:
			texture::begin(LOGPOCE);
			texture::draw(LOGPOCE, { 1335.0f,627.0f }, { 0.25f, logScale }, { 0, 0 }, { 256, 256 }, { 128, 128 }, 0, { 1, 1, 1, 1 });
			texture::draw(LOGPOCE, { 1395.0f , 627.0f }, { 0.25f, logScale }, { 0, 0 }, { 256, 256 }, { 128, 128 }, 0, { 1, 1, 1, 1 });
			texture::end(LOGPOCE);
			break;
		
		}
		
	}
	// 霧

#ifdef USE_IMGUI


	static float mist1Y = 0;
	
	static float mist2Y = 0;
	
	static float mist2_2Y = 0;
	
	static float mist3Y = 0;
	if (showUIWindow)
	{
		ImGui::Begin("MISTstatus");
		ImGui::SliderFloat("mist1X", &mist1X, 0, 1280);
		ImGui::SliderFloat("mist1Y", &mist1Y, 0, 720);

		ImGui::SliderFloat("mist2X", &mist2X, 0, 1280);
		ImGui::SliderFloat("mist2Y", &mist2Y, 0, 720);
		ImGui::SliderFloat("mist2_2X", &mist2_2X, 0, 1280);
		ImGui::SliderFloat("mist2_2Y", &mist2_2Y, 0, 720);

		ImGui::SliderFloat("mist3X", &mist3X, 0, 1280);
		ImGui::SliderFloat("mist3Y", &mist3Y, 0, 720);

		ImGui::End();
	}
#endif // USE_IMGUI

	if ((MoreAndLess(SceneGame::instance()->bgManager()->areaNum, 4, 7)))
	{
		texture::begin(MIST0);
		texture::draw(MIST0, { 0, 0 }, { 1.0f, 1.0f }, { 0, 0 }, { 1280, 720 }, { 0, 0 }, 0, { 1, 1, 1, mist_color_w });
		texture::end(MIST0);

		texture::begin(MIST1);
		texture::draw(MIST1, { mist1X, 113 }, { 1.0f, 1.0f }, { 0, 0 }, { 512, 180 }, { 0, 0 }, 0, { 1, 1, 1, mist_color_w });
		texture::end(MIST1);

		texture::begin(MIST2);
		texture::draw(MIST2, { mist2X, 275 }, { 1.0f, 1.0f }, { 0, 0 }, { 640, 308 }, { 0, 0 }, 0, { 1, 1, 1, mist_color_w });
		texture::draw(MIST2, { mist2_2X, 425 }, { 1.0f, 1.0f }, { 0, 0 }, { 640, 308 }, { 0, 0 }, 0, { 1, 1, 1, mist_color_w });
		texture::end(MIST2);

		texture::begin(MIST3);
		texture::draw(MIST3, { mist3X, 529 }, { 1.0f, 1.0f }, { 0, 0 }, { 640, 308 }, { 0, 0 }, 0, { 1, 1, 1, mist_color_w });
		texture::draw(MIST3, { mist3X, 36 }, { 1.0f, 1.0f }, { 0, 0 }, { 640, 308 }, { 0, 0 }, 0, { 1, 1, 1, mist_color_w });
		texture::end(MIST3);
	}


	//キー
	{
		texture::begin(KEY);
		texture::draw(KEY, { 1300.0f, 625.0f }, { 1, 1 }, { 0, 0 }, { 128, 128 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::draw(KEY, { 1300.0f, 450.0f }, { 1, 1 }, { 0, 128 }, { 640, 360 }, { 0, 0 }, 0, { 1, 1, 1, 1 });
		texture::end(KEY);
	}

}