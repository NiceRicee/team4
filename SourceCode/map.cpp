#include"all.h"

//------< using >---------------------------------------------------------------

//----------------------------------------------------------------------
//  BGクラス
//----------------------------------------------------------------------
BG::BG() : scroll(VECTOR2(0, 0)), back(), terrain()
{
    isDebugDraw = false;
    areaNum = 0;
    texNo = TEXNO::MAPCHIP1;
    mapStatus = false;
}

//----------------------------------------------------------------------
//  デストラクタ
//----------------------------------------------------------------------
BG::~BG()
{

}

//----------------------------------------------------------------------
//  メンバ変数のクリア
//----------------------------------------------------------------------
void BG::clear()
{
    for (int y = 0; y < CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < CHIP_NUM_X; x++)
        {
            terrain[y][x] = 0;
            back[y][x] = 0;
        }
    }
    MapEditor::instance().init();

    scroll = {};                                // スクロール座標（画面左上の位置のワールド座標）
}
//----------------------------------------------------------------------
//  マップデータのロード（仮）
//----------------------------------------------------------------------
bool BG::loadMapData(const char data[][CHIP_NUM_X], char map[][CHIP_NUM_X])
{


    for (int y = 0; y < BG::CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < BG::CHIP_NUM_X; x++)
        {
            map[y][x] = data[y][x];
        }
    }

    return true;
}

//----------------------------------------------------------------------
//  更新処理
//----------------------------------------------------------------------
void BG::update()
{
    scrollMap();       // clamp
    //scrollMap0();       // clamp
#ifdef USE_IMGUI
      
   
#endif

    debug::setString("mapstate%d", mapStatus);
}

void BG::scrollMap()
{
    using namespace input;
    /*if (TRG(0) & PAD_TRG2)
    {
        isDebugDraw = !isDebugDraw;
    }*/

}


//----------------------------------------------------------------------
//  BGデータ描画（スムーズなスクロール）
//----------------------------------------------------------------------
void BG::draw(int texNo, char map[][CHIP_NUM_X])
{
    const int DIV_X = static_cast<int>(scroll.x) / CHIP_SIZE; // division x
    const int DIV_Y = static_cast<int>(scroll.y) / CHIP_SIZE; // division y
    const int REM_X = static_cast<int>(scroll.x) % CHIP_SIZE; // remainder x
    const int REM_Y = static_cast<int>(scroll.y) % CHIP_SIZE; // remainder y

    texture::begin(texNo);
    for (int y = 0; y < BG::LOOP_Y; y++)
    {
        for (int x = 0; x < BG::LOOP_X; x++)
        {
            if (DIV_X + x >= CHIP_NUM_X || DIV_Y + y >= CHIP_NUM_Y)
                continue; // 添え字の範囲チェック

            char chip = map[DIV_Y + y][DIV_X + x];
            if (-1 == chip) continue;

            texture::draw(texNo,
                static_cast<float>(x * CHIP_SIZE - REM_X),
                static_cast<float>(y * CHIP_SIZE - REM_Y),
                1, 1,
                static_cast<float>(chip % CHIP_NUM_PER_LINE * CHIP_SIZE),
                static_cast<float>(chip / CHIP_NUM_PER_LINE * CHIP_SIZE),
                static_cast<float>(CHIP_SIZE), static_cast<float>(CHIP_SIZE)
            );
        }
    }
    texture::end(texNo);
}

void BG::hanten(char terrain[CHIP_NUM_Y][CHIP_NUM_X])
{
    char copy[CHIP_NUM_Y][CHIP_NUM_X];
    int mdl = CHIP_NUM_Y / 2;
    //マップ情報をコピー
    for (int y = 0; y < CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < CHIP_NUM_X; x++)
        {
            copy[y][x] = terrain[y][x];

            //三角チップ左下がりを反転
            if (copy[y][x] == 4)
            {
                copy[y][x] = 6;
            }
            else if(copy[y][x] == 6)
            {
                copy[y][x] = 4;
            }

            //三角チップ右下がりを反転
            if (copy[y][x] == 5)
            {
                copy[y][x] = 7;
            }
            else if (copy[y][x] == 7)
            {
                copy[y][x] = 5;
            }

            //三角チップ(氷)左下がりを反転
            if (copy[y][x] == 12)
            {
                copy[y][x] = 14;
            }
            else if (copy[y][x] == 14)
            {
                copy[y][x] = 12;
            }

            //三角チップ(氷)右下がりを反転
            if (copy[y][x] == 13)
            {
                copy[y][x] = 15;
            }
            else if (copy[y][x] == 15)
            {
                copy[y][x] = 13;
            }

            //三角チップ(水)左下がりを反転
            if (copy[y][x] == 16)
            {
                copy[y][x] = 18;
            }
            else if (copy[y][x] == 18)
            {
                copy[y][x] = 16;
            }

            //三角チップ(水)右下がりを反転
            if (copy[y][x] == 17)
            {
                copy[y][x] = 19;
            }
            else if (copy[y][x] == 19)
            {
                copy[y][x] = 17;
            }
        }
    }

    //真ん中を基準に反転
    for (int y = 0; y < CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < CHIP_NUM_X; x++)
        {
            if (y < mdl)
            {
                terrain[mdl + abs(mdl - y)][x] = copy[y][x];
            }

            if (y > mdl)
            {
                terrain[mdl - abs(mdl - y)][x] = copy[y][x];
            }
        }
    }

}

//******************************************************************************
//
//      あたり判定
//
//******************************************************************************
int BG::getData(char map[][CHIP_NUM_X], const VECTOR2& pos)
{
    //return map[static_cast<int>(y) >> 6][static_cast<int>(x) >> 6];

    const int divY = static_cast<int>(pos.y) / CHIP_SIZE;
    const int divX = static_cast<int>(pos.x) / CHIP_SIZE;

    if (divX < 0 || divX >= CHIP_NUM_X ||
        divY < 0 || divY >= CHIP_NUM_Y)
        return -1;

    return map[divY][divX];
}



bool BG::isHitAll(float x, float y)
{
    switch (getTerrainAttr(VECTOR2(x, y)))
    {
    case  TR_ATTR::ALL_BLOCK:  return false;
    case  TR_ATTR::WALL_BLOCK:  return true;
    case  TR_ATTR::KOORI:  return true;
    }
    return false;
}



bool BG::isFloor(float x, float y, float width)
{

    for (; width > 0; width -= CHIP_SIZE)
    {

        if (x - width < 0)          continue;
        if (x + width > BG::WIDTH)  continue;

        if (isHitDown(x - width, y)) return true;
        if (isHitDown(x + width, y)) return true;
    }
    return isHitDown(x, y);
}

//----------------------------------------------------------------------
//  横の壁にあたっているか
//----------------------------------------------------------------------
bool BG::isWall(float x, float y, float height)
{
    for (; height > 0; height -= CHIP_SIZE)
    {
        const float yPos = y - height;
        if (yPos < 0)      continue;
        if (yPos > HEIGHT) continue;

        if (isHitAll(x, yPos))
            return true;
    }

    return isHitAll(x, y);
}


bool BG::isCeiling(float x, float y, float width)
{
    for (; width > 0; width -= CHIP_SIZE)       // widthをCHIP_SIZE分減らしていく
    {
        const float left = x - width;
        const float right = x + width;

        // エリアチェック
        if (left < 0)       continue;
        if (right >= WIDTH) continue;

        if (isHitAll(left, y))  return true;   // 左端から
        if (isHitAll(right, y)) return true;   // 右端から
    }
    return isHitAll(x, y);
}



void BG::mapHoseiUp(VECTOR2* position, VECTOR2* size, VECTOR2* speed)
{
    /*float y = position->y - size->y;
    y += CHIP_SIZE - fmodf(y, CHIP_SIZE);
    position->y = y + size->y;
    speed->y = (std::max)(speed->y, 0.0f);*/

    float y = position->y - size->y;
    int intY = static_cast<int>(y);
    y += static_cast<float>(CHIP_SIZE - (intY % CHIP_SIZE));
    position->y = y + size->y;
    speed->y = (std::max)(speed->y, 0.0f);      // 天井にあたったので速度が止まる
}

void BG::mapHoseiDown(VECTOR2* position, VECTOR2* speed)
{
    //float y = position->y;                          // わかりやすく書くためいったんyに代入
    //int intY = static_cast<int>(y);
    //y = static_cast<float>(intY - intY % CHIP_SIZE);
    //position->y = y - ADJUST_Y;                     // 少し浮かせる
    //speed->y = (std::min)(speed->y, 0.0f);      // 地面にあたったので速度が止まる

    float y = position->y;
    position->y = y - fmodf(y, float(CHIP_SIZE)) - ADJUST_Y;
    speed->y = (std::min)(speed->y, 0.0f);
    //debug::setString("obj->speed.y:%f", obj->speed.y);
}




bool BG::isHitDown(float x, float y)
{
    switch (getTerrainAttr(VECTOR2(x, y)))                  // 地形の属性を取得する
    {
    case TR_ATTR::WALL_BLOCK:    return true;                // 全て壁の地形であった
    case TR_ATTR::KOORI:    return true;                // 全て壁の地形であった
    
        break;
    }
    return false;   // 地形ではなかった場合
}




void BG::mapHoseiRight(VECTOR2* position, VECTOR2* size, VECTOR2* speed)
{
    float x = position->x + size->x;
    x -= fmodf(x, static_cast<float>(CHIP_SIZE));
    position->x = x - size->x - ADJUST_X;
    speed->x = 0.0f;
}

void BG::mapHoseiLeft(VECTOR2* position, VECTOR2* size, VECTOR2* speed)
{
    float x = position->x - size->x;
    x += static_cast<float>(CHIP_SIZE) - fmodf(x, static_cast<float>(CHIP_SIZE));
    position->x = x + size->x + ADJUST_X;
    speed->x = 0.0f;
}

void BG::slipRight(VECTOR2* position, VECTOR2* speed)
{
    speed->y += 8.0f;
    speed->x += 8.0f;
    
}

void BG::slipLeft(VECTOR2* position, VECTOR2* speed)
{
    speed->y += 8.0f;
    speed->x -= 8.0f;
}

bool BG::isSuberuFloor(float x, float y, float width)
{
    for (; width > 0; width -= CHIP_SIZE)
    {

        if (x - width < 0)          continue;
        if (x + width > BG::WIDTH)  continue;

        if (isHitSuberu(x - width, y)) return true;
        if (isHitSuberu(x + width, y)) return true;
    }
    return isHitSuberu(x, y);
}

float BG::calcResistance(VECTOR2* position, VECTOR2* size, float speed)
{
    return getTeikou(getTerrainAttr(VECTOR2(position->x, position->y - size->y * 0.5f)), speed);
}

float BG::calcResistanceX(VECTOR2* position, VECTOR2* size, float speed)
{
    return getTeikouX(getTerrainAttr(VECTOR2(position->x, position->y - size->y * 0.5f)), speed);
}

float BG::getTeikou(TR_ATTR attr, float speed)
{
  
    switch (attr)
    {
    case TR_ATTR::WATER:
        SceneGame::instance()->playerManager()->graviTimer = 0;
        if (mapStatus)
        {
            
            return (speed / -1.5f) - speed;
        }
        else
        {    
            return speed  / 4.5f;
        }
        
        break;
    default:
        return 0.0f;
        break;
    }
    return 0.0f;
}

float BG::getTeikouX(TR_ATTR attr, float speed)
{
   
    switch (attr)
    {
    case TR_ATTR::WATER:
        SceneGame::instance()->playerManager()->graviTimer = 0;
        
        if (mapStatus)
        {
            
           //return (speed / -1.5f) ;
            return (speed / -2.0f) ;
        }
        else
        {
            return (speed / -2.5f) ;
        }
      

        break;
    default:
        return 0.0f;
        break;
    }
    return 0.0f;
}

bool BG::isInWater(const VECTOR2 position)
{
    return (getTerrainAttr(position) == TR_ATTR::WATER);
}

bool BG::isSlipfloor(float x, float y, float width)
{

    for (; width > 0; width -= CHIP_SIZE)
    {

        if (x - width < 0)          continue;
        if (x + width > BG::WIDTH)  continue;

        if (isHitSlope(x - width, y)) return true;
        if (isHitSlope(x + width, y)) return true;
    }
    return isHitSlope(x, y);
}




bool BG::isHitSlope(float x, float y)
{
    switch (getTerrainAttr(VECTOR2(x, y)))                  // 地形の属性を取得する
    {
    case TR_ATTR::L_SLIP_BLOCK:
        SlipFloorisRight = false;
        return true;                // 全て坂の地形であった
    case TR_ATTR::R_SLIP_BLOCK:
        SlipFloorisRight = true;;
        return true;                // 全て坂の地形であった

        break;
    }
    return false;   // 地形ではなかった場合
}

bool BG::isHitSuberu(float x, float y)
{
    switch (getTerrainAttr(VECTOR2(x, y)))            // 地形の属性を取得する
    {
    case TR_ATTR::KOORI:
        return true;                // 全て坂の地形であった
        break;
    }
    return false;   // 地形ではなかった場合
}
