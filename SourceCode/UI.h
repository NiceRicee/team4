#pragma once

class UI
{
	public:
	UI();
	~UI(){}
private:
	//--------<変数>-------
	// 変数
	int state;
	int timer;
	int stayMisttimer;
public:

	void init();
	void update();
	void draw();

public:		
	//public変数
	float posX = 0;
	float posY = 0;
	float logScale;

	OBJ2D minimap;
	OBJ2D minichara;
	BG* bg;
	Player* pl;

	bool graduateflg;
	bool showUIWindow = false;
	//public関数
	float mist_color_w;
	float mist_color_w2;
private:
	float mist1X;
	float mist2X;
	float mist2_2X;
	float mist3X;

	bool mist1XRightFlg;
	bool mist2XRightFlg;
	bool mist2_2XRightFlg;
	bool mist3XRightFlg;

};
