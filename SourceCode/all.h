#ifndef ALL_H
#define ALL_H

//マクロ
#define ToRadian( x )   DirectX::XMConvertToRadians( x )
#define ToDegree( x )   DirectX::XMConvertToDegrees( x )

// システムやライブラリのヘッダーファイル
#include "../GameLib/game_lib.h"

// 共通で使うようなヘッダー
#include "scene.h"
//#include "user.h"
#include "main.h"
#include "common.h"
#include "obj2d.h"
#include "sprite_data.h"
#include "judge.h"
#include "fade.h"

// 親クラスがあるヘッダー

//各種ヘッダー
#include"MapEditor.h"
#include "map.h"
#include"stage1.h"
#include "player.h"
#include "UI.h"

#include"scene_clear.h"
#include"scene_game.h"
#include "scene_title.h"

//******************************************************************************

#ifdef USE_IMGUI
#include "../imgui/imgui.h"
#include "../imgui/imgui_internal.h"
#include "../imgui/imgui_impl_dx11.h"
#include "../imgui/imgui_impl_win32.h"
#endif

// メモリーリークを調べる
#include<crtdbg.h>
#ifdef _DEBUG
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

//namespace
using namespace std;
using namespace GameLib;
using namespace input;
#endif // !ALL_H
