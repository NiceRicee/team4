#pragma once
#include "scene_game.h"
#include "anime_object_base.h"

class Bonfire : public BaseAnimeObject
{
public:
    //--------<定数>--------
    bool is_ignition_bonfire[BONFIRE_NUM];
private:
    //--------<定数>--------
    const static int PARTICLE_NUM = 1;
    enum BONFIRE_ANIME
    {
        BEFORE_IGNITION,     // 0 点火前
        AFTER_IGNITION,      // 1 点火後
    };

    //--------<アニメーション関連のパラメータ>--------
    const animeData before_bonfire_anime_data = { 0, 15, 2, true };
    const animeData after_bonfire_anime_data  = { 1, 15, 5, true };
public:
    //--------<コンストラクタ/関数等>--------
    Bonfire(VECTOR2 c) : BaseAnimeObject(c)
    {
        for (int i = 0; i < BONFIRE_NUM; ++i) {
            is_ignition_bonfire[i] = false;
        }
    }
    bool anime_movement() override;
    void anime_draw() override;
};

class SignBoard : public BaseAnimeObject
{
public:
    //--------<定数>--------

private:
    //--------<定数>--------
    const static int OBJ_NUM = 2;

    VECTOR2 pos_init[OBJ_NUM]{ {720.0f, 162.0f},{820.0f, 162.0f} };

    //--------<アニメーション関連のパラメータ>--------
    const animeData before_signBoard_anime_data = { 0, 10, 1, true };
public:
    bool is_display[OBJ_NUM]{ false,false };
    //--------<コンストラクタ/関数等>--------
    SignBoard(VECTOR2 c) : BaseAnimeObject(c)
    {
        for (int i = 0; i < OBJ_NUM; ++i) {
            is_display[i] = false;
        }
    }
    bool anime_movement() override;
    void anime_draw() override;
};


class Wind_obj1 : public BaseAnimeObject
{
public:
    //--------<定数>--------

private:
    //--------<定数>--------
    const static int OBJ_NUM = 1;
    const float OBJ_SPEED = 12.0f;
    //--------<変数>--------
    bool wind_dire;  // false:右向き   true:左向き
public:
    //--------<コンストラクタ/関数等>--------
    Wind_obj1(VECTOR2 c, bool wnd_dir)
        : BaseAnimeObject(c)
        , wind_dire(wnd_dir) {}
    bool anime_movement() override;
    void anime_draw() override;
};

class Wind_obj2 : public BaseAnimeObject
{
public:
    //--------<定数>--------

private:
    //--------<定数>--------
    const static int OBJ_NUM = 1;
    const float OBJ_SPEED    = 6.0f;
    const float OBJ_SPEED_ROTATION = 4.0f;
    //--------<変数>--------
    bool wind_dire;  // false:右向き   true:左向き
    int move_state;
    float angle_base;
public:
    //--------<コンストラクタ/関数等>--------
    Wind_obj2(VECTOR2 c, bool wnd_dir)
        : BaseAnimeObject(c)
        , wind_dire(wnd_dir)
        , move_state(1)
        , angle_base() {}
    bool anime_movement() override;
    void anime_draw() override;
};

class Wind_obj3 : public BaseAnimeObject
{
public:
    //--------<定数>--------

private:
    //--------<定数>--------
    const static int OBJ_NUM = 1;
    const float OBJ_SPEED    = 8.0f;
    const float OBJ_SPEED_ROTATION = 4.0f;
    //--------<変数>--------
    bool wind_dire;  // false:右向き   true:左向き
    int move_state;
public:
    //--------<コンストラクタ/関数等>--------
    Wind_obj3(VECTOR2 c, bool wnd_dir)
        : BaseAnimeObject(c)
        , wind_dire(wnd_dir)
        , move_state(1) {}
    bool anime_movement() override;
    void anime_draw() override;
};


extern bool hantenFlg;