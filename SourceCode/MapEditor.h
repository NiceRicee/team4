#pragma once
using namespace std;
#include "../GameLib/vector.h"


#define AREANUMMAX 24


class MapEditor
{
private:
	MapEditor()
		:mapEditorFlg()
		,tipType()
		,state()
		,timer()
		, cursolPos()
		, EDTpoint()
		, doUseMause(false)
		, conState()
	{}
	~MapEditor(){}
private:
int	state;
int	timer;
public:
	static const int CHIP_NUM_X = 40;                    // マップの横方向のチップ数
	static const int CHIP_NUM_Y = 23;                    // マップの縦方向のチップ数
	static const int CHIP_NUM_PER_LINE = 6;             // マップチップの1列が3個
	static const int CHIP_LINE_NUM = 4;                 // マップチップが2行

public:
	static MapEditor& instance()
	{
		static MapEditor mapEditor;
		return mapEditor;
	}

	//const 型名　get関数名　const { return 変数名 }

	//パブリック関数
	void init();
	void update(char terrain[CHIP_NUM_Y][CHIP_NUM_X]);
	void drawMaptip(char terrain[CHIP_NUM_Y][CHIP_NUM_X]);

	void ImGuiCode();

	 bool showMapEditWindow = false;

	bool mapEditorFlg;
	bool SaveAsFlg = false;
	int tipType;
	//--------カーソル関連--------//
	// カーソル
	VECTOR2 cursolPos;
	int cursolArrayPosX;
	int cursolArrayPosY;
	// マウス
	POINT EDTpoint;

	bool doUseMause;
	int conState;

	//ファイル操作関連
	void saveData(char terrain[CHIP_NUM_Y][CHIP_NUM_X]);
	//const wchar_t* Filename = { L"./Data/MapEditor/saveMap.txt" };


	char fileN[256] = "./Data/MapEditor/saveMap.txt";


	//ifstream input_File;
	//ofstream output_File;

	int maptip_array[23][40] = { 0 };

	const int array_value = 920;
};
